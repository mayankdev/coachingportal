package com.coachingPortal;

import com.coachingPortal.result.Result;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.util.CellRangeAddress;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * Created by MAYANK DWIVEDI on 6/17/2015.
 */
public class ApachePOI {
    public void resultPracticePaperAdmin(HttpServletResponse response, List<Result> resultList,String examName){
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet("Result");
        int rowNum=0;
        int cellNum=0;

        HSSFFont hssfFont=workbook.createFont();
        hssfFont.setBold(true);

        HSSFCellStyle hssfCellStyle=workbook.createCellStyle();
        HSSFCellStyle hssfCellStyle2=workbook.createCellStyle();
        HSSFCellStyle hssfCellStyleDate=workbook.createCellStyle();

        /*hssfCellStyle.setFillBackgroundColor(HSSFColor.LIME.index);*/
        hssfCellStyle.setFont(hssfFont);

        hssfCellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
        hssfCellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        hssfCellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        hssfCellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
        hssfCellStyle.setFillForegroundColor(HSSFColor.GOLD.index);
        hssfCellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);


        hssfCellStyle2.setBorderTop(HSSFCellStyle.BORDER_THIN);
        hssfCellStyle2.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        hssfCellStyle2.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        hssfCellStyle2.setBorderRight(HSSFCellStyle.BORDER_THIN);

        hssfCellStyleDate.setBorderTop(HSSFCellStyle.BORDER_THIN);
        hssfCellStyleDate.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        hssfCellStyleDate.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        hssfCellStyleDate.setBorderRight(HSSFCellStyle.BORDER_THIN);
        CreationHelper createHelper = workbook.getCreationHelper();
        hssfCellStyleDate.setDataFormat(
                createHelper.createDataFormat().getFormat("dd/mm/yyyy h:mm"));

        sheet.addMergedRegion(new CellRangeAddress(
                0, //first row (0-based)
                0, //last row  (0-based)
                0, //first column (0-based)
                6  //last column  (0-based)
        ));



        HSSFRow row = sheet.createRow(rowNum++);
        HSSFCell cellAHeader=row.createCell(cellNum++);
        cellAHeader.setCellValue(examName);

        cellNum=0;
        HSSFRow rowHeader = sheet.createRow(rowNum++);
        HSSFCell cellA=rowHeader.createCell(cellNum++);
        cellA.setCellValue("Student Name");
        cellA.setCellStyle(hssfCellStyle);


        HSSFCell cellB=rowHeader.createCell(cellNum++);
        cellB.setCellValue("Correct");
        cellB.setCellStyle(hssfCellStyle);

        HSSFCell cellC=rowHeader.createCell(cellNum++);
        cellC.setCellValue("Incorrect");
        cellC.setCellStyle(hssfCellStyle);

        HSSFCell cellD=rowHeader.createCell(cellNum++);
        cellD.setCellValue("Unattempted");
        cellD.setCellStyle(hssfCellStyle);

        HSSFCell cellE=rowHeader.createCell(cellNum++);
        cellE.setCellValue("Time taken(Minutes:Seconds)");
        cellE.setCellStyle(hssfCellStyle);

        HSSFCell cellF=rowHeader.createCell(cellNum++);
        cellF.setCellValue("Date (dd/mm/yyyy h:mm)");
        cellF.setCellStyle(hssfCellStyle);



        for(int j=0;j<cellNum;j++){
            sheet.autoSizeColumn(j);
        }


        for(int i=0;i<resultList.size();i++){
            cellNum=0;
            HSSFRow rowData = sheet.createRow(rowNum++);

            HSSFCell cellDataA=rowData.createCell(cellNum++);
            cellDataA.setCellValue(resultList.get(i).getStudent().fullName());
            cellDataA.setCellStyle(hssfCellStyle2);

            HSSFCell cellDataB=rowData.createCell(cellNum++);
            cellDataB.setCellValue(resultList.get(i).getQuestionsCorrect());
            cellDataB.setCellStyle(hssfCellStyle2);

            HSSFCell cellDataC=rowData.createCell(cellNum++);
            cellDataC.setCellValue(resultList.get(i).getQuestionsIncorrect());
            cellDataC.setCellStyle(hssfCellStyle2);

            HSSFCell cellDataD=rowData.createCell(cellNum++);
            cellDataD.setCellValue(resultList.get(i).getQuestionsUnattempted());
            cellDataD.setCellStyle(hssfCellStyle2);

            HSSFCell cellDataE=rowData.createCell(cellNum++);
            cellDataE.setCellValue( ((resultList.get(i).getTimeTaken())/60000)+":"+(((resultList.get(i).getTimeTaken())/1000)%60));
            cellDataE.setCellStyle(hssfCellStyle2);

            HSSFCell cellDataF=rowData.createCell(cellNum++);
            cellDataF.setCellValue(resultList.get(i).getDateCreated());
            cellDataF.setCellStyle(hssfCellStyleDate);
        }

        try {
            ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
            workbook.write(outByteStream);
            byte [] outArray = outByteStream.toByteArray();
            response.setContentType("application/ms-excel");
            response.setContentLength(outArray.length);
            response.setHeader("Expires:", "0"); // eliminates browser caching
            response.setHeader("Content-Disposition", "attachment; filename=testxls.xls");
            OutputStream outStream = response.getOutputStream();
            outStream.write(outArray);
            outStream.flush();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
