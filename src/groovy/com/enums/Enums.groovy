package com.enums

public class Enums {

    public enum ExamName{
        SBI_PO,SBI_CLERK,IBPS_CLERK,IBPS_PO,IBPS_SPECIALIST,IBPS_RRB
    }

    public enum Gender{
        MALE,FEMALE,OTHER
    }

    public enum Difficulty{
        EASY,MEDIUM,TOUGH
    }

    public enum PracticePaperStatus{
        PUBLISH('Publish'),UNPUBLISH('UnPublish')

        String status;

        PracticePaperStatus(String status) {
            this.status = status;
        }

        String toString() {
            return this.status;
        }
    }
    /*public enum SubjectType{
       ENGLISH('English'),QUANTITATIVE_APTITUDE('Aptitude'),REASONING('Reasoning')

         String subjectTypeName;

        SubjectType(String subjectType) {
            this.subjectTypeName = subjectType;
        }

        String toString() {
            return this.subjectTypeName;
        }
    }*/


}
