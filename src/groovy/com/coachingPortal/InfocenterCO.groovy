package com.coachingPortal

/**
 * Created by MAYANK DWIVEDI on 2/24/2016.
 */
class InfocenterCO {

    String title
    String description

    static constraints = {
        title nullable: false, blank: false
        description nullable: false, blank: false
    }
}
