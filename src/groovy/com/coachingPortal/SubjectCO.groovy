package com.coachingPortal

import grails.validation.Validateable

/**
 * Created by MAYANK DWIVEDI on 6/21/2015.
 */
@Validateable
class SubjectCO {

    String subject
    String subSubject
    String topic


    static constraints = {

        subject nullable: false,blank: false
        subSubject nullable: false, blank: false
        topic nullable: false,blank: false
    }
}
