package com.coachingPortal

import com.coachingPortal.security.User
import grails.validation.Validateable

@Validateable
class PersonSignUpCO {

    String firstName
    String lastName
    long contactNo
    String password
    String confirmPassword
    String username


    static constraints = {
        firstName nullable: true,blank: true
        lastName nullable: true,blank:true
        contactNo blank:false, matches:"[0-9]{10}"
        password nullable: false,blank: false

        confirmPassword blank: false,nullable: false,validator: {val,obj->
            if(!val.equals(obj.password))
                return 'userSignUpCO.password.match.error'
        }
        username blank: false, nullable: false, email: true, validator: { val, obj ->
            if (User.findByUsername(val))
                return 'userSignUpCO.username.already.registered'

        }
    }
}
