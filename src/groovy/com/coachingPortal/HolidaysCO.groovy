package com.coachingPortal

import com.coachingPortal.coaching.tasks.Holidays
import grails.validation.Validateable

/**
 * Created by MAYANK DWIVEDI on 2/24/2016.
 */
@Validateable
class HolidaysCO {
    String title
    Date dateOfHoliday

    HolidaysCO(Holidays holidays){
        this.title=holidays?.title
        this.dateOfHoliday=holidays?.dateOfHoliday
    }

    static constraints = {
        title nullable: false, blank: false
        dateOfHoliday nullable: false, blank: false
    }
}
