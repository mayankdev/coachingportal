package com.coachingPortal

import grails.validation.Validateable


@Validateable
class PasswordResetCO {
    String password
    String confirmPassword

    static constraints = {

        password blank: false,nullable: false

        confirmPassword blank: false,nullable: false,validator: {val,obj->
            if(!val.equals(obj.password))
                return 'passwordResetCO.password.match.error'
        }

    }
}
