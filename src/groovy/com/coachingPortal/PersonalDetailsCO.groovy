package com.coachingPortal

import grails.validation.Validateable

/**
 * Created by hitenpratap on 1/5/15.
 */
@Validateable
class PersonalDetailsCO {

    String firstName
    String lastName
    String phoneNumber
    String personId


}
