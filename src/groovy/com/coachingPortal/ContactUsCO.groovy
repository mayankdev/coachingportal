package com.coachingPortal

import grails.validation.Validateable


@Validateable
class ContactUsCO {

    String name
    String mailId
    String subject
    String body

    static constraints = {

        mailId nullable: false,blank: false,email:true
        name nullable:true,  blank: true
        subject nullable: false, blank: false
        body nullable: false,blank: false
    }
}
