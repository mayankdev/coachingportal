package com.coachingPortal

import com.coachingPortal.security.User
import grails.validation.Validateable

@Validateable
class CoachingSignUpCO {

    String name
    long contactNo
    String password
    String confirmPassword
    String username
    String streetName
    String city
    String state
    String country
    int postalCode



    static constraints = {
        name nullable: false,blank: false
        contactNo blank:false, matches:"[0-9]{10}"
        streetName nullable: false,blank: false
        city nullable: false,blank: false
        state nullable: false,blank: false
        postalCode max:6,min: 6


        confirmPassword blank: false,nullable: false,validator: {val,obj->
            if(!val.equals(obj.password))
                return 'userSignUpCO.password.match.error'
        }
        username blank: false, nullable: false, email: true, validator: { val, obj ->
            if (User.findByUsername(val))
                return 'userSignUpCO.username.already.registered'

        }
    }
}
