package com.coachingPortal.coaching.tasks

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

@TestFor(HolidaysController)
@Mock(Holidays)
class HolidaysControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.holidaysInstanceList
            model.holidaysInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.holidaysInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            request.contentType = FORM_CONTENT_TYPE
            def holidays = new Holidays()
            holidays.validate()
            controller.save(holidays)

        then:"The create view is rendered again with the correct model"
            model.holidaysInstance!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            holidays = new Holidays(params)

            controller.save(holidays)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/holidays/show/1'
            controller.flash.message != null
            Holidays.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def holidays = new Holidays(params)
            controller.show(holidays)

        then:"A model is populated containing the domain instance"
            model.holidaysInstance == holidays
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def holidays = new Holidays(params)
            controller.edit(holidays)

        then:"A model is populated containing the domain instance"
            model.holidaysInstance == holidays
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            request.contentType = FORM_CONTENT_TYPE
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/holidays/index'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def holidays = new Holidays()
            holidays.validate()
            controller.update(holidays)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.holidaysInstance == holidays

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            holidays = new Holidays(params).save(flush: true)
            controller.update(holidays)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/holidays/show/$holidays.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            request.contentType = FORM_CONTENT_TYPE
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/holidays/index'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def holidays = new Holidays(params).save(flush: true)

        then:"It exists"
            Holidays.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(holidays)

        then:"The instance is deleted"
            Holidays.count() == 0
            response.redirectedUrl == '/holidays/index'
            flash.message != null
    }
}
