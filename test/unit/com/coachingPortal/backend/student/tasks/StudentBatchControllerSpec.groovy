package com.coachingPortal.backend.student.tasks

import com.coachingPortal.backend.student.StudentBatchController
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(StudentBatchController)
class StudentBatchControllerSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test something"() {
    }
}
