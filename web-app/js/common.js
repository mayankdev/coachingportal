function setSubjectSubType() {
    var subjectType = $("#subjectType").val();

    $.ajax({
        type: "POST",
        url: "${createLink(controller: 'adminQuestion',action: 'ax_getSubjectSubType')}",
        data: {subjectType: subjectType}
    }).done(function (data) {
        if (data.result == 'SUCCESS') {
            $("#subjectSubType").html(data.subjectSubTypeTemplate);
            $("#subjectSubTypeTopic").html(data.subjectTopicTemplate);

        } else {
            alert(data.errMsg);
        }
    });
}

function setTopic() {
    var subjectType = $("#subjectType").val();
    var subjectSubType = $("#subjectSubTypeSelect").val();

    $.ajax({
        type: "POST",
        url: "${createLink(controller: 'adminQuestion',action: 'ax_getSubjectTopic')}",
        data: {subjectType: subjectType, subjectSubType: subjectSubType}
    }).done(function (data) {
        if (data.result == 'SUCCESS') {
            $("#subjectSubTypeTopic").html(data.subjectTopicTemplate);
        } else {
            alert(data.errMsg);
        }
    });
}
