<html>
<head></head>

<body>
<h2>
    <a href="${createLink(controller: 'common', action: 'verifySubscriberUserEmailAddress', absolute: true, params: [subscriberId: subscriberUser?.uniqueId])}">
        Verify Here
    </a>
</h2>
</body>
</html>