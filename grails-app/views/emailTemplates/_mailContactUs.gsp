<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>

</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-xs-12">
            <div class="well">
                <h4>Contact Us | ${contactUsCO?.subject}</h4>
                <br/><hr/>
                <br/>
                Name: ${contactUsCO?.name} <br/>
                Email Address: ${contactUsCO?.mailId}<br/>
                Subject: ${contactUsCO?.subject}<br/>
                Description: ${contactUsCO?.body}<br/>
             </div>
          </div>
    </div>
 </div>

</body>
</html>