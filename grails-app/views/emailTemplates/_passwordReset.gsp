<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>coachingeasy4u</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="600">
                <tr>
                    <td align="center"  style="padding: 20px 0 10px 0;">
                        <img src="http://mayankdwivedi.in/prepareEasy.png" alt="PrepareEasy"  style="display: block;height: 120px;" />
                        %{--<h2 style="color: #ffffff">coachingeasy4u</h2>--}%
                    </td>
                </tr>

                <tr>
                    <td bgcolor="#ffffff">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="left">

                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <h3>Password Reset</h3>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    Please click on this <a href="${createLink(absolute: true, controller: 'login', action: 'passwordResetMailPage', params: [token: person?.uniqueId])}">link</a> to reset your password.
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    Regards,<br>
                                    Team PrepareEasy
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td bgcolor="#c0c0c0">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td width="75%" height="40px" style="color:#ffffff;">
                                &reg; prepareeasy.in 2015<br/>

                                </td>
                                <td align="right">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <a href="http://www.twitter.com/">
                                                    %{--<img src="${resource(dir: 'images/emailTemplate' ,file: 'twitter.png')}" alt="Twitter" width="38" height="38" style="display: block;" border="0" />--}%
                                                    Twitter
                                                </a>
                                            </td>
                                            <td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
                                            <td>
                                                <a href="http://www.facebook.com/">
                                                    %{--<img src="${resource(dir: 'images/emailTemplate' ,file: 'facebook.png')}" alt="Facebook" width="38" height="38" style="display: block;" border="0" />--}%
                                                    Facebook
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>