<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!-- NAME: MINIMAL -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>*|MC:SUBJECT|*</title>

    <style type="text/css">
    body, #bodyTable, #bodyCell {
        height: 100% !important;
        margin: 0;
        padding: 0;
        width: 100% !important;
    }

    table {
        border-collapse: collapse;
    }

    img, a img {
        border: 0;
        outline: none;
        text-decoration: none;
    }

    h1, h2, h3, h4, h5, h6 {
        margin: 0;
        padding: 0;
    }

    p {
        margin: 1em 0;
        padding: 0;
    }

    a {
        word-wrap: break-word;
    }

    .ReadMsgBody {
        width: 100%;
    }

    .ExternalClass {
        width: 100%;
    }

    .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
        line-height: 100%;
    }

    table, td {
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
    }

    #outlook a {
        padding: 0;
    }

    img {
        -ms-interpolation-mode: bicubic;
    }

    body, table, td, p, a, li, blockquote {
        -ms-text-size-adjust: 100%;
        -webkit-text-size-adjust: 100%;
    }

    #bodyCell {
        padding: 0;
    }

    .mcnImage {
        vertical-align: bottom;
    }

    .mcnTextContent img {
        height: auto !important;
    }

    a.mcnButton {
        display: block;
    }

    /*
    @tab Page
    @section background style
    @tip Set the background color and top border for your email. You may want to choose colors that match your company's branding.
    */
    body, #bodyTable {
        /*@editable*/
        background-color: #FEFEFE;
    }

    /*
    @tab Page
    @section background style
    @tip Set the background color and top border for your email. You may want to choose colors that match your company's branding.
    */
    #bodyCell {
        /*@editable*/
        border-top: 0;
    }

    /*
    @tab Page
    @section heading 1
    @tip Set the styling for all first-level headings in your emails. These should be the largest of your headings.
    @style heading 1
    */
    h1 {
        /*@editable*/
        color: #C52E26 !important;
        display: block;
        /*@editable*/
        font-family: Helvetica;
        /*@editable*/
        font-size: 28px;
        /*@editable*/
        font-style: normal;
        /*@editable*/
        font-weight: bold;
        /*@editable*/
        line-height: 125%;
        /*@editable*/
        letter-spacing: normal;
        margin: 0;
        /*@editable*/
        text-align: left;
    }

    /*
    @tab Page
    @section heading 2
    @tip Set the styling for all second-level headings in your emails.
    @style heading 2
    */
    h2 {
        /*@editable*/
        color: #808080 !important;
        display: block;
        /*@editable*/
        font-family: Helvetica;
        /*@editable*/
        font-size: 26px;
        /*@editable*/
        font-style: normal;
        /*@editable*/
        font-weight: bold;
        /*@editable*/
        line-height: 125%;
        /*@editable*/
        letter-spacing: normal;
        margin: 0;
        /*@editable*/
        text-align: left;
    }

    /*
    @tab Page
    @section heading 3
    @tip Set the styling for all third-level headings in your emails.
    @style heading 3
    */
    h3 {
        /*@editable*/
        color: #A5A5A5 !important;
        display: block;
        /*@editable*/
        font-family: Helvetica;
        /*@editable*/
        font-size: 18px;
        /*@editable*/
        font-style: normal;
        /*@editable*/
        font-weight: bold;
        /*@editable*/
        line-height: 125%;
        /*@editable*/
        letter-spacing: normal;
        margin: 0;
        /*@editable*/
        text-align: left;
    }

    /*
    @tab Page
    @section heading 4
    @tip Set the styling for all fourth-level headings in your emails. These should be the smallest of your headings.
    @style heading 4
    */
    h4 {
        /*@editable*/
        color: #606060 !important;
        display: block;
        /*@editable*/
        font-family: Helvetica;
        /*@editable*/
        font-size: 16px;
        /*@editable*/
        font-style: normal;
        /*@editable*/
        font-weight: bold;
        /*@editable*/
        line-height: 125%;
        /*@editable*/
        letter-spacing: normal;
        margin: 0;
        /*@editable*/
        text-align: left;
    }

    /*
    @tab Preheader
    @section preheader style
    @tip Set the background color and borders for your email's preheader area.
    */
    #templatePreheader {
        /*@editable*/
        background-color: #ffffff;
        /*@editable*/
        border-top: 0;
        /*@editable*/
        border-bottom: 0;
    }

    /*
    @tab Preheader
    @section preheader text
    @tip Set the styling for your email's preheader text. Choose a size and color that is easy to read.
    */
    .preheaderContainer .mcnTextContent, .preheaderContainer .mcnTextContent p {
        /*@editable*/
        color: #808080;
        /*@editable*/
        font-family: Helvetica;
        /*@editable*/
        font-size: 10px;
        /*@editable*/
        line-height: 125%;
        /*@editable*/
        text-align: left;
    }

    /*
    @tab Preheader
    @section preheader link
    @tip Set the styling for your email's header links. Choose a color that helps them stand out from your text.
    */
    .preheaderContainer .mcnTextContent a {
        /*@editable*/
        color: #C52E26;
        /*@editable*/
        font-weight: bold;
        /*@editable*/
        text-decoration: none;
    }

    /*
    @tab Header
    @section header style
    @tip Set the background color and borders for your email's header area.
    */
    #templateHeader {
        /*@editable*/
        background-color: #FEFEFE;
        /*@editable*/
        border-top: 0;
        /*@editable*/
        border-bottom: 0;
    }

    /*
    @tab Header
    @section header text
    @tip Set the styling for your email's header text. Choose a size and color that is easy to read.
    */
    .headerContainer .mcnTextContent, .headerContainer .mcnTextContent p {
        /*@editable*/
        color: #606060;
        /*@editable*/
        font-family: Helvetica;
        /*@editable*/
        font-size: 14px;
        /*@editable*/
        line-height: 150%;
        /*@editable*/
        text-align: left;
    }

    /*
    @tab Header
    @section header link
    @tip Set the styling for your email's header links. Choose a color that helps them stand out from your text.
    */
    .headerContainer .mcnTextContent a {
        /*@editable*/
        color: #C52E26;
        /*@editable*/
        font-weight: bold;
        /*@editable*/
        text-decoration: none;
    }

    /*
    @tab Body
    @section body style
    @tip Set the background color and borders for your email's body area.
    */
    #templateBody {
        /*@editable*/
        background-color: #FEFEFE;
        /*@editable*/
        border-top: 0;
        /*@editable*/
        border-bottom: 0;
    }

    /*
    @tab Body
    @section body text
    @tip Set the styling for your email's body text. Choose a size and color that is easy to read.
    */
    .bodyContainer .mcnTextContent, .bodyContainer .mcnTextContent p {
        /*@editable*/
        color: #606060;
        /*@editable*/
        font-family: Helvetica;
        /*@editable*/
        font-size: 14px;
        /*@editable*/
        line-height: 150%;
        /*@editable*/
        text-align: left;
    }

    /*
    @tab Body
    @section body link
    @tip Set the styling for your email's body links. Choose a color that helps them stand out from your text.
    */
    .bodyContainer .mcnTextContent a {
        /*@editable*/
        color: #C52E26;
        /*@editable*/
        font-weight: bold;
        /*@editable*/
        text-decoration: none;
    }

    /*
    @tab Footer
    @section footer style
    @tip Set the background color and borders for your email's footer area.
    */
    #templateFooter {
        /*@editable*/
        background-color: #FEFEFE;
        /*@editable*/
        border-top: 0;
        /*@editable*/
        border-bottom: 0;
    }

    /*
    @tab Footer
    @section footer text
    @tip Set the styling for your email's footer text. Choose a size and color that is easy to read.
    */
    .footerContainer .mcnTextContent, .footerContainer .mcnTextContent p {
        /*@editable*/
        color: #808080;
        /*@editable*/
        font-family: Helvetica;
        /*@editable*/
        font-size: 10px;
        /*@editable*/
        line-height: 125%;
        /*@editable*/
        text-align: left;
    }

    /*
    @tab Footer
    @section footer link
    @tip Set the styling for your email's footer links. Choose a color that helps them stand out from your text.
    */
    .footerContainer .mcnTextContent a {
        /*@editable*/
        color: #C52E26;
        /*@editable*/
        font-weight: bold;
        /*@editable*/
        text-decoration: none;
    }

    @media only screen and (max-width: 480px) {
        body, table, td, p, a, li, blockquote {
            -webkit-text-size-adjust: none !important;
        }

    }

    @media only screen and (max-width: 480px) {
        body {
            width: 100% !important;
            min-width: 100% !important;
        }

    }

    @media only screen and (max-width: 480px) {
        table[class=mcnTextContentContainer] {
            width: 100% !important;
        }

    }

    @media only screen and (max-width: 480px) {
        table[class=mcnBoxedTextContentContainer] {
            width: 100% !important;
        }

    }

    @media only screen and (max-width: 480px) {
        table[class=mcpreview-image-uploader] {
            width: 100% !important;
            display: none !important;
        }

    }

    @media only screen and (max-width: 480px) {
        img[class=mcnImage] {
            width: 100% !important;
        }

    }

    @media only screen and (max-width: 480px) {
        table[class=mcnImageGroupContentContainer] {
            width: 100% !important;
        }

    }

    @media only screen and (max-width: 480px) {
        td[class=mcnImageGroupContent] {
            padding: 9px !important;
        }

    }

    @media only screen and (max-width: 480px) {
        td[class=mcnImageGroupBlockInner] {
            padding-bottom: 0 !important;
            padding-top: 0 !important;
        }

    }

    @media only screen and (max-width: 480px) {
        tbody[class=mcnImageGroupBlockOuter] {
            padding-bottom: 9px !important;
            padding-top: 9px !important;
        }

    }

    @media only screen and (max-width: 480px) {
        table[class=mcnCaptionTopContent], table[class=mcnCaptionBottomContent] {
            width: 100% !important;
        }

    }

    @media only screen and (max-width: 480px) {
        table[class=mcnCaptionLeftTextContentContainer], table[class=mcnCaptionRightTextContentContainer], table[class=mcnCaptionLeftImageContentContainer], table[class=mcnCaptionRightImageContentContainer], table[class=mcnImageCardLeftTextContentContainer], table[class=mcnImageCardRightTextContentContainer] {
            width: 100% !important;
        }

    }

    @media only screen and (max-width: 480px) {
        td[class=mcnImageCardLeftImageContent], td[class=mcnImageCardRightImageContent] {
            padding-right: 18px !important;
            padding-left: 18px !important;
            padding-bottom: 0 !important;
        }

    }

    @media only screen and (max-width: 480px) {
        td[class=mcnImageCardBottomImageContent] {
            padding-bottom: 9px !important;
        }

    }

    @media only screen and (max-width: 480px) {
        td[class=mcnImageCardTopImageContent] {
            padding-top: 18px !important;
        }

    }

    @media only screen and (max-width: 480px) {
        td[class=mcnImageCardLeftImageContent], td[class=mcnImageCardRightImageContent] {
            padding-right: 18px !important;
            padding-left: 18px !important;
            padding-bottom: 0 !important;
        }

    }

    @media only screen and (max-width: 480px) {
        td[class=mcnImageCardBottomImageContent] {
            padding-bottom: 9px !important;
        }

    }

    @media only screen and (max-width: 480px) {
        td[class=mcnImageCardTopImageContent] {
            padding-top: 18px !important;
        }

    }

    @media only screen and (max-width: 480px) {
        table[class=mcnCaptionLeftContentOuter] td[class=mcnTextContent], table[class=mcnCaptionRightContentOuter] td[class=mcnTextContent] {
            padding-top: 9px !important;
        }

    }

    @media only screen and (max-width: 480px) {
        td[class=mcnCaptionBlockInner] table[class=mcnCaptionTopContent]:last-child td[class=mcnTextContent] {
            padding-top: 18px !important;
        }

    }

    @media only screen and (max-width: 480px) {
        td[class=mcnBoxedTextContentColumn] {
            padding-left: 18px !important;
            padding-right: 18px !important;
        }

    }

    @media only screen and (max-width: 480px) {
        td[class=mcnTextContent] {
            padding-right: 18px !important;
            padding-left: 18px !important;
        }

    }

    @media only screen and (max-width: 480px) {
        /*
        @tab Mobile Styles
        @section template width
        @tip Make the template fluid for portrait or landscape view adaptability. If a fluid layout doesn't work for you, set the width to 300px instead.
        */
        table[class=templateContainer] {
            /*@tab Mobile Styles
@section template width
@tip Make the template fluid for portrait or landscape view adaptability. If a fluid layout doesn't work for you, set the width to 300px instead.*/
            max-width: 600px !important;
            /*@editable*/
            width: 100% !important;
        }

    }

    @media only screen and (max-width: 480px) {
        /*
        @tab Mobile Styles
        @section heading 1
        @tip Make the first-level headings larger in size for better readability on small screens.
        */
        h1 {
            /*@editable*/
            font-size: 24px !important;
            /*@editable*/
            line-height: 125% !important;
        }

    }

    @media only screen and (max-width: 480px) {
        /*
        @tab Mobile Styles
        @section heading 2
        @tip Make the second-level headings larger in size for better readability on small screens.
        */
        h2 {
            /*@editable*/
            font-size: 20px !important;
            /*@editable*/
            line-height: 125% !important;
        }

    }

    @media only screen and (max-width: 480px) {
        /*
        @tab Mobile Styles
        @section heading 3
        @tip Make the third-level headings larger in size for better readability on small screens.
        */
        h3 {
            /*@editable*/
            font-size: 18px !important;
            /*@editable*/
            line-height: 125% !important;
        }

    }

    @media only screen and (max-width: 480px) {
        /*
        @tab Mobile Styles
        @section heading 4
        @tip Make the fourth-level headings larger in size for better readability on small screens.
        */
        h4 {
            /*@editable*/
            font-size: 16px !important;
            /*@editable*/
            line-height: 125% !important;
        }

    }

    @media only screen and (max-width: 480px) {
        /*
        @tab Mobile Styles
        @section Boxed Text
        @tip Make the boxed text larger in size for better readability on small screens. We recommend a font size of at least 16px.
        */
        table[class=mcnBoxedTextContentContainer] td[class=mcnTextContent], td[class=mcnBoxedTextContentContainer] td[class=mcnTextContent] p {
            /*@editable*/
            font-size: 18px !important;
            /*@editable*/
            line-height: 125% !important;
        }

    }

    @media only screen and (max-width: 480px) {
        /*
        @tab Mobile Styles
        @section Preheader Visibility
        @tip Set the visibility of the email's preheader on small screens. You can hide it to save space.
        */
        table[id=templatePreheader] {
            /*@editable*/
            display: block !important;
        }

    }

    @media only screen and (max-width: 480px) {
        /*
        @tab Mobile Styles
        @section Preheader Text
        @tip Make the preheader text larger in size for better readability on small screens.
        */
        td[class=preheaderContainer] td[class=mcnTextContent], td[class=preheaderContainer] td[class=mcnTextContent] p {
            /*@editable*/
            font-size: 14px !important;
            /*@editable*/
            line-height: 115% !important;
        }

    }

    @media only screen and (max-width: 480px) {
        /*
        @tab Mobile Styles
        @section Header Text
        @tip Make the header text larger in size for better readability on small screens.
        */
        td[class=headerContainer] td[class=mcnTextContent], td[class=headerContainer] td[class=mcnTextContent] p {
            /*@editable*/
            font-size: 18px !important;
            /*@editable*/
            line-height: 125% !important;
        }

    }

    @media only screen and (max-width: 480px) {
        /*
        @tab Mobile Styles
        @section Body Text
        @tip Make the body text larger in size for better readability on small screens. We recommend a font size of at least 16px.
        */
        td[class=bodyContainer] td[class=mcnTextContent], td[class=bodyContainer] td[class=mcnTextContent] p {
            /*@editable*/
            font-size: 18px !important;
            /*@editable*/
            line-height: 125% !important;
        }

    }

    @media only screen and (max-width: 480px) {
        /*
        @tab Mobile Styles
        @section footer text
        @tip Make the body content text larger in size for better readability on small screens.
        */
        td[class=footerContainer] td[class=mcnTextContent], td[class=footerContainer] td[class=mcnTextContent] p {
            /*@editable*/
            font-size: 14px !important;
            /*@editable*/
            line-height: 115% !important;
        }

    }

    @media only screen and (max-width: 480px) {
        td[class=footerContainer] a[class=utilityLink] {
            display: block !important;
        }

    }</style>
    <script type="text/javascript">
        var AKSB = AKSB || {};
        AKSB.q = [];
        AKSB.mark = function (c, a) {
            AKSB.q.push(["mark", c, a || (new Date).getTime()])
        };
        AKSB.measure = function (c, a, b) {
            AKSB.q.push(["measure", c, a, b || (new Date).getTime()])
        };
        AKSB.done = function (c) {
            AKSB.q.push(["done", c])
        };
        AKSB.mark("firstbyte", (new Date).getTime());
        AKSB.prof = {
            custid: "335882",
            ustr: "",
            originlat: 0,
            clientrtt: 50,
            ghostip: "96.17.182.144",
            ipv6: false,
            pct: 10,
            xhrtest: false,
            clientip: "182.64.176.141",
            requestid: "6c21101f",
            protocol: "",
            akM: "b",
            akN: "ae"
        };
        (function (c) {
            var a = document.createElement("iframe");
            a.src = "javascript:false";
            (a.frameElement || a).style.cssText = "width: 0; height: 0; border: 0; display: none";
            var b = document.getElementsByTagName("script"), b = b[b.length - 1];
            b.parentNode.insertBefore(a, b);
            a = a.contentWindow.document;
            b = String.fromCharCode;
            c = b(60) + "body onload=\"var js = document.createElement('script');js.id = 'aksb-ifr';js.src = '" + c + "';document.body.appendChild(js);\"" + b(62);
            a.open().write(c);
            a.close()
        })(("https:" === document.location.protocol ?
                "https:" : "http:") + "//ds-aksb-a.akamaihd.net/aksb.min.js");
    </script>
</head>

<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0"
      style="margin: 0;padding: 0;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FEFEFE;height: 100% !important;width: 100% !important;">
<center>
    <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable"
           style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;margin: 0;padding: 0;background-color: #FEFEFE;height: 100% !important;width: 100% !important;">
        <tr>
            <td align="center" valign="top" id="bodyCell"
                style="padding-bottom: 40px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;margin: 0;padding: 0;border-top: 0;height: 100% !important;width: 100% !important;">
                <!-- BEGIN TEMPLATE // -->
                <table border="0" cellpadding="0" cellspacing="0" width="100%"
                       style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                    <tr>
                        <td align="center" valign="top"
                            style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                            <!-- BEGIN PREHEADER // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templatePreheader"
                                   style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #ffffff;border-top: 0;border-bottom: 0;">
                                <tr>
                                    <td align="center" valign="top"
                                        style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                        <table border="0" cellpadding="0" cellspacing="0" width="600"
                                               class="templateContainer"
                                               style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                            <tr>
                                                <td valign="top" class="preheaderContainer"
                                                    style="padding-top: 9px;padding-bottom: 9px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <!-- // END PREHEADER -->
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top"
                            style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                            <!-- BEGIN HEADER // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateHeader"
                                   style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FEFEFE;border-top: 0;border-bottom: 0;">
                                <tr>
                                    <td align="center" valign="top"
                                        style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                        <table border="0" cellpadding="0" cellspacing="0" width="600"
                                               class="templateContainer"
                                               style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                            <tr>
                                                <td valign="top" class="headerContainer"
                                                    style="padding-top: 9px;padding-bottom: 9px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" "><table
                                                    class="mcnDividerBlock" border="0" width="100%" cellpadding="0"
                                                    cellspacing="0"
                                                    style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                <tbody class="mcnDividerBlockOuter">
                                                <tr>
                                                    <td class="mcnDividerBlockInner"
                                                        style="padding: 18px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                        <table class="mcnDividerContent" border="0" width="100%"
                                                               cellpadding="0" cellspacing="0"
                                                               style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                            <tbody><tr>
                                                                <td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                    <span></span>
                                                                </td>
                                                            </tr>
                                                            </tbody></table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table><table class="mcnImageBlock" border="0" width="100%" cellpadding="0"
                                                           cellspacing="0"
                                                           style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                <tbody class="mcnImageBlockOuter">
                                                <tr>
                                                    <td style="padding: 9px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                        class="mcnImageBlockInner" valign="top">
                                                        <table class="mcnImageContentContainer" align="left" border="0"
                                                               width="100%" cellpadding="0" cellspacing="0"
                                                               style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                            <tbody><tr>
                                                                <td class="mcnImageContent"
                                                                    style="padding-right: 9px;padding-left: 9px;padding-top: 0;padding-bottom: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                    valign="top">

                                                                    <a href="http://clashmate.in" title="" class=""
                                                                       target="_blank"
                                                                       style="word-wrap: break-word;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                        <img alt="ClashMate | Your true competition companion"
                                                                             src="https://gallery.mailchimp.com/6b1dbc663d15a0992099c07dc/images/288a5176-9268-48b4-a6ad-dbe9be29c518.png"
                                                                             style="max-width: 514px;padding-bottom: 0;display: inline !important;vertical-align: bottom;border: 0;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;"
                                                                             class="mcnImage" align="left" width="257">
                                                                    </a>

                                                                </td>
                                                            </tr>
                                                            </tbody></table>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <!-- // END HEADER -->
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top"
                            style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                            <!-- BEGIN BODY // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody"
                                   style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FEFEFE;border-top: 0;border-bottom: 0;">
                                <tr>
                                    <td align="center" valign="top"
                                        style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                        <table border="0" cellpadding="0" cellspacing="0" width="600"
                                               class="templateContainer"
                                               style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                            <tr>
                                                <td valign="top" class="bodyContainer"
                                                    style="padding-top: 9px;padding-bottom: 9px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table
                                                        border="0" cellpadding="0" cellspacing="0" width="100%"
                                                        class="mcnBoxedTextBlock"
                                                        style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                    <tbody class="mcnBoxedTextBlockOuter">
                                                    <tr>
                                                        <td valign="top" class="mcnBoxedTextBlockInner"
                                                            style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">

                                                            <table align="left" border="0" cellpadding="0"
                                                                   cellspacing="0" width="600"
                                                                   class="mcnBoxedTextContentContainer"
                                                                   style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                <tbody><tr>

                                                                    <td style="padding-top: 9px;padding-left: 18px;padding-bottom: 9px;padding-right: 18px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">

                                                                        <table border="0" cellpadding="18"
                                                                               cellspacing="0"
                                                                               class="mcnTextContentContainer"
                                                                               width="100%"
                                                                               style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                            <tbody><tr>
                                                                                <td valign="top" class="mcnTextContent"
                                                                                    style="color: #2C3E50;font-family: Verdana, Geneva, sans-serif;font-size: 30px;font-weight: bold;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;line-height: 150%;text-align: left;">
                                                                                    <h1 style="margin: 0;padding: 0;display: block;font-family: Helvetica;font-size: 28px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: normal;text-align: left;color: #C52E26 !important;"><span
                                                                                            style="color:#000000"><span
                                                                                                style="font-size:13px; line-height:1.6em">Hello</span><span
                                                                                                style="font-family:trebuchet ms,lucida grande,lucida sans unicode,lucida sans,tahoma,sans-serif; font-size:13px; line-height:1.6em"><strong> ${emailAddress}</strong>
                                                                                        </span><span
                                                                                                style="font-size:13px; line-height:1.6em">,</span>
                                                                                    </span></h1>

                                                                                    <p style="margin: 1em 0;padding: 0;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #606060;font-family: Helvetica;font-size: 14px;line-height: 150%;text-align: left;">${mailBody}</p>

                                                                                </td>
                                                                            </tr>
                                                                            </tbody></table>
                                                                    </td>
                                                                </tr>
                                                                </tbody></table>

                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table><table border="0" cellpadding="0" cellspacing="0" width="100%"
                                                               class="mcnButtonBlock"
                                                               style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                    <tbody class="mcnButtonBlockOuter">
                                                    <tr>
                                                        <td style="padding-top: 0;padding-right: 18px;padding-bottom: 18px;padding-left: 18px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                            valign="top" align="left" class="mcnButtonBlockInner">
                                                            <table border="0" cellpadding="0" cellspacing="0"
                                                                   width="100%" class="mcnButtonContentContainer"
                                                                   style="border-collapse: separate !important;border: 2px none #415161;border-radius: 10px;background-color: #415161;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                <tbody>
                                                                <tr>
                                                                    <td align="center" valign="middle"
                                                                        class="mcnButtonContent"
                                                                        style="font-family: 'Lucida Sans Unicode', 'Lucida Grande', sans-serif;font-size: 18px;padding: 20px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                        <a class="mcnButton " title="Go To ClashMate"
                                                                           href="${createLink(absolute: true, uri: '/')}"
                                                                           target="_self"
                                                                           style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;word-wrap: break-word;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;display: block;">Go To ClashMate</a>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <!-- // END BODY -->
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top"
                            style="padding-bottom: 40px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                            <!-- BEGIN FOOTER // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateFooter"
                                   style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FEFEFE;border-top: 0;border-bottom: 0;">
                                <tr>
                                    <td align="center" valign="top"
                                        style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                        <table border="0" cellpadding="0" cellspacing="0" width="600"
                                               class="templateContainer"
                                               style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                            <tr>
                                                <td valign="top" class="footerContainer"
                                                    style="padding-top: 9px;padding-bottom: 9px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><table
                                                        class="mcnTextBlock" border="0" width="100%" cellpadding="0"
                                                        cellspacing="0"
                                                        style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                    <tbody class="mcnTextBlockOuter">
                                                    <tr>
                                                        <td class="mcnTextBlockInner" valign="top"
                                                            style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">

                                                            <table class="mcnTextContentContainer" align="left"
                                                                   border="0" width="600" cellpadding="0"
                                                                   cellspacing="0"
                                                                   style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                <tbody><tr>

                                                                    <td class="mcnTextContent"
                                                                        style="padding-top: 9px;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #808080;font-family: Helvetica;font-size: 10px;line-height: 125%;text-align: left;"
                                                                        valign="top">

                                                                        <div style="text-align: left;"><span
                                                                                style="font-size:13px"><span
                                                                                    style="font-family:verdana,geneva,sans-serif"><strong>&nbsp;&nbsp; Regards,<br>
                                                                                    &nbsp;&nbsp; Team ClashMate</strong>
                                                                            </span></span></div>

                                                                    </td>
                                                                </tr>
                                                                </tbody></table>

                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table><table class="mcnDividerBlock" border="0" width="100%"
                                                               cellpadding="0" cellspacing="0"
                                                               style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                    <tbody class="mcnDividerBlockOuter">
                                                    <tr>
                                                        <td class="mcnDividerBlockInner"
                                                            style="padding: 9px 18px 27px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                            <table style="border-top: 1px solid #D5D5D5;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                   class="mcnDividerContent" border="0" width="100%"
                                                                   cellpadding="0" cellspacing="0">
                                                                <tbody><tr>
                                                                    <td style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                        <span></span>
                                                                    </td>
                                                                </tr>
                                                                </tbody></table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table><table class="mcnTextBlock" border="0" width="100%"
                                                               cellpadding="0" cellspacing="0"
                                                               style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                    <tbody class="mcnTextBlockOuter">
                                                    <tr>
                                                        <td class="mcnTextBlockInner" valign="top"
                                                            style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">

                                                            <table class="mcnTextContentContainer" align="left"
                                                                   border="0" width="600" cellpadding="0"
                                                                   cellspacing="0"
                                                                   style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                <tbody><tr>

                                                                    <td class="mcnTextContent"
                                                                        style="padding-top: 9px;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #808080;font-family: Helvetica;font-size: 10px;line-height: 125%;text-align: left;"
                                                                        valign="top">

                                                                        <div style="text-align: center;"><strong>Copyright <span
                                                                                class="st">©</span> 2015 TechSect Inc. All rights reserved.
                                                                        </strong></div>

                                                                    </td>
                                                                </tr>
                                                                </tbody></table>

                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table><table class="mcnFollowBlock" border="0" width="100%"
                                                               cellpadding="0" cellspacing="0"
                                                               style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                    <tbody class="mcnFollowBlockOuter">
                                                    <tr>
                                                        <td style="padding: 9px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                            class="mcnFollowBlockInner" align="center" valign="top">
                                                            <table class="mcnFollowContentContainer" border="0"
                                                                   width="100%" cellpadding="0" cellspacing="0"
                                                                   style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                <tbody><tr>
                                                                    <td style="padding-left: 9px;padding-right: 9px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                        align="center">
                                                                        <table style="background-color: #FAFAFA;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                               class="mcnFollowContent" border="0"
                                                                               width="100%" cellpadding="0"
                                                                               cellspacing="0">
                                                                            <tbody><tr>
                                                                                <td style="padding-top: 9px;padding-right: 9px;padding-left: 9px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                    align="center" valign="top">
                                                                                    <table border="0" cellpadding="0"
                                                                                           cellspacing="0"
                                                                                           style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                        <tbody><tr>
                                                                                            <td valign="top"
                                                                                                style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">

                                                                                                <table align="left"
                                                                                                       border="0"
                                                                                                       cellpadding="0"
                                                                                                       cellspacing="0"
                                                                                                       style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                                    <tbody><tr>
                                                                                                        <td style="padding-right: 10px;padding-bottom: 9px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                                            class="mcnFollowContentItemContainer"
                                                                                                            valign="top">
                                                                                                            <table class="mcnFollowContentItem"
                                                                                                                   border="0"
                                                                                                                   width="100%"
                                                                                                                   cellpadding="0"
                                                                                                                   cellspacing="0"
                                                                                                                   style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                                                <tbody><tr>
                                                                                                                    <td style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                                                        align="left"
                                                                                                                        valign="middle">
                                                                                                                        <table align="left"
                                                                                                                               border="0"
                                                                                                                               width=""
                                                                                                                               cellpadding="0"
                                                                                                                               cellspacing="0"
                                                                                                                               style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                                                            <tbody><tr>

                                                                                                                                <td class="mcnFollowIconContent"
                                                                                                                                    align="center"
                                                                                                                                    valign="middle"
                                                                                                                                    width="24"
                                                                                                                                    style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                                                                    <a href="https://www.facebook.com/clashmate99"
                                                                                                                                       target="_blank"
                                                                                                                                       style="word-wrap: break-word;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img
                                                                                                                                            src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png"
                                                                                                                                            style="display: block;border: 0;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;"
                                                                                                                                            class=""
                                                                                                                                            height="24"
                                                                                                                                            width="24">
                                                                                                                                    </a>
                                                                                                                                </td>

                                                                                                                            </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    </tbody></table>

                                                                                                <!--[if gte mso 6]>
								</td>
						    	<td align="left" valign="top">
								<![endif]-->



                                                                                                <table align="left"
                                                                                                       border="0"
                                                                                                       cellpadding="0"
                                                                                                       cellspacing="0"
                                                                                                       style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                                    <tbody><tr>
                                                                                                        <td style="padding-right: 10px;padding-bottom: 9px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                                            class="mcnFollowContentItemContainer"
                                                                                                            valign="top">
                                                                                                            <table class="mcnFollowContentItem"
                                                                                                                   border="0"
                                                                                                                   width="100%"
                                                                                                                   cellpadding="0"
                                                                                                                   cellspacing="0"
                                                                                                                   style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                                                <tbody><tr>
                                                                                                                    <td style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                                                        align="left"
                                                                                                                        valign="middle">
                                                                                                                        <table align="left"
                                                                                                                               border="0"
                                                                                                                               width=""
                                                                                                                               cellpadding="0"
                                                                                                                               cellspacing="0"
                                                                                                                               style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                                                            <tbody><tr>

                                                                                                                                <td class="mcnFollowIconContent"
                                                                                                                                    align="center"
                                                                                                                                    valign="middle"
                                                                                                                                    width="24"
                                                                                                                                    style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                                                                    <a href="https://twitter.com/clashmate_dev"
                                                                                                                                       target="_blank"
                                                                                                                                       style="word-wrap: break-word;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img
                                                                                                                                            src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png"
                                                                                                                                            style="display: block;border: 0;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;"
                                                                                                                                            class=""
                                                                                                                                            height="24"
                                                                                                                                            width="24">
                                                                                                                                    </a>
                                                                                                                                </td>

                                                                                                                            </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    </tbody></table>

                                                                                                <!--[if gte mso 6]>
								</td>
						    	<td align="left" valign="top">
								<![endif]-->



                                                                                                <table align="left"
                                                                                                       border="0"
                                                                                                       cellpadding="0"
                                                                                                       cellspacing="0"
                                                                                                       style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                                    <tbody><tr>
                                                                                                        <td style="padding-right: 0;padding-bottom: 9px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                                            class="mcnFollowContentItemContainer"
                                                                                                            valign="top">
                                                                                                            <table class="mcnFollowContentItem"
                                                                                                                   border="0"
                                                                                                                   width="100%"
                                                                                                                   cellpadding="0"
                                                                                                                   cellspacing="0"
                                                                                                                   style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                                                <tbody><tr>
                                                                                                                    <td style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"
                                                                                                                        align="left"
                                                                                                                        valign="middle">
                                                                                                                        <table align="left"
                                                                                                                               border="0"
                                                                                                                               width=""
                                                                                                                               cellpadding="0"
                                                                                                                               cellspacing="0"
                                                                                                                               style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                                                            <tbody><tr>

                                                                                                                                <td class="mcnFollowIconContent"
                                                                                                                                    align="center"
                                                                                                                                    valign="middle"
                                                                                                                                    width="24"
                                                                                                                                    style="mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                                                                    <a href="https://plus.google.com/100360560619328863082"
                                                                                                                                       target="_blank"
                                                                                                                                       style="word-wrap: break-word;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><img
                                                                                                                                            src="http://cdn-images.mailchimp.com/icons/social-block-v2/color-googleplus-48.png"
                                                                                                                                            style="display: block;border: 0;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;"
                                                                                                                                            class=""
                                                                                                                                            height="24"
                                                                                                                                            width="24">
                                                                                                                                    </a>
                                                                                                                                </td>

                                                                                                                            </tr>
                                                                                                                            </tbody>
                                                                                                                        </table>
                                                                                                                    </td>
                                                                                                                </tr>
                                                                                                                </tbody>
                                                                                                            </table>
                                                                                                        </td>
                                                                                                    </tr>
                                                                                                    </tbody></table>

                                                                                                <!--[if gte mso 6]>
								</td>
						    	<td align="left" valign="top">
								<![endif]-->

                                                                                            </td>
                                                                                        </tr>
                                                                                        </tbody></table>
                                                                                </td>
                                                                            </tr>
                                                                            </tbody></table>
                                                                    </td>
                                                                </tr>
                                                                </tbody></table>

                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <!-- // END FOOTER -->
                        </td>
                    </tr>
                </table>
                <!-- // END TEMPLATE -->
            </td>
        </tr>
    </table>
</center>
</body>
</html>