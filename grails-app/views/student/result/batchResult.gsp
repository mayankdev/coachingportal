<%@ page import="com.coachingPortal.coaching.Batch" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main" />
</head>

<body>
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <g:render template="/student/myCoaching/myBatchNavBar" model="[navBarSelectedItem:'Result']"/>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <g:render template="/common/errorAndMessage" />
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <h3>${(Batch.findById(Long.parseLong((String)(session.getAttribute('batchId'))))).name}->Result</h3>
                <hr>
                <g:if test="${resultSet.size()>0}">
                    <table class="table table-responsive table-hover">
                        <tr>
                            <th>#</th>
                            <th>Practice Paper Name</th>
                            <th>Subjects</th>
                            <th>Time Taken</th>
                            <th>Correct</th>
                            <th>Incorrect</th>
                            <th>Unattempted</th>
                            <th>Date</th>
                        </tr>
                        <tbody>
                        <g:each in="${resultSet}" var="result" status="i">
                            <tr>
                                <td>${i + 1}</td>
                                <td>${result?.practicePaper.examName}</td>
                                <td>${result?.practicePaper.practicePaperSubjects}</td>
                                <td>${result?.timeTaken}</td>
                                <td>${result?.questionsCorrect}</td>
                                <td>${result?.questionsIncorrect}</td>
                                <td>${result?.questionsUnattempted}</td>
                                <td><g:formatDate format="dd-MM-yyyy" date="${result?.dateCreated}"/></td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </g:if>
                <g:else>
                    <div class="alert alert-info">
                        <i class="fa fa-warning"></i> No exams given by you.
                    </div>
                </g:else>
            </div>
        </div>
    </div>

</div>
</body>
</html> 