
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main"/>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
</head>

<body>
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <g:render template="/student/myCoaching/myBatchNavBar" model="[navBarSelectedItem:'Result']"/>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <g:render template="/common/errorAndMessage" model="[someInstance:practicePaper]"/>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="well well-sm">
                <h3>Result - ${result?.practicePaper?.examName}<g:link controller="studentResult" action="allPapersResult" class="btn btn-primary pull-right">All Papers Result</g:link></h3>
              </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-5">
            <div class="well well-sm">
                <h4><b>Accuracy</b></h4>
                <hr>
                Correct-${result?.questionsCorrect}<br>
                InCorrect-${result?.questionsIncorrect}<br>
                UnAttempted-${result?.questionsUnattempted}<br>
                Time taken-${(Integer)(result?.timeTaken)/60000} Minutes
                ${((Integer)(result?.timeTaken)/1000)%60} Seconds
            </div>
        </div>
        <div class="col-lg-7">
            <div class="well well-sm">
                <h4><b>Overview</b></h4>
                <hr/>

                <div id="questionResponseChart"></div>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">
    google.load("visualization", "1", {packages: ["corechart"]});

    function drawQuestionResponseChart() {
        var data = google.visualization.arrayToDataTable([
            ['Response', 'Type'],
            ['Correct', ${correctPercent}],
            ['Incorrect', ${incorrectPercent}],
            ['Unattempted', ${unattemptedPercent}]
        ]);

        var options = {
            height: 300,
            width: 400,
            title: 'My Daily Activities',
            pieSliceTextStyle: {fontSize: 18, fontName: "'Ubuntu',sans-serif", bold: true},
            tooltip: {
                textStyle: {color: '#2c3e50', fontSize: 16, fontName: "'Ubuntu',sans-serif"},
                showColorCode: true
            },
            keepAspectRatio: false,
            legend: {alignment: 'center', position: 'bottom'},
            slices: [{color: '#3c763d'}, {color: '#a94442'}, {color: '#8a6d3b'}],
            chartArea: {left: '8%', top: '2%', width: "80%", height: '85%'}
        };
        var chart = new google.visualization.PieChart(document.getElementById('questionResponseChart'));
        chart.draw(data, options);
    }

    $(document).ready(function () {
        drawQuestionResponseChart();
    });

</script>
</body>
</html>