<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main" />
</head>

<body>
<div class="container">
    <g:if test="${flash.success}">
        <div class="alert alert-success">
            ${flash.success}
        </div>
    </g:if>
    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <g:render template="/student/myCoaching/myCoachingNavBar" model="[navBarSelectedItem:'Infocenter']"/>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <h3>Infocenter</h3>
                <hr>
                <g:if test="${!tasksList?.empty}">
                    <table class="table table-responsive table-hover">
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Date</th>

                        </tr>
                        <tbody>
                        <g:each in="${tasksList}" var="task" status="i">
                            <tr>
                                <td>${i + 1}</td>
                                <td><a href="${createLink(controller:'myCoaching',action: 'taskDescription',params: [taskId:task.id])}">${task?.title}</a></td>
                                <td><g:formatDate format="dd-MM-yyyy" date="${task?.lastUpdated}"/></td>

                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </g:if>
                <g:else>
                    <div class="alert alert-info">
                        <i class="fa fa-warning"></i> There are no news.
                    </div>
                </g:else>
            </div>
        </div>
    </div>

</div>
</body>
</html>