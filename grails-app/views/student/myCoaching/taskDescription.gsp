<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main" />
</head>

<body>
<div class="container">
    <g:if test="${flash.success}">
        <div class="alert alert-success">
            ${flash.success}
        </div>
    </g:if>
    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <g:render template="/student/myCoaching/myCoachingNavBar" model="[navBarSelectedItem:task.taskType]"/>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <h3>${task.taskType}<a href="${createLink(controller: 'myCoaching',action: task.taskType,params:[taskType:task.taskType])}" class="btn btn-sm btn-primary pull-right">Back</a></h3>
                <hr>
                <div class="row">
                    <div class="col-lg-9">${task.title}</div>
                    <div class="col-lg-3"><strong>Date&nbsp;</strong>
                        ${task.lastUpdated}</div>
                </div>
                <div class="space20"></div>
                <div class="row">
                    <div class="col-lg-12">${task.description}</div>
                </div>
                %{--<strong>Title</strong>
                ${task.title}
                <strong>Description</strong>
                ${task.description}
                <strong>Date</strong>
                ${task.lastUpdated}--}%
            </div>
        </div>
    </div>

</div>
</body>
</html> 