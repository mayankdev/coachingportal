<div class="row">
    <strong>

        <div class="col-lg-3 text-center">
            <g:if test="${navBarSelectedItem == 'PracticePaper'}">
                <a style="color: #000000;">Practice Papers</a>
            </g:if>
            <g:else>
                <a href="${createLink(controller: 'studentPracticePaper', action: 'index')}">Practice Papers</a>
            </g:else>
        </div>

        <div class="col-lg-3 text-center">
            <g:if test="${navBarSelectedItem == 'StudyMaterials'}">
                <a style="color: #000000;">Study Materials</a>
            </g:if>
            <g:else>
                <a href="${createLink(controller: 'studentStudyMaterial', action: 'index')}">Study Materials</a>
            </g:else>
        </div>

        <div class="col-lg-3 text-center">
            <g:if test="${navBarSelectedItem == 'TimeTable'}">
                <a style="color: #000000;">Time table</a>
            </g:if>
            <g:else>
                <a href="${createLink(controller: 'studentTimetable', action: 'index')}">Time table</a>
            </g:else>
        </div>

        <div class="col-lg-3 text-center">
            <g:if test="${navBarSelectedItem == 'Result'}">
                <a style="color: #000000;">Result</a>
            </g:if>
            <g:else>
                <a href="${createLink(controller: 'studentResult', action: 'batchResult')}">Result</a>
            </g:else>
        </div>
</strong>
</div>