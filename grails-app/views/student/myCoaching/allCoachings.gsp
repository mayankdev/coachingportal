<html>
<head>
    <title></title>
    <meta name="layout" content="main" />
</head>

<body>
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <strong>Welcome <sec:loggedInUserInfo field="username"/></strong>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <g:render template="/common/errorAndMessage" />
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <h3>My Institutes</h3>
                <hr>
                <g:if test="${subscribeUserList.size()>0}">
                    <table class="table table-responsive table-hover">
                        <tr>
                            <th>#</th>
                            <th>Institute Name</th>
                            <th>Batch Name</th>
                            <th>Joined On</th>
                        </tr>
                        <tbody>
                        <g:each in="${subscribeUserList}" var="subscribeUser" status="i">
                            <tr>
                                <td>${i + 1}</td>
                                <td><g:link controller="studentInfocenter" action="index" params="[coachingId:subscribeUser.batch.coaching.id]">${subscribeUser.batch.coaching.name}</g:link></td>
                                <td>${subscribeUser.batch.name}</td>
                                <td>${subscribeUser.dateCreated}</td>

                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </g:if>
                <g:else>
                    <div class="alert alert-info">
                        <i class="fa fa-warning"></i> Time table not updated by coaching.
                    </div>
                </g:else>
            </div>
        </div>
    </div>

</div>
</body>
</html> 