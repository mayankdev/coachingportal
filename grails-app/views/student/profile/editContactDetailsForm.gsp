<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Edit Contact Details | ClashMate</title>
        <meta name="keywords" content="">
    <meta name="description" content="">

    <meta name="layout" content="main" />

</head>

<body>
<div class="space40"></div>
<div class="container">

    <g:render template="/student/profile/profileCommon" />

    <div class="well well-sm">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h4 class="no-margin">Contact Details</h4>
            </div>
        </div>
        <hr class="h2_horizontal_line"/>

        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <form name="contactDetailForm"
                      action="${createLink(controller: 'studentProfile', action: 'submitContactDetailsForm')}">
                    <input type="hidden" name="contactId" value="${contactDetailsInstance?.uniqueId}"/>

                    <div class="row">
                        <div class="form-group col-lg-6 ${hasErrors(bean: contactDetailsInstance, field: "houseNo", 'has-error')}">
                            <label for="houseNo">House No</label>
                            <input type="text" name="houseNo" id="houseNo" class="form-control"
                                   value="${contactDetailsInstance?.houseNo}">
                        </div>

                        <div class="form-group col-lg-6 ${hasErrors(bean: contactDetailsInstance, field: "street", 'has-error')}">
                            <label for="street">Street</label>
                            <input type="text" name="street" id="street" class="form-control"
                                   value="${contactDetailsInstance?.street}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-lg-6 ${hasErrors(bean: contactDetailsInstance, field: "locality", 'has-error')}">
                            <label for="locality">Locality</label>
                            <input type="text" name="locality" id="locality" class="form-control"
                                   value="${contactDetailsInstance?.locality}">
                        </div>

                        <div class="form-group col-lg-6 ${hasErrors(bean: contactDetailsInstance, field: "city", 'has-error')}">
                            <label for="city">City</label>
                            <input type="text" name="city" id="city" class="form-control"
                                   value="${contactDetailsInstance?.city}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-lg-6 ${hasErrors(bean: contactDetailsInstance, field: "postalCode", 'has-error')}">
                            <label for="postalCode">Postal Code</label>
                            <input type="text" name="postalCode" id="postalCode" class="form-control"
                                   value="${contactDetailsInstance?.postalCode}">
                        </div>

                        <div class="form-group col-lg-6 ${hasErrors(bean: contactDetailsInstance, field: "state", 'has-error')}">
                            <label for="state">State</label>
                            <input type="text" name="state" id="state" class="form-control"
                                   value="${contactDetailsInstance?.state}">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-lg-6">
                            <input type="submit" class="btn btn-clash btn-block" value="Submit">
                        </div>

                        <div class="form-group col-lg-6">
                            <input type="reset" class="btn btn-default btn-block" value="Reset">
                        </div>
                    </div>

                </form>


            </div>
        </div>

    </div>
</div>
    <div class="space60"></div>
</body>
</html>
    