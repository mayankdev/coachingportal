<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Contact Details | ClashMate</title>
        <meta name="keywords" content="">
    <meta name="description" content="">

    <meta name="layout" content="main" />

</head>

<body>
<div class="space40"></div>
<div class="container">
    <g:render template="/common/errorAndMessage"/>
    <g:render template="/student/profile/profileCommon" />

    <div class="well well-sm">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h4 class="no-margin">Contact Details&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <g:if test="${contactDetailsInstance}">
                <g:link controller="studentProfile" action="editContactDetailsForm" class="btn btn-clash">Edit Contact Details</g:link>
                </g:if>
                <g:else>
                    <g:link controller="studentProfile" action="editContactDetailsForm" class="btn btn-clash">Add Contact Details</g:link>

                </g:else>
                </h4>
        </div>
    </div>
    <hr class="h2_horizontal_line"/>

    <div class="row">
        <div class="col-lg-10 text-center col-lg-offset-1">
            <g:if test="${contactDetailsInstance}">
            <div class="row">
                <div class="form-group col-lg-6">
                    <label>House No</label>
                    <p class="cp-highlighted-text">${contactDetailsInstance.houseNo}</p>
                </div>

                <div class="form-group col-lg-6">
                    <label>Street</label>
                    <p class="cp-highlighted-text">${contactDetailsInstance.street}</p>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-lg-6">
                    <label>Locality</label>
                    <p class="cp-highlighted-text">${contactDetailsInstance.locality}</p>
                </div>
                <div class="form-group col-lg-6">
                    <label>City</label>
                    <p class="cp-highlighted-text">${contactDetailsInstance.city}</p>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-lg-6">
                    <label>Postal Code</label>
                    <p class="cp-highlighted-text">${contactDetailsInstance.postalCode}</p>
                </div>
                <div class="form-group col-lg-6">
                    <label>State</label>
                    <p class="cp-highlighted-text">${contactDetailsInstance.state}</p>
                </div>
            </div>
            </g:if>
            <g:else>
                <div class="row well well-sm">
                    <div class="col-lg-12 text-center">
                        Please provide your contact details, you have not provided yet.
                        <g:link controller="studentProfile" action="editContactDetailsForm" class="btn btn-clash" >Add Contact Details</g:link>

                    </div>
                </div>

            </g:else>

        </div>
    </div>
  </div>
</div>
<div class="space60"></div>
</body>
</html>
    