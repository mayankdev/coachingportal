<html>
<head>
    <title>Profile </title>
        <meta name="keywords" content="">
    <meta name="description" content="">

    <meta name="layout" content="main" />

</head>

<body>
<div class="space40"></div>
<div class="container">

    <g:render template="/common/errorAndMessage"/>
    <g:render template="/student/profile/profileCommon" />

    <div class="well well-sm">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h4 class="no-margin">Personal Details&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<g:link controller="studentProfile" action="editPersonalDetailsForm" class="btn btn-clash">Edit Profile</g:link></h4>
        </div>
    </div>
    <hr class="h2_horizontal_line"/>

    <div class="row">
        <div class="col-lg-2 text-center">
         <img src="${resource(dir: 'images',file: 'author-placeholder.jpg')}" style="border: 2px solid #ffffff;">
        </div>
        <div class="col-lg-10 text-center">
            <div class="row">
                <div class="form-group col-lg-6">
                    <label>Firstname</label>
                    <g:if test="${studentInstance?.firstName}">
                        <p class="cp-highlighted-text">${studentInstance?.firstName}</p>
                    </g:if>
                    <g:else>
                        <p class="cp-highlighted-text">None</p>
                    </g:else>

                </div>

                <div class="form-group col-lg-6">
                    <label>Lastname</label>
                    <g:if test="${studentInstance?.lastName}">
                        <p class="cp-highlighted-text">${studentInstance?.lastName}</p>
                    </g:if>
                    <g:else>
                        <p class="cp-highlighted-text">None</p>
                    </g:else>

                </div>
            </div>

            <div class="row">
                <div class="form-group col-lg-6">
                    <label>Contact Number</label>

                    <g:if test="${studentInstance?.contactNo}">
                        <p class="cp-highlighted-text">${studentInstance?.contactNo}</p>
                    </g:if>
                    <g:else>
                        <p class="cp-highlighted-text">None</p>
                    </g:else>

                </div>


            </div>

        </div>
    </div>
</div>
</div>
<div class="space60"></div>
</body>
</html>
    