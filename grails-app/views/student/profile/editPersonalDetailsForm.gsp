<html>
<head>
    <title>Edit Personal Details | ClashMate</title>
        <meta name="keywords" content="discussion forum ,user profile,interview story,aptitude practice,reasoning practice,english practice,online test,news,jobs,competition‬,competitive exam preparation,ibps preparation,banking preparation,banking exam,bank preparation,‎ibps,‎sbi po,‎ibps po‬,ibps clerk‬,ibps rrb,ibps specialist,‎online exam‬,‎bank exam‬,‎sbi clerk,‎ssc‬,‎civil services‬,‎ssc">
    <meta name="description" content="ClashMate shows  you your profile details in most elegant way so that you can easily update or can see them when you want to.">


    <meta name="layout" content="main" />

</head>

<body>
<div class="space40"></div>
<div class="container">

<g:render template="/common/errorAndMessage" model="[someInstance:studentInstance]"/>
    <g:render template="/student/profile/profileCommon" />

    <div class="well well-sm">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h4 class="no-margin">Personal Details</h4>
        </div>
    </div>
    <hr class="h2_horizontal_line"/>

    <div class="row">
        <div class="col-lg-2 text-center">
            <img src="${resource(dir: 'images',file: 'author-placeholder.jpg')}" style="border: 2px solid #ffffff;">
        </div>
        <div class="col-lg-10 text-center">

            <form name="personalDetailsForm"
                  action="${createLink(controller: 'studentProfile', action: 'submitPersonalDetailsForm')}">

                <input type="hidden" name="studentId" value="${studentInstance.uniqueId}">
                <div class="row">
                    <div class="form-group col-lg-6 ${hasErrors(bean: studentInstance, field: "firstName", 'has-error')}">
                        <label for="firstName">First Name</label>
                        <input type="text" name="firstName" id="firstName" class="form-control"
                               value="${studentInstance?.firstName}">
                    </div>

                    <div class="form-group col-lg-6 ${hasErrors(bean: studentInstance, field: "lastName", 'has-error')}">
                        <label for="lastName">Last Name</label>
                        <input type="text" name="lastName" id="lastName" class="form-control"
                               value="${studentInstance?.lastName}">
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-lg-6 ${hasErrors(bean: studentInstance, field: "contactNo", 'has-error')}">
                        <label for="contactNo">Contact Number</label>
                        <input type="text" name="contactNo" id="contactNo" class="form-control"
                               value="${studentInstance?.contactNo}">
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <input type="submit" class="btn btn-primary btn-block" value="Submit">
                    </div>

                    <div class="col-lg-6">
                        <input type="reset" class="btn btn-default btn-block" value="Reset">
                    </div>
                </div>
            </form>
        </div>
    </div>
    </div>
</div>
<div class="space60"></div>
</body>
</html>
    