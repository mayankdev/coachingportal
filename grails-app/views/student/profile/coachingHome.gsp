<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Welcome to Coaching Portal</title>

</head>
<body>
<div class="container">
    <g:render template="/common/errorAndMessage"/>
    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <strong>Welcome <sec:loggedInUserInfo field="username"/></strong>
            </div>
         </div>
     </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">Practice Papers</h3>
                            </div>
                            <div class="panel-body">
                                Practice Papers
                                %{--<g:if test="${}">

                                </g:if>
                                <g:else>
                                    <div class="alert alert-info"></div>
                                </g:else>--}%
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">Time Table</h3>
                            </div>
                            <div class="panel-body">
                                <g:if test="${timeTableList}">
                                    <table class="table table-responsive">
                                        <g:each in="${timeTableList}" var="timetable">
                                            <tr><td>
                                                ${timetable.title}
                                            </td></tr>
                                        </g:each>
                                    </table>
                                    <a href="${createLink(controller: 'myCoaching', action: 'timeTable')}" class="pull-right">more</a>
                                </g:if>
                                <g:else>
                                    <div class="alert alert-info">
                                        No timetable updated
                                    </div>
                                </g:else>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">Infocenter</h3>
                            </div>
                            <div class="panel-body">
                                <g:if test="${infocenterList}">
                                    <table class="table table-responsive">
                                    <g:each in="${infocenterList}" var="infocenter">
                                        <tr><td>
                                        ${infocenter.title}
                                        </td></tr>
                                    </g:each>
                                    </table>
                                    <a href="${createLink(controller: 'myCoaching', action: 'infocenter')}" class="pull-right">more</a>
                                </g:if>
                                <g:else>
                                    <div class="alert alert-info">
                                        No news are there
                                    </div>
                                </g:else>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-8">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">Progress</h3>
                            </div>
                            <div class="panel-body">
                                Progress
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">Holidays</h3>
                            </div>
                            <div class="panel-body">
                                <g:if test="${holidayList}">
                                    <table class="table table-responsive">
                                    <g:each in="${holidayList}" var="holiday">
                                        <tr><td>
                                        ${holiday.title}
                                        </td></tr>
                                    </g:each>
                                    </table>
                                    <a href="${createLink(controller: 'myCoaching', action: 'holidays')}" class="pull-right">more</a>
                                </g:if>
                                <g:else>
                                    <div class="alert alert-warning">
                                        No Holidays are there.
                                    </div>
                                </g:else>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>




</div>
</body>
</html>
