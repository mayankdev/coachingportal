<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main" />
</head>

<body>
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <g:render template="/student/myCoaching/myCoachingNavBar" model="[navBarSelectedItem:'Batches']"/>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <g:render template="/common/errorAndMessage"/>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <h3>Batches</h3>
                <hr>
                <g:if test="${batchList?.size()>0}">
                    <table class="table table-responsive table-hover">
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Practice Paper</th>
                            <th>Study Material</th>
                            <th>Timetable</th>
                            <th>Result</th>
                        </tr>
                        <tbody>
                        <g:each in="${batchList}" var="batch" status="i">
                            <tr>
                                <td>${i + 1}</td>
                                <td>${batch?.name}</td>
                                <td><g:link controller="studentPracticePaper" action="index" params="[batchId:batch.id]">${batch?.practicePapers.size()}</g:link></td>
                                <td><g:link controller="studentStudyMaterial" action="index" params="[batchId:batch.id]">${batch?.studyMaterials.size()}</g:link></td>
                                <td><g:link controller="studentTimetable" action="index" params="[batchId:batch.id]">${batch?.timetable}</g:link></td>
                                <td><g:link controller="studentResult" action="batchResult" params="[batchId:batch.id]"><i class="fa fa-file-text"></i></g:link></td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </g:if>
                <g:else>
                    <div class="alert alert-info">
                        <i class="fa fa-warning"></i> There are no batches.
                    </div>
                </g:else>
            </div>
        </div>
    </div>

</div>
</body>
</html>