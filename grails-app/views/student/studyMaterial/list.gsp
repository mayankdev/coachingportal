<%@ page import="com.coachingPortal.coaching.Batch" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main"/>
</head>

<body>
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <g:render template="/student/myCoaching/myBatchNavBar"
                          model="[navBarSelectedItem: 'StudyMaterials']"/>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <g:render template="/common/errorAndMessage" />
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <h3>${(Batch.findById(Long.parseLong((String)(session.getAttribute('batchId'))))).name}->Study Material</h3>
                <hr>

                <g:if test="${studyMaterialList.size()>0}">
            <table class="table table-responsive">
                        <thead>
                        <tr>
                            <g:sortableColumn property="name" title="Name"/>
                            <th>Download</th>
                            <th>Last Updated</th>

                        </tr>
                        </thead>
                        <tbody>
                        <g:each in="${studyMaterialList}" status="i" var="studyMaterialInstance">
                            <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                                <td>${studyMaterialInstance.name}</td>
                                <td><g:link controller="studentStudyMaterial" action="downloadFile" params="[smId:studyMaterialInstance.smUniqueId]"><i class="fa fa-download"></i></g:link></td>
                                <td>${studyMaterialInstance.lastUpdated}</td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                 </g:if>
                <g:else>
                    <div class="alert alert-info">
                        <i class="fa fa-warning"></i> There are no study materials added.
                    </div>
                </g:else>
                </div>
            </div>
        </div>
    </div>

</div>
</body>
</html>