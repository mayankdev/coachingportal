<input type="button" class="btn  btn-sm ${questionNo-1?'':'disabled'}"  onclick="nxtOrPreQuestion('${subjectType}','${questionNo}','Previous')" value="Previous">

<input type="button" class="btn  btn-sm" onclick="cReset()" value="Reset">

<input type="button" class="btn  btn-sm ${(questionNo)%questionListSize?'':'disabled'}" onclick="nxtOrPreQuestion('${subjectType}','${questionNo}','Next')" value="Next">

%{--
<input type="button" class="btn  btn-sm " onclick="reviewAndNxt('${subjectType}','${questionNo}')" value="Review & Next">
--}%
