<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main"/>

    <script type="text/javascript">


        //Code for review and next start
        function reviewAndNxt(subjectType,questionNo){
            var ax_questionNo=parseInt(questionNo)+1
            var submittedQuestionUID = $("#questionUID").val();
            var selectedAnswer = $('input:radio[name=queAnswer]:checked').val();

            $.ajax({
                type: "POST",
                url: "${createLink(controller: 'studentProfile',action: 'ax_getSelectedQuestion')}",
                data: {subjectType: subjectType, questionNo: ax_questionNo,submittedQuestionUID:submittedQuestionUID,selectedAnswer:selectedAnswer,markedReview:'markedReview'}
            }).done(function (data) {
                if (data.result == 'SUCCESS') {
                    $("#questionTemplateId").html(data.subjectTypeQuestion);

                    $("#questionNxtPreId").html(data.nxtOrPreQuestion);
                    /*$("#questionSerialNoId").html(data.questionList);*/


                } else {
                    alert(data.errMsg);
                }
            });
        }
        //Code for review and next end

        //Code to get next or previous question start
        function nxtOrPreQuestion(subjectType,questionNo,NxtOrPre){

            var ax_questionNo

            var submittedQuestionUID = $("#questionUID").val();
            var selectedAnswer = $('input:radio[name=queAnswer]:checked').val();

            if(NxtOrPre=='Next'){
                ax_questionNo=parseInt(questionNo)+1
                $("#questionSerialNo"+questionNo).removeClass('active');
            }else{
                ax_questionNo=parseInt(questionNo)-1
                $("#questionSerialNo"+questionNo).removeClass('active');
            }

            if(selectedAnswer==null){
                $("#questionSerialNo"+questionNo).addClass('btn-danger');
            }else{
                $("#questionSerialNo"+questionNo).addClass('btn-success');
            }
            $("#questionSerialNo"+ax_questionNo).addClass('active');



            $.ajax({
                type: "POST",
                url: "${createLink(controller: 'studentPracticePaper',action: 'ax_getSelectedQuestion')}",
                data: {subjectType: subjectType, questionNo: ax_questionNo,submittedQuestionUID:submittedQuestionUID,selectedAnswer:selectedAnswer}
            }).done(function (data) {
                if (data.result == 'SUCCESS') {
                    $("#questionTemplateId").html(data.subjectTypeQuestion);

                    $("#questionNxtPreId").html(data.nxtOrPreQuestion);
                    /*$("#questionSerialNoId").html(data.questionList);*/


                } else {
                    alert(data.errMsg);
                }
            });




        }
        //Code to get next or previous question end


        //Code to reset selected radio button start
        function cReset() {
            $('input[name=queAnswer]').attr('checked', false);
        }
        //Code to reset selected radio button end


        //Code to display question numbersList start
        function displaySelectedQuestion(subjectType, questionNo) {
            var submittedQuestionUID = $("#questionUID").val();
            var selectedAnswer = $('input:radio[name=queAnswer]:checked').val();

            $.ajax({
                type: "POST",
                url: "${createLink(controller: 'studentPracticePaper',action: 'ax_getSelectedQuestion')}",
                data: {subjectType: subjectType, questionNo: questionNo,submittedQuestionUID:submittedQuestionUID,selectedAnswer:selectedAnswer}
            }).done(function (data) {
                if (data.result == 'SUCCESS') {
                    $("#questionTemplateId").html(data.subjectTypeQuestion);
                    $("#questionNxtPreId").html(data.nxtOrPreQuestion);
                    /*$("#questionSerialNoId").html(data.questionList);*/


                } else {
                    alert(data.errMsg);
                }
            });
        }
        //Code to display question numbersList end

        //Code to change subject start
        function changeSubject(subjectType, subjectTypeList) {
            var submittedQuestionUID = $("#questionUID").val();
            var selectedAnswer = $('input:radio[name=queAnswer]:checked').val();

            $.ajax({
                type: "POST",
                url: "${createLink(controller: 'studentPracticePaper',action: 'ax_getSelectedSubject')}",
                data: {subjectType: subjectType,submittedQuestionUID:submittedQuestionUID,selectedAnswer:selectedAnswer}
            }).done(function (data) {
                if (data.result == 'SUCCESS') {
                    $("#questionTemplateId").html(data.subjectTypeQuestion);
                    $("#questionSerialNoId").html(data.questionList);
                    $("#questionNxtPreId").html(data.nxtOrPreQuestion);

                } else {
                    alert(data.errMsg);
                }
            });

            var subjectTypeArray = jQuery.parseJSON(subjectTypeList);
            for (var i = 0; i < subjectTypeArray.length; i++) {

                if (subjectTypeArray[i] == subjectType) {
                    document.getElementById(subjectTypeArray[i]).className = "btn btn-sm btn-success";
                } else {
                    document.getElementById(subjectTypeArray[i]).className = "btn btn-sm btn-default";
                }
            }

        }
        //Code to change subject end
        //Code to handle timer start
        //Code to handle timer on test page
        var settimmer = 0;
        $(function () {
            window.setInterval(function () {
                var timeCounter = $("b[id=show-time]").html();
                var timeleft = timeCounter.split(':')

                /*alert('Minutes-'+timeleft[0])
                 alert('Second-'+timeleft[1])
                 */
                if (timeleft[1] == 0) {
                    timeleft[0] = timeleft[0] - 1
                    timeleft[1] = 59
                } else {
                    timeleft[1] = timeleft[1] - 1
                }
                /*var updateTime = eval(timeCounter)- eval(1);*/

                $("b[id=show-time]").html(timeleft[0] + ':' + timeleft[1]);

                if (timeleft[1] == 0 && timeleft[0] == 0) {
                    var questionUID = $("#questionUID").val();
                    /*window.location = ("https://www.google.co.in/?gfe_rd=cr&ei=1HsBVZHzIcLM8geL4IDABg&gws_rd=ssl");*/
                    submitExam(questionUID);
                }
            }, 1000);

        });
        //Code to handle timer end

        //Code to submit exam start
        function submitExam() {
            var submittedQuestionUID = $("#questionUID").val();
            var practicePaperUniqueId = $("#practicePaperUniqueId").val();
            var selectedAnswer = $('input:radio[name=queAnswer]:checked').val();

            window.location.href = "${createLink(controller:'studentResult',action:'index')}?submittedQuestionUID=" + submittedQuestionUID + "&selectedAnswer=" + selectedAnswer+"&practicePaperUniqueId=" + practicePaperUniqueId ;
        }
        //Code to submit exam end


    </script>

</head>

<body>
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <div class="well well-sm">
                <div class="row">
                    <div class="col-lg-8">
                        <h2>${practicePaper.examName}</h2>
                    </div>

                    <div class="col-lg-4">
                        <div id="my-timer" class="pull-right" style="font-size:20px; ">
                            Time remaining <i class="fa fa-clock-o"></i> <b id="show-time">${timeLeft}</b> Minutes
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <input type="hidden" name="practicePaperUniqueId" id="practicePaperUniqueId" value="${practicePaper.uniqueId}">
            <g:each in="${subjectTypeList}" var="examSubject" status="i">
                <div class="col-lg-3">

                    <g:if test="${i == 0}">
                        <button onclick="changeSubject('${examSubject}', '${subjectTypeJsonList}')"
                                class="btn btn-sm btn-success" id="${examSubject}">${examSubject}</button>
                    </g:if>
                    <g:else>
                        <button onclick="changeSubject('${examSubject}', '${subjectTypeJsonList}')"
                                class="btn btn-sm btn-default" id="${examSubject}">${examSubject}</button>
                    </g:else>
                </div>
            </g:each>

        </div>
    </div>

    <div class="space20"></div>

    <div class="row">
        <div class="col-lg-12">

            <div class="col-lg-8">
                <div class="well">
                    <div id="questionTemplateId">
                        <g:render template="/student/practicePaper/questionTemplate" model="[question: question]"/>
                    </div>
                    <div class="space40"></div>

                    <div id="questionNxtPreId">
                    <g:render template="/student/practicePaper/questionNxtPreTemplate" model="[subjectType:subjectTypeList.get(0),questionNo:1,questionListSize:questionList.size(),questionUniqueId:question.uniqueId]"/>
                    </div>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="well">
                    <strong>Question No<button onclick="submitExam()" class="btn btn-sm btn-primary pull-right">Finish</button></strong>
                    <hr class="horizontal_line">

                    <div id="questionSerialNoId">
                        <g:render template="/student/practicePaper/questionSerialNo"
                                  model="[questionList: questionList, subjectType: subjectTypeList.get(0),questionStatusMap:questionStatusMap]"/>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

</body>
</html>