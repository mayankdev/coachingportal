
<%@ page import="com.coachingPortal.result.Result; com.coachingPortal.coaching.Batch" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main" />
</head>

<body>
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <g:render template="/student/myCoaching/myBatchNavBar" model="[navBarSelectedItem:'PracticePaper']"/>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <g:render template="/common/errorAndMessage" />
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <h3>${(Batch.findById(Long.parseLong((String)(session.getAttribute('batchId'))))).name}->Practice Papers</h3>
                <hr>
                <g:if test="${!practicePaperStatusMap.keySet()?.empty}">
                  <table class="table table-responsive table-hover">
                      <tr>
                      <th>S.No</th>
                      <th>Practice Paper name</th>
                      <th>Duration</th>
                      <th>Practice Paper subjects</th>
                      <th>Total Questions</th>
                      <th>Action</th>
                      </tr>
                      <tbody>
                      <g:each in="${practicePaperStatusMap.keySet()}" var="practicePaper" status="i">
                          <tr>
                              <td>${i + 1}</td>
                              <td>${practicePaper?.examName}</td>
                              <td>${practicePaper?.totalTime}</td>
                              <td>${practicePaper?.practicePaperSubjects}</td>
                              <td>${practicePaper?.totalQuestions}</td>
                              <td>
                                  <g:if test="${practicePaperStatusMap.get(practicePaper)=='Attempted'}">
                                      <a href="${createLink(controller:'studentResult',action: 'renderResult',params: [resultUniqueId:(Result.findByPracticePaper(practicePaper)).uniqueId] )}">Result</a>
                                  </g:if>
                                  <g:else>
                                      <a href="${createLink(controller:'studentPracticePaper',action: 'instruction',params: [practicePaperId:practicePaper.id] )}">Take Test</a>
                                  </g:else>
                                  </td>
                          </tr>
                      </g:each>
                      </tbody>
                  </table>
                </g:if>
                <g:else>
                    <div class="alert alert-danger">
                    No practice papers are added or in published state.
                    </div>
                </g:else>
            </div>
        </div>
    </div>


</div>
</body>
</html>