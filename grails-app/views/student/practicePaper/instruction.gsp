<%--
  Created by IntelliJ IDEA.
  User: MAYANK DWIVEDI
  Date: 6/6/2015
  Time: 4:59 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main" />
</head>

<body>
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <h2>${practicePaper.examName}</h2>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">

                <h3 >Instructions<a href="${createLink(controller: 'studentPracticePaper', action: 'test', params: [practicePaperId: practicePaper.id])}"
                                                                class="btn btn-success pull-right">Start Test</a></h3>
                <div class="space20"></div>
                <hr class="horizontal_line"/>
                <div class="space20"></div>
                <strong>
                <ul >
                    <li>Total time allowed for test is ${practicePaper.totalTime} minutes.</li>
                    <li>Test comprises of ${practicePaper.totalQuestions} questions.</li>
                    <li>Test is divided into ${(practicePaper.practicePaperSubjects).size()} sections , namely ${(practicePaper?.practicePaperSubjects)}.</li>
                    <li>You can finish the test at any time, if you don't submit  the test then it will automatically be submitted when the time expires.</li>
                    <li>Don't try to use any unfair means otherwise your account will be blocked. </li>
                    <li>You won't be able to pause test,start test only when you are sure to give it.</li>
                    <li>Don't refresh the page.</li>
                </ul>
                </strong>
            </div>
        </div>
    </div>


</div>
</body>
</html>