<g:each in="${questionList}" var="question"  status="i">
    <g:if test="${questionStatusMap.get(question.uniqueId)==''}">
        <button onclick="displaySelectedQuestion('${subjectType}','${i+ 1}')" id="questionSerialNo${i+1}">${i+1}</button>
    </g:if>
    <g:elseif test="${questionStatusMap.get(question.uniqueId)=='passed'}">
        <button class="btn-danger" onclick="displaySelectedQuestion('${subjectType}','${i+1}')" id="questionSerialNo${i+1}">${i+1}</button>
    </g:elseif>
    %{--<g:elseif test="${questionStatusMap.get(question.uniqueId)=='review'}">
        <button class="btn-info" onclick="displaySelectedQuestion('${subjectType}','${i+1}')" id="questionSerialNo${i+1}">${i+1}</button>
    </g:elseif>--}%
    <g:else>
        <button class="btn-success" onclick="displaySelectedQuestion('${subjectType}','${i+1}')" id="questionSerialNo${i+1}">${i+1}</button>
    </g:else>

</g:each>