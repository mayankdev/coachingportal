

        ${question.questionText}
        <input type="hidden" name="questionUID" id="questionUID" value="${question?.uniqueId}" />
        <div class="space20"></div>
        <div class="row">
            <div class="col-lg-6">
                <g:radio name="queAnswer" value="${question.answer.answerText1}"
                         checked="${markedAnswer == question.answer.answerText1}"/> <span
                    class="answer-text">${question.answer.answerText1}</span>
            </div>

            <div class="col-lg-6">
                <g:radio name="queAnswer" value="${question.answer.answerText2}"
                         checked="${markedAnswer == question.answer.answerText2}"/> <span
                    class="answer-text">${question.answer.answerText2}</span>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <g:radio name="queAnswer" value="${question.answer.answerText3}"
                         checked="${markedAnswer == question.answer.answerText3}"/> <span
                    class="answer-text">${question.answer.answerText3}</span>
            </div>

            <div class="col-lg-6">
                <g:radio name="queAnswer" value="${question.answer.answerText4}"
                         checked="${markedAnswer == question.answer.answerText4}"/> <span
                    class="answer-text">${question.answer.answerText4}</span>
            </div>

        </div>

