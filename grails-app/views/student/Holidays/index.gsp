<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main" />
</head>

<body>
<div class="container">
    <g:render template="/common/errorAndMessage" model="[someInstance:someInstance]"/>
    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <g:render template="/student/myCoaching/myCoachingNavBar" model="[navBarSelectedItem:'Holidays']"/>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <h3>Holidays</h3>
                <hr>
                <g:if test="${holidaysInstanceList?.size()>0}">
                    <table class="table table-responsive table-hover">
                        <tr>
                            <th>#</th>
                            <th>Date of Holiday</th>
                            <th>Holiday</th>

                        </tr>
                        <tbody>
                        <g:each in="${holidaysInstanceList}" var="task" status="i">
                            <tr>
                                <td>${i + 1}</td>
                                <td><g:formatDate format="dd-MM-yyyy" date="${task?.dateOfHoliday}"/></td>
                                <td>${task?.title}</td>

                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </g:if>
                <g:else>
                    <div class="alert alert-info">
                        <i class="fa fa-warning"></i> There are no holidays added.
                    </div>
                </g:else>
            </div>
        </div>
    </div>

</div>
</body>
</html>