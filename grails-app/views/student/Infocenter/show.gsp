<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main"/>
</head>

<body>
<div class="container">
    <g:render template="/common/errorAndMessage" model="[someInstance:infocenterInstance]"/>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <g:render template="/student/myCoaching/myCoachingNavBar" model="[navBarSelectedItem:'Infocenter']"/>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <h3>Infocenter<a href="${createLink(controller: 'coachingInfocenter',action: 'index')}" class="btn btn-sm btn-primary pull-right">Back</a></h3>
                <hr>
                <div class="row">
                    <div class="col-lg-9">${infocenterInstance.title}</div>
                    <div class="col-lg-3"><strong>Date&nbsp;</strong>
                        ${infocenterInstance.lastUpdated}</div>
                </div>
                <div class="space20"></div>
                <div class="row">
                    <div class="col-lg-12 paraBreak">${infocenterInstance.description}</div>
                </div>

            </div>
        </div>
    </div>

</div>
</body>
</html> 