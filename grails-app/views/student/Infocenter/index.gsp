<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main" />
</head>

<body>
<div class="container">
    <g:render template="/common/errorAndMessage" model="[someInstance:someInstance]"/>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <g:render template="/student/myCoaching/myCoachingNavBar" model="[navBarSelectedItem:'Infocenter']"/>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12 well">
            <h3>Infocenter</h3>
                <hr>
                <g:if test="${infocenterInstanceList?.size()>0}">
                    <table class="table table-responsive table-hover">
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Date</th>
                            <th>Description</th>
                        </tr>
                        <tbody>
                        <g:each in="${infocenterInstanceList}" var="task" status="i">
                            <tr>
                                <td>${i + 1}</td>
                                <td><g:link controller="studentInfocenter" action="show" id="${task.id}">${task?.title}</g:link></td>
                                <td><g:formatDate format="dd-MM-yyyy" date="${task?.lastUpdated}"/></td>
                                <td>${task?.getShortDescription()}</td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </g:if>
                <g:else>
                    <div class="alert alert-info">
                        <i class="fa fa-warning"></i> There is no information added.
                    </div>
                </g:else>
          </div>
    </div>

</div>
</body>
</html>