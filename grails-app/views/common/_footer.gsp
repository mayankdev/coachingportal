<style type="text/css">
.footerBackground {
    padding-top: 20px;
    padding-bottom: 40px;
    background-color: #222222;

}
.lastFooterBackground{
    padding-top: 10px;
    padding-bottom: 4px;
    color:#717168;
    background-color:#454444;
}
</style>

<div class="footerBackground">

    <div class="container">
        <div class="row">

            <div class="col-lg-3">
                <font style="color: #ffffff; font-size: 12px;">About Us</font>
                <br>

                <a><font
                        style="color: #7f7f7f; font-size: 11px;">Our Story</font></a>
                <br>
                <a><font
                        style="color: #7f7f7f; font-size: 11px;">Locations</font></a>
            </div>


            <div class="col-lg-3 ">

                <font style="color: #ffffff; font-size: 12px;">Support</font>
                <br>

                <a><font
                        style="color: #7f7f7f; font-size: 11px;">Contact Us</font></a>

            </div>

            <div class="col-lg-3 ">
                <font style="color: #ffffff; font-size: 12px;">Connect</font>
                <br>

                <a><font
                        style="color: #7f7f7f; font-size: 11px;">Facebook</font></a>
                <br>
                <a><font
                        style="color: #7f7f7f; font-size: 11px;">Twitter</font></a>
                <br>
                <a><font
                        style="color: #7f7f7f; font-size: 11px;">Google +</font></a>
                <br>
                <a><font
                        style="color: #7f7f7f; font-size: 11px;">Youtube</font></a>

            </div>

            <div class="col-lg-3 ">
                <font style="color: #ffffff; font-size: 12px;">Other</font>
                <br>

                <a><font
                        style="color: #7f7f7f; font-size: 11px;">Privacy Policy</font></a>
                <br>
                <a><font
                        style="color: #7f7f7f; font-size: 11px;">Terms & Conditions</font></a>

            </div>


        </div>
    </div>
</div>



<div class="lastFooterBackground">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <span class="copyright"> &copy; PrepareEasy, 2016</span>
            </div>

            <div class="col-lg-4">
%{--
                <a><font style="color: #717168;">Powered by ThoughLining</font></a>
--}%
            </div>

            <div class="col-lg-4">
                <ul class="list-inline quicklinks">
                    %{--<li><a><font style="color: #717168;">Powered by ThoughtLining</font></a></li>--}%
%{--
                    <li><a><font style="color: #717168;">Terms of Use</font> </a></li>
--}%
                    <li><img src="${resource(dir: '/images', file: 'indianFlag.jpeg')}" style="width:40px;height: 28px; "></li>
                </ul>
            </div>
        </div>
    </div>
</div>