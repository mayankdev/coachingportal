<sec:ifAllGranted roles="ROLE_ADMIN">
    <nav class="navbar navbar-default navbar-fixed-top" style="background-color: #003366">
</sec:ifAllGranted>
<sec:ifAllGranted roles="ROLE_STUDENT">
    <nav class="navbar navbar-default navbar-fixed-top" style="background-color: #000000">
</sec:ifAllGranted>
<sec:ifAllGranted roles="ROLE_COACHING">
    <nav class="navbar navbar-default navbar-fixed-top" style="background-color: #36648b">
</sec:ifAllGranted>

%{--2c539e--}%
<div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header page-scroll">
        <button type="button" class="navbar-toggle" data-toggle="collapse"
                data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand page-scroll" style="color: #ffffff">Prepare Easy</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">

            <g:if test="${session.getAttribute('lastUser')!=null}">
                <li><g:link controller="login" action="reAuthenticateUser"
                            params="[username: session.getAttribute('lastUser')]">Switch Back</g:link>
                </li>
            </g:if>
            <sec:ifLoggedIn>
                <sec:ifAllGranted roles="ROLE_COACHING">
                    <li><a href="${createLink(controller: 'coachingQuestion', action: 'index')}"><i
                            class="fa fa-book"></i> Questions</a></li>
                </sec:ifAllGranted>

        %{--<li><a href="${createLink(controller:'coachingSubject',action: 'index')}"><i
                class="fa fa-bar-chart-o"></i> Manage Subject</a></li>--}%

                <sec:ifAllGranted roles="ROLE_COACHING">
                    <li>
                        <a href="${createLink(controller: 'coachingInfocenter', action: 'index')}"><i
                                class="fa fa-leanpub"></i> My Institute</a>
                    </li>
                </sec:ifAllGranted>
                <sec:ifAllGranted roles="ROLE_STUDENT">
                    <li>
                        <a href="${createLink(controller: 'studentProfile', action: 'home')}"><i
                                class="fa fa-leanpub"></i> My Institutes</a>
                    </li>
                    <li>
                        <a href="${createLink(controller: 'studentResult', action: 'allPapersResult')}"><i
                                class="fa fa-file-text"></i> Result</a>
                    </li>
                </sec:ifAllGranted>


        %{--<li><a href="${createLink(controller:'coachingMail',action: 'index')}"><i
                class="fa fa-envelope"></i> Communicate</a></li>--}%



                <sec:ifAllGranted roles="ROLE_COACHING">
                    <li>
                        <a href="${createLink(controller: 'coachingProfile', action: 'index')}"><i
                                class="fa fa-user"></i><sec:loggedInUserInfo field="username"/></a>
                    </li>
                    <li>
                        <g:link controller="coachingProfile" action="home" class="page-scroll"><i
                                class="fa fa-home"></i>Home</g:link>
                    </li>
                </sec:ifAllGranted>

                <sec:ifAllGranted roles="ROLE_STUDENT">
                    <li>
                        <a href="${createLink(controller: 'studentProfile', action: 'index')}"><i
                                class="fa fa-user"></i><sec:loggedInUserInfo field="username"/></a>
                    </li>
                    <li>
                        <g:link controller="studentProfile" action="home" class="page-scroll"><i
                                class="fa fa-home"></i>Home</g:link>
                    </li>
                </sec:ifAllGranted>

                <sec:ifAllGranted roles="ROLE_ADMIN">
                    <li>
                        <a href="${createLink(controller: 'activity', action: 'userList')}"><i
                                class="fa fa-users"></i>Activity</a>
                    </li>
                    <li>
                        <a href="${createLink(controller: 'adminProfile', action: 'home')}"><i
                                class="fa fa-user"></i><sec:loggedInUserInfo field="username"/></a>
                    </li>
                    <li>
                        <g:link controller="adminProfile" action="home" class="page-scroll"><i
                                class="fa fa-home"></i>Home</g:link>
                    </li>
                </sec:ifAllGranted>

                <li>
                    <g:link controller="logout" action="index" class="page-scroll"><i
                            class="fa fa-circle-o-notch"></i>Logout</g:link>
                </li>
            </sec:ifLoggedIn>

            <sec:ifNotLoggedIn>
                <li>
                    <g:link controller="login" action="index">Login</g:link>
                </li>
            </sec:ifNotLoggedIn>
        </ul>
    </div>
    <!-- /.navbar-collapse -->
</div>
<!-- /.container-fluid -->
</nav>