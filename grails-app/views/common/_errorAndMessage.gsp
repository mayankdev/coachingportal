<g:hasErrors bean="${someInstance}">
    <div class="alert alert-danger">
        <g:renderErrors bean="${someInstance}" />
    </div>
</g:hasErrors>


<g:if test="${flash.success}">
    <div class="alert alert-success">
        ${flash.success}
    </div>
</g:if>

<g:if test="${flash.danger}">
    <div class="alert alert-danger">
        ${flash.danger}
    </div>
</g:if>

<g:if test="${flash.message}">
    <div class="alert alert-info">
        ${flash.message}
    </div>
</g:if>

<g:if test="${flash.warning}">
    <div class="alert alert-warning">
        ${flash.warning}
    </div>
</g:if>



