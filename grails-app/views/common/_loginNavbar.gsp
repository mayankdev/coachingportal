    <nav class="navbar navbar-default navbar-fixed-top" style="background-color: #000000">

%{--2c539e--}%
<div class="container" >
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header page-scroll">
        <button type="button" class="navbar-toggle" data-toggle="collapse"
                data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand page-scroll" style="color: #ffffff" href="${createLink(controller: 'login',action:'home' )}">Prepare Easy</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">

            %{--<li><a href="${createLink(controller:'coachingQuestion',action: 'index')}"><i
                    class="fa fa-globe"></i> Coaching Locator</a></li>--}%


            <sec:ifNotLoggedIn>
                <li>

                    <g:link controller="login" action="index">Login</g:link>
                </li>
            </sec:ifNotLoggedIn>

            <sec:ifLoggedIn>
                <li>
                    <a href="${createLink(controller:'coachingProfile',action: 'index')}"><i class="fa fa-user"></i><sec:loggedInUserInfo field="username"/></a>
                </li>
                <li>
                    <g:link controller="coachingProfile" action="home" class="page-scroll"><i class="fa fa-home"></i>Home</g:link>
                </li>
                <li>

                    <g:link controller="logout" action="index" class="page-scroll"><i class="fa fa-circle-o-notch"></i>Logout</g:link>

                </li>
            </sec:ifLoggedIn>
        </ul>
    </div>
    <!-- /.navbar-collapse -->
</div>
<!-- /.container-fluid -->
</nav>