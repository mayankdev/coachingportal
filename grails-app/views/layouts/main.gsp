<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, ">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="Grails"/></title>
		<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.css" />
        %{--<link rel="stylesheet" href="${resource(dir: '/css/font-awesome', file: 'font-awesome.min.css')}" type="text/css" />--}%



    <link rel="stylesheet" href="${resource(dir: 'css', file: 'bootstrap.min.css')}" type="text/css">

    %{--<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>--}%
    <script src="${resource(dir: 'js', file: 'jquery-ui.min.js')}"></script>

    <script src="${resource(dir: 'js', file: 'jquery-2.1.1.js')}" type="text/javascript"></script>

    %{--<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>--}%
    <script src="${resource(dir: 'js', file: 'bootstrap.min.js')}" type="text/javascript"></script>

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
	 <link rel="stylesheet" href="${resource(dir: '/css', file: 'common.css')}"type="text/css">
		<g:layoutHead/>
		<g:javascript library="application"/>		
		<r:layoutResources />
    <title>Prepare Easy</title>
    <style type="text/css">
    html,body
    {
        height:88%;
    }

    #wrap
    {
        min-height: 100%;
    }
    </style>
	</head>
        <div id="wrap">
	<body style="margin-top:60px; ">
        <g:render template="/common/navbar" />
		<g:layoutBody/>
        <r:layoutResources />
        </div>
        <g:render template="/common/footer"/>

	</body>
</html>
