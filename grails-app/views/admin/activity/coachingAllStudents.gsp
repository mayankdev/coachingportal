<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Prepare Easy</title>
    <meta name="layout" content="main">
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <g:render template="/common/errorAndMessage"/>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <h3>Institute Name->${coaching.coachingName()}<g:link controller="activity" action="deactivateAllStudents" params="[uniqueId: coaching.uniqueId]" class="btn btn-primary pull-right">Deactivate All Students</g:link></h3>
                <hr>
                <table class="table table-responsive table-hover">
                    <tr>
                        <th>#</th>
                        <th>Username</th>
                        <th>Enabled</th>
                        <th>Password Reset</th>
                        <th>Added to coaching On</th>
                        <th>Joined On</th>
                        <th>Last Updated</th>
                    </tr>
                    <g:if test="${subscribeStudentList?.size() > 0}">
                        <g:each in="${subscribeStudentList}" var="subscribeStudentInstance" status="i">
                            <tr>
                                <td>${i + 1}</td>
                                <td>%{--<g:link controller="user" action="userEdit" params="[username:userInstance.username]">--}%${subscribeStudentInstance.student.username}%{--</g:link>--}%</td>
                                <td>
                               <g:link controller="activity" action="deactivateUser"
                                                params="[uniqueId: subscribeStudentInstance.student.uniqueId]">${subscribeStudentInstance.student.enabled}</g:link>

                                </td>
                                <td>
                                    <g:link controller="activity" action="passwordReset"
                                            params="[uniqueId: subscribeStudentInstance.student.uniqueId]">Reset</g:link>

                                </td>
                                <td><g:formatDate format="dd-MM-yyyy" date="${subscribeStudentInstance?.dateCreated}"/></td>
                                <td><g:formatDate format="dd-MM-yyyy" date="${subscribeStudentInstance.student?.dateCreated}"/></td>
                                <td><g:formatDate format="dd-MM-yyyy" date="${subscribeStudentInstance.student?.lastUpdated}"/></td>

                            </tr>
                        </g:each>
                    </g:if>
                    <g:else>
                        <div class="alert alert-info">
                            There are no students.
                        </div>
                    </g:else>
                </table>
            </div>
        </div>
    </div>

</div>
</body>
</html>