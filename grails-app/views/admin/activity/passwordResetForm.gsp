<html>
<head>
    <meta name='layout' content='main'/>
    <title>Prepare Easy</title>

</head>

<body>
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <g:render template="/common/errorAndMessage"/>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-8 col-lg-offset-2 well">
            <strong>Password Reset</strong>
            <sec:ifAnyGranted roles="ROLE_ADMIN">
            <div class="pull-right">
                <g:link controller="activity" action="userList">UserList</g:link>
            </div>
            </sec:ifAnyGranted>
            <hr>

            <g:form controller="activity" action="passwordResetValidate">

                <input type="hidden" name="username" value="${username}">
                <div class="row">
                    <div class="col-lg-6">
                        <label for='password'>Password:*</label>
                        <input type='password' class='form-control' name='password' id='password' placeholder="Password"/>
                    </div>
                    <div class="col-lg-6">
                        <label for='confirmPassword'>Re-type Password :*</label>
                        <input type='password' class='form-control' name='confirmPassword' id='confirmPassword' placeholder="Re-type Password"/>
                    </div>
                </div>

                <div class="space20"></div>
                <p>
                    <input type='submit' class="btn btn-primary" id="submit" value='Submit'/>
                </p>
            </g:form>
        </div>
    </div>
</div>
<script type='text/javascript'>
    <!--
    (function() {
        document.forms['loginForm'].elements['j_username'].focus();
    })();
    // -->
</script>
</body>
</html>
