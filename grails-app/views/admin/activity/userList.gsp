<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Prepare Easy</title>
    <meta name="layout" content="main">
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <g:render template="/common/errorAndMessage"/>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <h3>Institute List</h3>
                <hr>
                <table class="table table-responsive table-hover">
                    <tr>
                        <th>#</th>
                        <th>Username</th>
                        <th>Enabled</th>
                        <th>Password Reset</th>
                        <th>Joined On</th>
                        <th>Last Updated</th>
                        <th>Sign In as</th>
                    </tr>
                <g:if test="${coachingList?.size()>0}">
                    <g:each in="${coachingList}" var="instituteInstance" status="i">
                        <tr>
                            <td>${i + 1}</td>
                            <td><g:link controller="activity" action="coachingAllStudents" params="[uniqueId:instituteInstance.uniqueId]">${instituteInstance.username}</g:link></td>
                            <td>
                                <g:link controller="activity" action="deactivateUser"
                                            params="[uniqueId: instituteInstance.uniqueId]">${instituteInstance.enabled}</g:link>
                            </td>
                            <td>
                                <g:link controller="activity" action="passwordReset"
                                        params="[uniqueId: instituteInstance.uniqueId]">Reset</g:link>

                            </td>
                            <td><g:formatDate format="dd-MM-yyyy" date="${instituteInstance?.dateCreated}"/></td>
                            <td><g:formatDate format="dd-MM-yyyy" date="${instituteInstance?.lastUpdated}"/></td>
                            <td><g:link controller="activity" action="reAuthenticateUser"
                                        params="[username: instituteInstance.username]">${instituteInstance?.username}</g:link></td>

                        </tr>
                    </g:each>
                    </g:if>
                    <g:else>
                        <div class="alert alert-info">
                            There are no institutes added yet.
                        </div>
                    </g:else>
                </table>
            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <h3>Student List</h3>
                <hr>
                <table class="table table-responsive table-hover">
                    <tr>
                        <th>#</th>
                        <th>Username</th>
                        <th>Enabled</th>
                        <th>Password Reset</th>
                        <th>Joined On</th>
                        <th>Last Updated</th>
                        <th>Sign In as</th>
                    </tr>
                    <g:if test="${studentList?.size()>0}">
                    <g:each in="${studentList}" var="studentInstance" status="i">
                        <tr>
                            <td>${i + 1}</td>
                            <td>%{--<g:link controller="user" action="userEdit" params="[username:userInstance.username]">--}%${studentInstance.username}%{--</g:link>--}%</td>
                            <td>
                                <g:if test="${sec.username() == studentInstance.username}">
                                    ${studentInstance.enabled}
                                </g:if>
                                <g:else>
                                    <g:link controller="activity" action="deactivateUser"
                                            params="[uniqueId: studentInstance.uniqueId]">${studentInstance.enabled}</g:link>
                                </g:else>
                            </td>
                            <td>
                                <g:link controller="activity" action="passwordReset"
                                        params="[uniqueId: studentInstance.uniqueId]">Reset</g:link>

                            </td>
                            <td><g:formatDate format="dd-MM-yyyy" date="${studentInstance?.dateCreated}"/></td>
                            <td><g:formatDate format="dd-MM-yyyy" date="${studentInstance?.lastUpdated}"/></td>
                            <td><g:link controller="activity" action="reAuthenticateUser"
                                        params="[username: studentInstance.username]">${studentInstance?.username}</g:link></td>

                        </tr>
                    </g:each>
                    </g:if>
                    <g:else>
                        <div class="alert alert-info">
                            There are no students.
                        </div>
                    </g:else>
                </table>
            </div>
        </div>
    </div>



</div>
</body>
</html>