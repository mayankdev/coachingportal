<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Prepare Easy</title>

</head>
<body>
<div class="container">
    <g:render template="/common/errorAndMessage"/>
    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <strong>Welcome <sec:loggedInUserInfo field="username"/>
                    <sec:ifLoggedIn>
                        <sec:ifAllGranted roles="ROLE_ADMIN">
                            <a href="${createLink(controller: 'coaching',action: 'addCoachingForm')}" class="btn btn-sm pull-right btn-primary">Add Coaching</a>
                        </sec:ifAllGranted>
                    </sec:ifLoggedIn>
                </strong>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <h1>Hello Admin</h1>
            </div>
        </div>
    </div>




</div>
</body>
</html>
