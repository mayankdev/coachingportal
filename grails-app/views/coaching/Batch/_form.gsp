<%@ page import="com.coachingPortal.coaching.Batch" %>



<div class="fieldcontain ${hasErrors(bean: batchInstance, field: 'name', 'error')} required">
    <label for="name">
        <g:message code="batch.name.label" default="Name" />
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="name" class="form-control" required="" value="${batchInstance?.name}"/>

</div>

