<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">

</head>
<body>
<div class="container">

    <g:render template="/common/errorAndMessage" model="[someInstance:batchInstance]"/>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <g:render template="/coaching/myCoaching/myCoachingNavBar" model="[navBarSelectedItem:'Holidays']"/>
            </div>
        </div>

    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="well">

              <h3>Add Batch<a href="${createLink(controller: 'coachingBatch',action:'index' )}" class="pull-right btn btn-primary">Back</a></h3>
                <hr>

                <g:form url="[resource:batchInstance, action:'save']" >

                    <fieldset class="form">
                        <g:render template="/coaching/Batch/form"/>
                    </fieldset>

                    <fieldset class="buttons">
                        <g:submitButton name="create" class="btn btn-success" value="${message(code: 'default.button.create.label', default: 'Create')}" />
                    </fieldset>


                </g:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
