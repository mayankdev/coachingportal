<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main" />
</head>

<body>
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <g:render template="/coaching/myCoaching/myCoachingNavBar" model="[navBarSelectedItem:'Batches']"/>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <g:render template="/common/errorAndMessage"/>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <h3>Batches<a href="${createLink(controller: 'coachingBatch',action: 'create')}" class="btn btn-sm btn-primary pull-right">Add</a></h3>
                <hr>
                <g:if test="${batchList?.size()>0}">
                    <table class="table table-responsive table-hover">
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Subscribed Students</th>
                            <th>Practice Paper</th>
                            <th>Study Material</th>
                            <th>Timetable</th>
                        </tr>
                        <tbody>
                        <g:each in="${batchList}" var="batch" status="i">
                            <tr>
                                <td>${i + 1}</td>
                                <td>${batch?.name}</td>
                                <td><g:link controller="subscribeStudent" action="index" params="[batchId:batch.id]">${batch?.subscribeStudents.size()}</g:link></td>
                                <td><g:link controller="coachingPracticePaper" action="index" params="[batchId:batch.id]">${batch?.practicePapers.size()}</g:link></td>
                                <td><g:link controller="coachingStudyMaterial" action="index" params="[batchId:batch.id]">${batch?.studyMaterials.size()}</g:link></td>
                                <td><g:link controller="coachingTimetable" action="index" params="[batchId:batch.id]">${batch?.timetable}</g:link></td>
                                %{--<sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_COACHING">
                                    <td><g:link action="edit" id="${task.id}"><i class="fa fa-pencil"></i></g:link>|
                                        <g:link controller="holidays" action="delete" id="${task.id}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"><i class="fa fa-trash"></i></g:link></td>
                                </sec:ifAnyGranted>--}%
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </g:if>
                <g:else>
                    <div class="alert alert-info">
                        <i class="fa fa-warning"></i> There are no batches.
                    </div>
                </g:else>
            </div>
        </div>
    </div>

</div>
</body>
</html>