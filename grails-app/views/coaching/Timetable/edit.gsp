<%@ page import="com.coachingPortal.coaching.Batch; com.coachingPortal.coaching.tasks.Timetable" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'timetable.label', default: 'Timetable')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <g:render template="/coaching/myCoaching/myBatchNavBar" model="[navBarSelectedItem: 'TimeTable']"/>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <g:render template="/common/errorAndMessage"/>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <h3>${(Batch.findById(Long.parseLong(session.getAttribute('batchId')))).name}->Edit Timetable</h3>
                <hr>
                <g:form controller="coachingTimetable" action="update" method="PUT">
                    <input type="hidden" name="id" value="${timetableInstance.id}">
                    <g:hiddenField name="version" value="${timetableInstance?.version}"/>
                    <fieldset class="form">
                        <g:render template="/coaching/Timetable/form" model="[timetableInstance: timetableInstance]"/>
                    </fieldset>
                    <br>
                    <fieldset class="buttons">
                        <g:submitButton class="btn btn-success" name="update"
                                        value="${message(code: 'default.button.update.label', default: 'Update')}"/>
                    </fieldset>
                </g:form>
            </div>
        </div>
    </div>

</div>
</body>
</html>
