<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main" />
</head>

<body>
<div class="container">
    <g:if test="${flash.success}">
        <div class="alert alert-success">
            ${flash.success}
        </div>
    </g:if>
    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <g:render template="/coaching/myCoaching/myBatchNavBar" model="[navBarSelectedItem:'TimeTable']"/>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <h3>Add Timetable<a href="${createLink(controller: 'coachingTimetable',action: 'index')}" class="btn btn-sm btn-primary pull-right">Back</a></h3>
                <hr>
                <g:form controller="coachingTimetable" action="save"  >
                    <fieldset class="form">
                        <g:render template="/coaching/Timetable/form"/>
                    </fieldset>
                    <br>
                    <fieldset class="buttons">
                        <g:submitButton name="create" class="btn btn-success" value="${message(code: 'default.button.create.label', default: 'Create')}" />
                    </fieldset>
                </g:form>
            </div>
        </div>
    </div>

</div>
</body>
</html>