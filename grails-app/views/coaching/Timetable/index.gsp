<%@ page import="com.coachingPortal.coaching.Batch" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main" />
</head>

<body>
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <g:render template="/coaching/myCoaching/myBatchNavBar" model="[navBarSelectedItem:'TimeTable']"/>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
              <g:render template="/common/errorAndMessage" />
        </div>
    </div>

                <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <h3>${(Batch.findById(Long.parseLong(session.getAttribute('batchId')))).name}->Timetable<a href="${createLink(controller: 'coachingTimetable',action: 'create',params: [batchId:batchInstance?.id])}" class="btn btn-sm btn-primary pull-right">Add</a></h3>
                <hr>
                <g:if test="${batchInstance?.timetable.size()>0}">
                    <table class="table table-responsive table-hover">
                        <tr>
                            <th>#</th>
                            <th>Day</th>
                            <th>Start Time</th>
                            <th>Duration</th>
                            <th>Subject Name</th>
                            <th>Last Updated</th>
                            <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_COACHING">
                                <th>Action</th>
                            </sec:ifAnyGranted>
                        </tr>
                        <tbody>
                        <g:each in="${timetableList}" var="task" status="i">
                            <tr>
                                <td>${i + 1}</td>
                                <td>${task?.day}</td>
                                <td>${task?.startTime}</td>
                                <td>${task?.duration}</td>
                                <td>${task?.subjectName}</td>
                                <td><g:formatDate format="dd-MM-yyyy" date="${task?.lastUpdated}"/></td>
                                <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_COACHING">
                                    <td><a href="${createLink(controller:'coachingTimetable',action: 'edit',params: [timetableId:task.id])}"><i class="fa fa-pencil"></i></a>|
                                        <a href="${createLink(controller:'coachingTimetable',action: 'delete',params: [timetableId:task.id])}" onclick="return confirm('${'Are you sure, you want to delete ?'}');"><i class="fa fa-trash"></i></a></td>
                                </sec:ifAnyGranted>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </g:if>
                <g:else>
                    <div class="alert alert-info">
                        <i class="fa fa-warning"></i> Time table not updated by coaching.
                    </div>
                </g:else>
            </div>
        </div>
    </div>

</div>
</body>
</html> 