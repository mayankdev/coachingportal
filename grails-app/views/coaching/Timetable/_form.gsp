<%@ page import="com.coachingPortal.coaching.tasks.Timetable" %>


<div class="col-lg-6">
<div class="fieldcontain ${hasErrors(bean: timetableInstance, field: 'day', 'error')} required">
	<label for="day">
		<g:message code="timetable.day.label" default="Day" />
		<span class="required-indicator">*</span>
	</label>
%{--
	<g:textField name="day" class="form-control" required="" value="${timetableInstance?.day}"/>
--}%
    <g:select name="day" class="form-control" from="${['Sunday', 'Monday', 'Tuesday','Wednesday','Thursday','Friday','Saturday']}" value="${timetableInstance?.day}" />

</div>
</div>

<div class="col-lg-6">
    <div class="fieldcontain ${hasErrors(bean: timetableInstance, field: 'subjectName', 'error')} required">
        <label for="subjectName">
            <g:message code="timetable.subjectName.label" default="Subject Name" />
            <span class="required-indicator">*</span>
        </label>
        <g:textField name="subjectName" class="form-control"  required="" value="${timetableInstance?.subjectName}"/>
    </div>
</div>

<div class="col-lg-6">
<div class="fieldcontain ${hasErrors(bean: timetableInstance, field: 'startTime', 'error')} required">
    
    <div class="col-lg-6">
    <label for="startHours">
        <g:message code="timetable.startTime.label" default="Start Hours" />
        <span class="required-indicator">*</span>
    </label>
        <g:if test="${startHours}">
            <g:select name="startHours" from="${0..23}" class="form-control" value="${startHours}" />
            </g:if>
        <g:else>
             <g:select name="startHours" from="${0..23}" class="form-control" value="" />
        </g:else>
    </div>

    <div class="col-lg-6">
        <label for="startMinutes">
            <g:message code="timetable.startTime.label" default="Start Minutes" />
            <span class="required-indicator">*</span>
        </label>
        <g:if test="${startMinutes}">
       <g:select name="startMinutes" from="${(0..59).step(5)}" class="form-control" value="${startMinutes}" />
            </g:if>
        <g:else>
            <g:select name="startMinutes" from="${(0..59).step(5)}" class="form-control" value="" />
        </g:else>
    </div>
</div>
</div>

<div class="col-lg-6">
<div class="fieldcontain ${hasErrors(bean: timetableInstance, field: 'duration', 'error')} required">

    <div class="col-lg-6">
        <label for="durationHours">
            <g:message code="timetable.startTime.label" default="Duration Hours" />
            <span class="required-indicator">*</span>
        </label>
        <g:if test="${durationHours}">
        <g:select name="durationHours" from="${0..15}" class="form-control" value="${durationHours}" />
        </g:if>
        <g:else>
          <g:select name="durationHours" from="${0..15}" class="form-control" value="" />
        </g:else>
    </div>

    <div class="col-lg-6">
        <label for="durationMinutes">
            <g:message code="timetable.startTime.label" default="Duration Minutes" />
            <span class="required-indicator">*</span>
        </label>
        <g:if test="${durationMinutes}">
        <g:select name="durationMinutes" from="${(0..59).step(5)}" class="form-control" value="${durationMinutes}" />
        </g:if>
        <g:else>
            <g:select name="durationMinutes" from="${(0..59).step(5)}" class="form-control" value="" />
        </g:else>
    </div>
</div>
</div>



%{--<div class="col-lg-6">
<div class="fieldcontain ${hasErrors(bean: timetableInstance, field: 'batch', 'error')} required">
	<label for="batch">
		<g:message code="timetable.batch.label" default="Batch" />
		<span class="required-indicator">*</span>
	</label>--}%
%{--
	<g:select id="batch" name="batch.id" class="form-control"  from="${com.coachingPortal.coaching.Batch.list()}" optionKey="id" required="" value="${timetableInstance?.batch?.id}" class="many-to-one"/>
--}%
%{--
    <g:textField name="batchName" class="form-control"  required="" value="${batchInstance?.name}" readonly="readonly" style="background-color:#c0c0c0;"/>
--}%
%{--
</div>
</div>
--}%

