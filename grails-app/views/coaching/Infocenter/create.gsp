<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'infocenter.label', default: 'Infocenter')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
    <div class="container">

        <g:render template="/common/errorAndMessage" model="[someInstance:infocenterInstance]"/>
        <div class="row">
            <div class="col-lg-12">
                <div class="well">
                    <g:render template="/coaching/myCoaching/myCoachingNavBar" model="[navBarSelectedItem:'Infocenter']"/>
                </div>
            </div>

        </div>


<div class="row">
   <div class="col-lg-12">
     <div class="well">

			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>

         <h3>Add Info<a href="${createLink(controller: 'coachingInfocenter',action:'index' )}" class="pull-right btn btn-primary">Back</a></h3>
         <hr>
			<g:form controller="coachingInfocenter" action="save">

				<fieldset class="form">
					<g:render template="/coaching/Infocenter/form"/>
				</fieldset>

				<fieldset class="buttons">
					<g:submitButton name="create" class="btn btn-success" value="${message(code: 'default.button.create.label', default: 'Create')}" />
				</fieldset>


			</g:form>
     </div>
       </div>
		</div>
   </div>
	</body>
</html>
