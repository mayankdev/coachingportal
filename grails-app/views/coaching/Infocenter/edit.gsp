<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main" />
</head>

<body>
<div class="container">
    <g:render template="/common/errorAndMessage" model="[someInstance:infocenterInstance]"/>
    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <g:render template="/coaching/myCoaching/myCoachingNavBar" model="[navBarSelectedItem:'Infocenter']"/>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-8">
                        <h3>Edit Infocenter<a href="${createLink(controller: 'coachingInfocenter',action:'index' )}" class="pull-right">Back</a></h3>
                        <hr>
                        <g:form controller="coachingInfocenter" action="update" method="PUT" >
                            <input type="hidden" name="id" value="${infocenterInstance?.id}">
                            <g:hiddenField name="version" value="${infocenterInstance?.version}" />
                            <fieldset class="form">
                                <g:render template="/coaching/Infocenter/form"/>
                            </fieldset>
                            <fieldset class="buttons">
                                <g:actionSubmit class="btn btn-success" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                            </fieldset>
                        </g:form>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
            </div>
        </div>
    </div>

</div>
</body>
</html>