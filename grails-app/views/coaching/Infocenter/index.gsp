<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main" />
</head>

<body>
<div class="container">
    <g:render template="/common/errorAndMessage" model="[someInstance:someInstance]"/>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <g:render template="/coaching/myCoaching/myCoachingNavBar" model="[navBarSelectedItem:'Infocenter']"/>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <h3>Infocenter<a href="${createLink(controller: 'coachingInfocenter',action: 'create',params:[taskType:'Infocenter'] )}" class="btn btn-sm btn-primary pull-right">Add</a></h3>
                <hr>
                <g:if test="${infocenterInstanceList?.size()>0}">
                    <table class="table table-responsive table-hover">
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Date</th>
                            <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_COACHING">
                                <th>Action</th>
                            </sec:ifAnyGranted>
                        </tr>
                        <tbody>
                        <g:each in="${infocenterInstanceList}" var="task" status="i">
                            <tr>
                                <td>${i + 1}</td>
                                <td><g:link action="show" id="${task.id}">${task?.title}</g:link></td>
                                <td><g:formatDate format="dd-MM-yyyy" date="${task?.lastUpdated}"/></td>
                                <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_COACHING">
                                    <td><g:link controller="coachingInfocenter" action="edit" id="${task.id}"><i class="fa fa-pencil"></i></g:link>|
                                    <g:link controller="coachingInfocenter" action="delete" id="${task.id}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"><i class="fa fa-trash"></i></g:link></td>
                                </sec:ifAnyGranted>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </g:if>
                <g:else>
                    <div class="alert alert-info">
                        <i class="fa fa-warning"></i> There is no information added.
                    </div>
                </g:else>
            </div>
        </div>
    </div>

</div>
</body>
</html>