<%@ page import="com.coachingPortal.users.Coaching; com.coachingPortal.coaching.tasks.Infocenter" %>



<div class="fieldcontain ${hasErrors(bean: infocenterInstance, field: 'title', 'error')} required">
	<label for="title">
		<g:message code="infocenter.title.label" default="Title" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="title" class="form-control" required="" value="${infocenterInstance?.title}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: infocenterInstance, field: 'description', 'error')} required">
	<label for="description">
		<g:message code="infocenter.description.label" default="Description" />
		<span class="required-indicator">*</span>
	</label>
	<g:textArea name="description" class="form-control" required="" value="${infocenterInstance?.description}" rows="6" />

</div>



