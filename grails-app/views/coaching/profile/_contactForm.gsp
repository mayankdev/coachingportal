<div class="row">
    <div class="form-group col-lg-6 ${hasErrors(bean: contactDetailsInstance, field: "houseNo", 'has-error')}">
        <label for="houseNo">House No</label>
        <input type="text" name="houseNo" id="houseNo" class="form-control"
               value="${contactDetailsInstance?.houseNo}" required>
    </div>

    <div class="form-group col-lg-6 ${hasErrors(bean: contactDetailsInstance, field: "streetName", 'has-error')}">
        <label for="street">Street</label>
        <input type="text" name="streetName" id="street" class="form-control"
               value="${contactDetailsInstance?.streetName}" required>
    </div>
</div>

<div class="row">

    <div class="form-group col-lg-6 ${hasErrors(bean: contactDetailsInstance, field: "city", 'has-error')}">
        <label for="city">City</label>
        <input type="text" name="city" id="city" class="form-control"
               value="${contactDetailsInstance?.city}" required>
    </div>

    <div class="form-group col-lg-6 ${hasErrors(bean: contactDetailsInstance, field: "postalCode", 'has-error')}">
        <label for="postalCode">Postal Code</label>
        <input type="text" name="postalCode" id="postalCode" class="form-control" pattern="[0-9]{6}"
               value="${contactDetailsInstance?.postalCode}" required>
    </div>

</div>

<div class="row">


    <div class="form-group col-lg-6 ${hasErrors(bean: contactDetailsInstance, field: "state", 'has-error')}">
        <label for="state">State</label>
        <input type="text" name="state" id="state" class="form-control"
               value="${contactDetailsInstance?.state}" required>
    </div>
</div>