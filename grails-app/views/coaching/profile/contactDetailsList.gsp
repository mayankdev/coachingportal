<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Contact Details | ClashMate</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <meta name="layout" content="main" />

</head>

<body>
<div class="space40"></div>
<div class="container">
    <g:render template="/common/errorAndMessage"/>
    <g:render template="/coaching/profile/profileCommon" />

    <div class="well well-sm">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="no-margin">Contact Details <g:link controller="coachingProfile" action="addContactDetailsForm" class="btn btn-primary pull-right">Add</g:link></h3>
            </div>
        </div>
        <hr class="h2_horizontal_line"/>

        <div class="row">
            <div class="col-lg-12">
                <g:if test="${contactDetailsList.size()>0}">
                    <table class="table table-responsive table-hover">
                        <tr>
                            <th>#</th>
                            <th>House No</th>
                            <th>Street</th>
                            <th>City</th>
                            <th>Postal Code</th>
                            <th>State</th>
                            <th>Last Updated</th>
                            <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_COACHING">
                                <th>Action</th>
                            </sec:ifAnyGranted>
                        </tr>
                        <tbody>
                        <g:each in="${contactDetailsList}" var="cDetails" status="i">
                            <tr>
                                <td>${i + 1}</td>
                                <td>${cDetails?.houseNo}</td>
                                <td>${cDetails?.streetName}</td>
                                <td>${cDetails?.city}</td>
                                <td>${cDetails?.postalCode}</td>
                                <td>${cDetails?.state}</td>
                                <td><g:formatDate format="dd-MM-yyyy" date="${cDetails?.lastUpdated}"/></td>
                                <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_COACHING">
                                    <td><a href="${createLink(controller:'coachingProfile',action: 'edit',params: [cDetailsId:cDetails.id])}"><i class="fa fa-pencil"></i></a>|
                                        <a href="${createLink(controller:'coachingProfile',action: 'delete',params: [cDetailsId:cDetails.id])}" onclick="return confirm('${'Are you sure, you want to delete ?'}');"><i class="fa fa-trash"></i></a></td>
                                </sec:ifAnyGranted>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </g:if>
                <g:else>
                    <div class="alert alert-info">
                        No contact details added
                    </div>

                </g:else>

            </div>
        </div>
    </div>
</div>
<div class="space60"></div>
</body>
</html>
    