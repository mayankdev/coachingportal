<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Contact Details</title>
    <meta name="keywords" content="">
    <meta name="description" content="">

    <meta name="layout" content="main" />

</head>

<body>
<div class="space40"></div>
<div class="container">
    <g:render template="/common/errorAndMessage" model="[someInstance:contactDetailsInstance]"/>
    <g:render template="/coaching/profile/profileCommon" model="[activeLink:'ContactDetails']"/>

    <div class="well well-sm">
        <div class="row">
            <div class="col-lg-12">
                <h3>Contact Details</h3>
            </div>
        </div>
        <hr class="h2_horizontal_line"/>

        <div class="row">
            <div class="col-lg-10 text-center col-lg-offset-1">
                <g:form controller="coachingProfile" action="contactDetailsSubmit">

                    <g:render template="/coaching/profile/contactForm" model="[contactDetailsInstance:contactDetailsInstance]"/>

                    <div class="row">
                        <div class="form-group col-lg-6">
                            <input type="submit" class="btn btn-block btn-primary" value="Submit">
                        </div>

                        <div class="form-group col-lg-6">
                            <input type="reset" class="btn btn-default btn-block" value="Reset">
                        </div>
                    </div>

                </g:form>

            </div>
        </div>
    </div>
</div>
<div class="space60"></div>
</body>
</html>
    