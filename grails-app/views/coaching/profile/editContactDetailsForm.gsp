<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Edit Contact Details | CP</title>
        <meta name="keywords" content="">
    <meta name="description" content="">

    <meta name="layout" content="main" />

</head>

<body>
<div class="space40"></div>
<div class="container">

    <g:render template="/coaching/profile/profileCommon" />

    <div class="well well-sm">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h4 class="no-margin">Contact Details</h4>
            </div>
        </div>
        <hr class="h2_horizontal_line"/>

        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <g:form controller="coachingProfile" action="update">
                    <input type="hidden" name="id" value="${contactDetailsInstance.id}">
                    <g:render template="/coaching/profile/contactForm" model="[contactDetailsInstance:contactDetailsInstance]"/>

                    <div class="row">
                        <div class="form-group col-lg-6">
                            <input type="submit" class="btn btn-block btn-primary" value="Submit">
                        </div>

                        <div class="form-group col-lg-6">
                            <input type="reset" class="btn btn-default btn-block" value="Reset">
                        </div>
                    </div>

                </g:form>


            </div>
        </div>

    </div>
</div>
    <div class="space60"></div>
</body>
</html>
    