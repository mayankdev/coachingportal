<div class="well well-sm">
    <div class="row ">
        <div class="col-lg-4 text-center">
            <g:if test="${activeLink=='PersonalDetails'}">
                <h5 class="dark_blue_text_color">PERSONAL DETAILS</h5>
            </g:if>
            <g:else>
                <h5><g:link controller="coachingProfile" action="index">PERSONAL DETAILS</g:link></h5>
            </g:else>
        </div>


        <div class="col-lg-4 text-center">
            <g:if test="${activeLink=='ContactDetails'}">
                <h5 class="dark_blue_text_color">CONTACT DETAILS</h5>
            </g:if>
            <g:else>
                <h5><g:link controller="coachingProfile" action="contactDetailsList">CONTACT DETAILS</g:link></h5>
            </g:else>
        </div>

        <div class="col-lg-4">

            <g:if test="${activeLink=='ChangePassword'}">
                <h5 class="dark_blue_text_color">CHANGE PASSWORD</h5>
            </g:if>
            <g:else>
                <h5><g:link controller="coachingProfile" action="changePasswordForm">CHANGE PASSWORD</g:link></h5>
            </g:else>
        </div>


    </div>
</div>