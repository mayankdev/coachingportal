<html>
<head>
    <title>Edit Personal Details | CP</title>
        <meta name="keywords" content="">
    <meta name="description" content="">


    <meta name="layout" content="main" />

</head>

<body>
<div class="space40"></div>
<div class="container">

<g:render template="/coaching/profile/profileCommon" />
<g:render template="/common/errorAndMessage" model="[someInstance:adminInstance]"/>

    <div class="well well-sm">
    <div class="row">
        <div class="col-lg-12 text-center">
            <h4 class="no-margin">Personal Details</h4>
        </div>
    </div>
    <hr class="h2_horizontal_line"/>

    <div class="row">
        <div class="col-lg-2 text-center">
            <img src="${resource(dir: 'images',file: 'author-placeholder.jpg')}" style="border: 2px solid #ffffff;">
        </div>
        <div class="col-lg-10 text-center">

            <form name="personalDetailsForm"
                  action="${createLink(controller: 'coachingProfile', action: 'submitPersonalDetailsForm')}">

                <input type="hidden" name="coachingId" value="${adminInstance.uniqueId}">
                <div class="row">
                    <div class="form-group col-lg-6 ${hasErrors(bean: adminInstance, field: "name", 'has-error')}">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name" class="form-control"
                               value="${adminInstance?.name}">
                    </div>


                </div>

                <div class="row">
                    <div class="form-group col-lg-6 ${hasErrors(bean: adminInstance, field: "contactNo", 'has-error')}">
                        <label for="contactNo">Contact Number(10 digit)*</label>
                        <input type="text" name="contactNo" id="contactNo" class="form-control" pattern="[0-9]{10}"
                               value="${adminInstance?.contactNo}">
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <input type="submit" class="btn btn-primary btn-block" value="Submit">
                    </div>

                    <div class="col-lg-6">
                        <input type="reset" class="btn btn-default btn-block" value="Reset">
                    </div>
                </div>
            </form>
        </div>
    </div>
    </div>
</div>
<div class="space60"></div>
</body>
</html>
    