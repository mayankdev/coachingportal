<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Change Password | ClashMate</title>
        <meta name="keywords" content="discussion forum ,user profile,interview story,aptitude practice,reasoning practice,english practice,online test,news,jobs,competition‬,competitive exam preparation,ibps preparation,banking preparation,banking exam,bank preparation,‎ibps,‎sbi po,‎ibps po‬,ibps clerk‬,ibps rrb,ibps specialist,‎online exam‬,‎bank exam‬,‎sbi clerk,‎ssc‬,‎civil services‬,‎ssc">
    <meta name="description" content="ClashMate shows  you your profile details in most elegant way so that you can easily update or can see them when you want to.">

    <meta name="layout" content="main" />

</head>

<body>
<div class="space40"></div>
<div class="container">
    <g:render template="/common/errorAndMessage" model="[someInstance:passwordResetCO]" />
    <g:render template="/coaching/profile/profileCommon" />

    <div class="well well-sm">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-xs-12 text-center">
            <h4 class="no-margin">Change Password</h4>
        </div>
    </div>
    <hr class="h2_horizontal_line"/>

    <div class="row">
        <div class="col-sm-2 col-md-2 col-xs-2 text-center">
            <img src="${resource(dir: 'images',file: 'author-placeholder.jpg')}" style="border: 2px solid #ffffff;">
        </div>
        <div class="col-sm-10 col-md-10 col-xs-10 text-center">

            <g:form controller="coachingProfile" action="submitPasswordForm">
                <input type="hidden" name="id"  value="${adminInstance.id}" />

                <div class="row">

                    <div class="form-group col-sm-8">
                        <label for="oldPassword">Current Password</label>
                        <input type="password" name="oldPassword" id="oldPassword" class="form-control" placeholder="Current Password" >
                    </div>



                    <div class="form-group col-sm-8">
                        <label for="password">New Password</label>
                        <input type="password" name="password" id="password" class="form-control" placeholder="New Password">
                    </div>



                    <div class="form-group col-sm-8">
                        <label for="confirmPassword">Confirm Password</label>
                        <input type="password" name="confirmPassword" id="confirmPassword" class="form-control" placeholder="Confirm Password">

                    </div>
                </div>


                <input type="submit" class="btn btn-primary" value="Submit">
                <input type="reset" class="btn btn-default" value="Reset">
            </g:form>
        </div>
    </div>
    </div>
</div>
<div class="space60"></div>
</body>
</html>
    