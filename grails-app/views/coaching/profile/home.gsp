<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Prepare Easy</title>

</head>
<body>
<div class="container">
    <g:render template="/common/errorAndMessage"/>
    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <strong>Welcome <sec:loggedInUserInfo field="username"/></strong>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <div class="row">


                    <div class="col-lg-7">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">Infocenter</h3>
                            </div>
                            <div class="panel-body">
                                <g:if test="${infocenterList}">
                                    <table class="table table-responsive">
                                        <g:each in="${infocenterList}" var="infocenter">
                                            <tr><td>
                                                ${infocenter.title}
                                            </td></tr>
                                        </g:each>
                                    </table>
                                    <a href="${createLink(controller: 'myCoaching', action: 'infocenter')}" class="pull-right">more</a>
                                </g:if>
                                <g:else>
                                    <div class="alert alert-info">
                                        No news are there
                                    </div>
                                </g:else>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">Holidays</h3>
                            </div>
                            <div class="panel-body">
                                <g:if test="${holidayList}">
                                    <table class="table table-responsive">
                                        <g:each in="${holidayList}" var="holiday">
                                            <tr><td>
                                                ${holiday.title}
                                            </td></tr>
                                        </g:each>
                                    </table>
                                    <a href="${createLink(controller: 'myCoaching', action: 'holidays')}" class="pull-right">more</a>
                                </g:if>
                                <g:else>
                                    <div class="alert alert-warning">
                                        No Holidays are there.
                                    </div>
                                </g:else>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">Batches</h3>
                            </div>
                            <div class="panel-body">

                                <g:if test="${batchList?.size()>0}">
                                    <table class="table table-responsive table-hover">
                                        <tr>
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Subscribed Students</th>
                                            <th>Practice Paper</th>
                                            <th>Study Material</th>
                                            <th>Timetable</th>
                                        </tr>
                                        <tbody>
                                        <g:each in="${batchList}" var="batch" status="i">
                                            <tr>
                                                <td>${i + 1}</td>
                                                <td>${batch?.name}</td>
                                                <td><g:link controller="subscribeStudent" action="index" params="[batchId:batch.id]">${batch?.subscribeStudents.size()}</g:link></td>
                                                <td><g:link controller="coachingPracticePaper" action="index" params="[batchId:batch.id]">${batch?.practicePapers.size()}</g:link></td>
                                                <td><g:link controller="coachingStudyMaterial" action="index" params="[batchId:batch.id]">${batch?.studyMaterials.size()}</g:link></td>
                                                <td><g:link controller="studentTimetable" action="index" params="[batchId:batch.id]">${batch?.timetable}</g:link></td>
                                                %{--<sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_COACHING">
                                                    <td><g:link action="edit" id="${task.id}"><i class="fa fa-pencil"></i></g:link>|
                                                        <g:link controller="holidays" action="delete" id="${task.id}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"><i class="fa fa-trash"></i></g:link></td>
                                                </sec:ifAnyGranted>--}%
                                            </tr>
                                        </g:each>
                                        </tbody>
                                    </table>
                                </g:if>
                                <g:else>
                                    <div class="alert alert-info">
                                        <i class="fa fa-warning"></i> There are no batches.
                                    </div>
                                </g:else>





                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>




</div>
</body>
</html>
