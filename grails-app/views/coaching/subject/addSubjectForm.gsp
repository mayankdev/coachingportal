<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main" />

    <script>
     $(document).ready(function() {
            $('#subjectId').autocomplete({
                source: '<g:createLink controller='coachingSubject' action='ax_renderSubjectNames'/>'
            });

        });
    </script>

    <link rel="stylesheet" href="${resource(dir:'fonts/smoothness',file:'jquery-ui-1.8.14.custom.css')}" />

    <script src="${resource(dir: 'js', file: 'jquery.min.js')}" type="text/javascript"></script>
    <script src="${resource(dir: 'js', file: 'jquery-ui-1.8.14.custom.min.js')}" type="text/javascript"></script>


</head>

<body>
<div class="container">
    <g:render template="/common/errorAndMessage" model="[someInstance:subjectCO]"/>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
               <h3>Manage Subject</h3>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <div class="row">
                <g:form controller="coachingSubject" action="addSubject">
                    <div class="form-group col-lg-3 ">
                        <div class="col-lg-12"><label>Subject Type</label>
                        </div>
                        <input type="text" class="form-control" id="subjectId" name="subject" value="${subjectCO?.subject}" placeholder="Subject Type" >
                    </div>

                    <div class="form-group col-lg-3 ">
                        <div class="col-lg-12"><label>Subject Sub Type</label>
                        </div>
                        <input type="text" class="form-control" id="subSubject" name="subSubject" value="${subjectCO?.subSubject}" placeholder="Subject Sub Type" >
                    </div>

                    <div class="form-group col-lg-3 ">
                        <div class="col-lg-12"><label>Topic</label>
                        </div>
                        <input type="text" class="form-control" id="topic" name="topic" value="${subjectCO?.topic}" placeholder="Topic" >
                    </div>


                    <div class="form-group">
                        <div class="col-xs-6 col-md-6 col-sm-6 col-lg-6">
                            <button type="submit" class="btn btn-primary btn-block" value="Submit">Submit</button>
                        </div>

                        <div class="col-xs-6 col-md-6 col-sm-6 col-lg-6">
                            <button type="reset" class="btn btn-default btn-block" value="Reset">Reset</button>
                        </div>
                    </div>

                </g:form>
                </div>
            </div>
        </div>
    </div>

</div>

</body>
</html>