<%@ page import="com.coachingPortal.coaching.Batch" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main"/>
</head>

<body>
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <g:render template="/coaching/myCoaching/myBatchNavBar"
                          model="[navBarSelectedItem: 'SubscribedStudents']"/>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <g:render template="/common/errorAndMessage" />
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <h3>${(Batch.findById(Long.parseLong(session.getAttribute('batchId')))).name}->Subscribed Users<a
                        href="${createLink(controller: 'subscribeStudent', action: 'findUserPage')}"
                        class="btn btn-sm btn-primary pull-right">Add</a></h3>
                <hr>
                <g:if test="${!subscribeStudentsList?.empty}">
                    <table class="table table-responsive table-hover">
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Unsubscribe</th>
                            %{--<th>Mail</th>--}%

                        </tr>
                        <tbody>
                        <g:each in="${subscribeStudentsList}" var="subscribeStudent" status="i">
                            <tr>
                                <td>${i + 1}</td>
                                <td>${subscribeStudent?.fullName()}</td>
                                <td>${subscribeStudent?.student.username}</td>
                                <td><a href="${createLink(controller: 'subscribeStudent', action: 'removeUser', params: [studentId: subscribeStudent.id])}"
                                       class="btn btn-sm btn-danger">Unsubscribe</a></td>
                                %{--<td><a href="${createLink(controller: 'coachingMail', action: 'mailToUserForm', params: [mailId: subscribeStudent?.student.username])}"><i
                                        class="fa fa-envelope"></i></a></td>--}%
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </g:if>
                <g:else>
                    <div class="alert alert-info">
                        <i class="fa fa-warning"></i> There are no users subscribed.
                    </div>
                </g:else>
            </div>
        </div>
    </div>

</div>
</body>
</html>