<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main"/>
    <script type="text/javascript">
        function displaySearchedStudent(){
            var searchedUserUsername= $("#searchById").val();

            $.ajax({
                type: "POST",
                url: "${createLink(controller: 'subscribeStudent',action: 'ax_findUser')}",
                data: {searchedUserUsername: searchedUserUsername}
            }).done(function (data) {
                if (data.result == 'SUCCESS') {
                    $("#searchedUserId").html(data.studentList);
                } else {
                    alert(data.errMsg);
                }
            });
        }
    </script>
</head>

<body>
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <g:render template="/coaching/myCoaching/myBatchNavBar" model="[navBarSelectedItem:'SubscribedStudents']"/>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <g:render template="/common/errorAndMessage"/>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <h3>Find User<a href="${createLink(controller: 'subscribeStudent', action: 'index',params: [batchId:batchId])}"
                                class="btn btn-sm btn-primary pull-right">Back to Subscribed users</a></h3>
                <hr>

                <div class="row">
                    <div class="col-lg-8">
                        <input type="text" name="username" id="searchById" class="col-lg-12" placeholder="Username"/>
                    </div>

                    <div class="col-lg-4">
                        <button onclick="displaySearchedStudent()" name="findByUsername" class="btn btn-sm btn-success col-lg-12"> Find By Username</button>
                    </div>
                </div>
                <div class="space20"></div>
                <div class="row">
                    <div id="searchedUserId">

                    </div>
                </div>


            </div>
        </div>
    </div>

</div>
</body>
</html>