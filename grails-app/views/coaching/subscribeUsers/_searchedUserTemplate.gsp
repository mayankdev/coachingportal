<g:if test="${student}">
    <table class="table table-responsive table-hover">
        <tr>
            <th>Name</th>
            <th>Username</th>
            <th>Unsubscribe</th>

        </tr>
        <tbody>
            <tr>
                <td>${student?.fullName()}</td>
                <td>${student?.username}</td>
                <td><g:if test="${isSuscribed=='false'}">
                      <a href="${createLink(controller: 'subscribeStudent',action: 'addUser' ,params: [studentId:student?.id])}" class="btn btn-sm btn-success">Subscribe</a>
                     </g:if>
                    <g:else>
                        <a href="${createLink(controller: 'subscribeStudent',action: 'removeUser' ,params: [studentId:student?.id])}" class="btn btn-sm btn-danger">Unsubscribe</a>
                    </g:else>
                </td>
            </tr>

        </tbody>
    </table>
</g:if>
<g:else>
    <div class="alert alert-info">
        <i class="fa fa-warning"></i> There are no users with this username.
    </div>
</g:else>