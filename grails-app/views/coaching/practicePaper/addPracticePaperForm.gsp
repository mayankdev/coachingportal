<%@ page import="com.coachingPortal.subject.SubjectType" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main"/>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script>
        jQuery(document).ready(function () {
            jQuery("#btnleft").click(function () {
                jQuery("#allsubjects option:selected").each(function () {
                    jQuery("#selectedsubjects").append(jQuery(this).clone());
                    jQuery(this).remove();
                });
            });

            jQuery("#btnright").click(function () {
                jQuery("#selectedsubjects option:selected").each(function () {
                    jQuery("#allsubjects").append(jQuery(this).clone());
                    jQuery(this).remove();
                });
            });
        });

        function validateForm() {

           var selectedSubjectValues = [];

            $('#selectedsubjects option').each(function() {
                selectedSubjectValues.push($(this).val());
            });

            if(selectedSubjectValues.length==0){
                alert('Please add atleast one subject')
                return false;
            }else{
                document.getElementById('selectedSubjectValuesId').value=selectedSubjectValues
                return true;
            }

        }

    </script>
</head>

<body>
<div class="container">

<g:render template="/common/errorAndMessage" model="[someInstance:practicePaper]"/>

    <div class="row">
        <div class="col-lg-12 well">
           <g:render template="/coaching/myCoaching/myBatchNavBar" model="[navBarSelectedItem:'PracticePaper']"/>
         </div>
    </div>


    <div class="row well well-lg">
        <div class="col-lg-12">
            <h3 >Add Practice Paper<a
                    href="${createLink(controller: 'coachingPracticePaper', action: 'index')}"
                    class="btn btn-primary pull-right">Back to Practice Papers</a></h3>
            <hr>

            <form action="${createLink(controller: 'coachingPracticePaper', action: 'addPracticePaper')}" method="post"
                  name="practicePaperForm">
                <input type="hidden" name="selectedSubjectValues" id="selectedSubjectValuesId" value="">
                <div class="form-group col-lg-9 ${hasErrors(bean: practicePaper, field: "examName", 'has-error')}">
                    <label for="examName">Exam Name</label>
                    <input type="text" class="form-control" name="examName" id="examName" placeholder="Enter exam name"
                           autofocus="autofocus" value="${practicePaper?.examName}" required="required">
%{--
                    <helper:renderFieldError bean="${practicePaper}" field="examName"/>
--}%
                </div>

                <div class="form-group col-lg-3 ${hasErrors(bean: practicePaper, field: "totalTime", 'has-error')}">
                    <label for="totalTime">Total Time (in minutes)</label>
                    <input type="text" class="form-control" name="totalTime" id="totalTime"
                           placeholder="Total time" autofocus="autofocus" value="${practicePaper?.totalTime}" required="required">
%{--
                    <helper:renderFieldError bean="${practicePaper}" field="totalTime"/>
--}%
                </div>

                <div class="form-group col-lg-6 col-lg-offset-3" >
                <div style="float: left;">
                    <g:select name="allsubjects" id="allsubjects" multiple="multiple" from="${SubjectType.list().sort { it.subject }}" style="width: 200px;height: 230px;"/>
                </div>
                    <div class="btn" style="float: left; padding-top: 20px;">
                        <br/>
                        <br/>
                        <br/>
                        <input id="btnleft" value=">>>>" type="button" />
                        <br/>
                        <br/>
                        <br/>
                        <input id="btnright" value="<<<<" type="button" />
                    </div>
                    <div style="float: left;">
                        <select name="selectedsubjects" id="selectedsubjects" multiple="multiple" style="width: 200px;height: 230px;">
                        </select>
                    </div>

                </div>


                <div class="form-group">
                       <div class="col-xs-6 col-md-6 col-sm-6 col-lg-6">
                        <button type="submit" class="btn btn-primary btn-block" value="Submit" onclick="return validateForm()">Submit</button>
                       </div>

                    <div class="col-xs-6 col-md-6 col-sm-6 col-lg-6">
                        <button type="reset" class="btn btn-default btn-block" value="Reset">Reset</button>
                    </div>
                </div>

            </form>

        </div>
    </div>
</div>

</body>
</html>