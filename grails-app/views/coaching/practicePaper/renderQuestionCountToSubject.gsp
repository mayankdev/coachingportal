<html>
<head>
    <title></title>
    <meta name="layout" content="main" />
</head>

<body>
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <g:render template="/common/errorAndMessage" />
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 well">
            <g:render template="/coaching/myCoaching/myBatchNavBar" model="[navBarSelectedItem:'PracticePaper']"/>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12 well">
            <h3>Add Questions Count</h3>
            <hr>
                <g:if test="${selectedSubjects.size()>0}">
                    <g:form controller="coachingPracticePaper" action="addQuestionCountToSubject">

                     <g:each in="${selectedSubjects}" var='selectedSubject'>
                     <div class="col-lg-2">
                    <label>${selectedSubject}</label>
                    <input type="number" class="form-control" name="${selectedSubject}" required="required">
                    </div>
                     </g:each>
                        <br>
                        <br>
                        <br>
                        <br>
                     <input type="hidden" name="selectedSubjectsTotal" value="${selectedSubjectsTotal}">
                     <input type="hidden" name="practicePaperId" value="${practicePaperId}">
                    <input type="submit" value="Submit" class="btn btn-success">

                    </g:form>
                </g:if>
                <g:else>
                <div class="alert alert-danger">
                    No subjects added to this paper.
                 </div>
                </g:else>
         </div>
    </div>


 </div>
</body>
</html>