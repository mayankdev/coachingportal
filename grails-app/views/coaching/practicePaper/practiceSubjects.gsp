<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main" />

</head>

<body>
<div class="space20"></div>
<div class="container">

    <div class="row">
        <div class="col-lg-12 well">
            <g:render template="/coaching/myCoaching/myBatchNavBar" model="[navBarSelectedItem:'PracticePaper']"/>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="well well-sm">
                <h3 class="dark_blue_text_color">Practice Paper - ${practicePaper.examName}<g:link controller="coachingPracticePaper" action="index" class="btn btn-primary pull-right">Back to Practice Papers</g:link> </h3>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
                <g:each in="${practicePaper.practicePaperSubjects}" var="practiceSubject">
                    <div class="col-lg-6">
                       <g:render template="/coaching/practicePaper/practiceSubjectQuestionsShort" model="[practiceSubject:practiceSubject,practicePaper:practicePaper]"/>
                    </div>
                </g:each>

        </div>
    </div>


</div>
</body>
</html>
    
	