<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main"/>

</head>

<body>

<div class="container">

<g:render template="/common/errorAndMessage"/>

    <div class="row">
        <div class="col-lg-12 well">
            <g:render template="/coaching/myCoaching/myBatchNavBar" model="[navBarSelectedItem:'PracticePaper']"/>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="well well-sm">
                <h3 class="dark_blue_text_color">${practicePaper?.examName}-${practicePaperSubject?.subjectType}<a href="${createLink(controller:'coachingPracticePaper',action: 'renderSubjects',params: [practicePaperId:practicePaper.id] )}" class="btn  pull-right">Back to subjects</a></h3>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="well well-sm">
                <g:if test="${!questionList?.isEmpty()}">
                    <table class="table table-responsive table-condensed">
                        <tbody>
                        <tr>
                            <th>#</th>
                            <th>Question</th>
                            <th>Action</th>
                        </tr>
                        <g:each in="${questionList}" var="question" status="i">
                            <tr>
                                <td>${i + 1}</td>
                                <td>${raw(question.questionText)}</td>
                                <td>
                                    <g:if test="${practicePaperUniqueIdList.contains(question.uniqueId)}">
                                        <a href="${createLink(controller: 'coachingPracticePaper', action: 'ax_addOrRemoveQuestion', params: [questionUniqueId: question.uniqueId, practicePaperSubjectId: practicePaperSubject.id,practicePaperId:practicePaper.id, questionAction: 'remove'])}"
                                           class="btn btn-sm btn-danger">Remove</a>
                                    </g:if>
                                    <g:else>
                                        <a href="${createLink(controller: 'coachingPracticePaper', action: 'ax_addOrRemoveQuestion', params: [questionUniqueId: question.uniqueId, practicePaperSubjectId: practicePaperSubject.id,practicePaperId:practicePaper.id, questionAction: 'add'])}"
                                           class="btn btn-sm btn-success">Add</a>
                                    </g:else>

                                </td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </g:if>
                <g:else>
                    <div class="alert alert-danger">
                        No questions added by you.
                    </div>
                </g:else>
            </div>
        </div>
    </div>
</div>
</body>
</html>