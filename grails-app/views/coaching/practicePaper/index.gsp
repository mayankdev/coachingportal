<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main"/>
</head>

<body>
<div class="container">
    <g:render template="/common/errorAndMessage"/>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <g:render template="/coaching/myCoaching/myBatchNavBar" model="[navBarSelectedItem:'PracticePaper']"/>
            </div>
        </div>
    </div>


    <div class="row well well-lg">
        <div class="col-lg-12">
            <h3 class="text-primary"><i class="fa fa-book"></i> Practice Papers<a href="${createLink(controller:'coachingPracticePaper',action: 'addPracticePaperForm' )}" class="btn btn-primary pull-right">Add Practice Paper</a></h3>
            <strong>Change the status of practice paper to 'UnPublish' to add or remove questions</strong>
            <hr>
            <g:if test="${!practicePaperList?.empty}">
                <table class="table table-responsive table-hover">
                    <tr>
                        <th>#</th>
                        <th>Exam Name</th>
                        <th>Total time</th>
                        <th>Date Created</th>
                        <th>Topics</th>
                        <th>Status</th>
                        <th>Result</th>
                    </tr>
                    <tbody>
                    <g:each in="${practicePaperList}" var="practicePaper" status="i">
                        <tr>
                            <td>${i + 1}</td>
                            <g:if test="${(practicePaper.practicePaperStatus).toString().equals('UnPublish')}">
                                <td><a href="${createLink(controller:'coachingPracticePaper',action: 'renderSubjects',params: [practicePaperId:practicePaper.id] )}">${practicePaper?.examName}</a> </td>
                            </g:if>
                            <g:else>
                                <td>${practicePaper?.examName} </td>
                            </g:else>
                            <td>${practicePaper?.totalTime}</td>
                            <td>${practicePaper?.dateCreated?.format("MMMM dd, yyyy")}</td>
                            <td>
                                <g:each in="${practicePaper?.practicePaperSubjects}" var="practiceSubjects">
                                    ${practiceSubjects}|
                                </g:each>
                            </td>
                            <td><a href="${createLink(controller:'coachingPracticePaper',action: 'changeStatus',params: [practicePaperId:practicePaper.id])}">${practicePaper?.practicePaperStatus}</a></td>
                            <td><a href="${createLink(controller:'coachingResult',action: 'practicePaperResult',params: [practicePaperId:practicePaper.id])}"><i class="fa fa-file-excel-o"></i></a></td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </g:if>
            <g:else>
                <div class="alert alert-info">
                    <i class="fa fa-warning"></i> There are no practice papers added till now.
                </div>
            </g:else>




        </div>
    </div>
</div>
</body>
</html>