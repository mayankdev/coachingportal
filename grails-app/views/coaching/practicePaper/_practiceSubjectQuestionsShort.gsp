<div class="panel panel-default">
    <div class="panel-heading">${practiceSubject}(${practiceSubject.numOfQuestion})
        <span class="pull-right"><a
                href="${createLink(controller: 'coachingPracticePaper', action: 'renderAddQuestionsToSubject', params: [practiceSubjectId:practiceSubject.id,practicePaperId: practicePaper.id])}">Add or Remove Questions</a>
        </span>
    </div>

    <div class="panel-body">
        <g:if test="${!practiceSubject?.practicePaperQuestions?.isEmpty()}">
            <table class="table table-responsive table-condensed">
                <tbody>
                <tr>
                    <th>#</th>
                    <th>Question</th>
                </tr>
                <g:each in="${practiceSubject?.practicePaperQuestions}" var="practiceQuestion" status="i">
                    <tr>
                        <td>${i + 1}</td>
                        <td>${raw(practiceQuestion?.questionShortText)}</td>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </g:if>
        <g:else>
            <div class="alert alert-danger">
                No questions added yet.
            </div>
        </g:else>
    </div>
</div>