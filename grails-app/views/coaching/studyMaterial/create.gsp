<%@ page import="com.coachingPortal.coaching.Batch" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main"/>
</head>

<body>
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <g:render template="/coaching/myCoaching/myBatchNavBar"
                          model="[navBarSelectedItem: 'StudyMaterial']"/>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <g:render template="/common/errorAndMessage" />
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <h3>${(Batch.findById(Long.parseLong(session.getAttribute('batchId')))).name}->Study Material<a href="${createLink(controller: 'studentTimetable',action: 'create',params: [batchId:batchInstance?.id])}" class="btn btn-sm btn-primary pull-right">Add</a></h3>
                <hr>
                <g:form method="post"  enctype="multipart/form-data" controller="coachingStudyMaterial" action="upload">

                    <label for="fileUpload">Upload:</label>
                    <input type="file" id="fileUpload" name="fileUpload" />

                    <div class="buttons">
                        <span class="button"><g:actionSubmit class="btn btn-success" value="Upload" /></span>
                    </div>
                </g:form>
               </div>
        </div>
    </div>
</div>

</div>
</body>
</html>