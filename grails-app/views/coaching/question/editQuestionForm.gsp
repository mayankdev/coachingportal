<%@ page import="com.coachingPortal.subject.SubjectType; com.enums.Enums" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main"/>

</head>

<body>
<div class="container">


    <div class="row well well-lg">
        <div class="col-lg-12">
            <h3 class="text-primary"><i class="fa fa-book"></i>Add Question
                <a href="${createLink(controller:'coachingQuestion',action: 'bulkUploadQuestionForm' )}" class="btn btn-primary pull-right">Bulk Upload</a>
<a href="${createLink(controller:'coachingQuestion',action: 'index' )}" class="btn btn-primary pull-right">All Questions</a>
</h3>
</div>
</div>

<g:render template="/common/errorAndMessage" model="[someInstance:question]"/>

<div class="row well well-lg">
<div class="col-lg-12">

<g:form url="[controller: 'coachingQuestion', action: 'update']" method="PUT">
<input type="hidden" name="questionUId" value="${question.uniqueId}">
<g:render template="/coaching/question/formEdit"/>

    <div class="form-group">
        <div class="col-xs-6 col-md-6 col-sm-6 col-lg-6">
            <button type="submit" class="btn btn-primary btn-block" onclick="return atLeastOneRadio()" value="Submit">Submit</button>
        </div>

        <div class="col-xs-6 col-md-6 col-sm-6 col-lg-6">
            <button type="reset" class="btn btn-default btn-block" value="Reset">Reset</button>
        </div>
    </div>

    </g:form>


</div>
</div>
</div>

</body>
</html>