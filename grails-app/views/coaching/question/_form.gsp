<%@ page import="com.enums.Enums; com.coachingPortal.subject.SubjectType" %>
<script>
    function atLeastOneRadio() {
        if($('input[type=radio]:checked').size() > 0){
            return true;
        }else{
            alert('Please select  any one answer');
            return false;
        }

    }
</script>

<div class="form-group col-lg-12 ${hasErrors(bean: question, field: "questionText", 'has-error')}">
    <label for="questionText">Question</label>

    <g:textArea rows="5" name="questionText" class="form-control" placeholder="Enter question" autofocus="autofocus" value="${question?.questionText}" />
    %{--
                        <helper:renderFieldError bean="${practicePaper}" field="examName"/>
    --}%
    <p class="text-danger">
        <g:hasErrors bean="${question}">
            <g:fieldError bean="${question}" field="${questionText}" />
        </g:hasErrors>
    </p>

</div>



<div class="form-group col-lg-6 ${hasErrors(bean: question, field: "difficulty", 'has-error')} ">
    <div class="col-lg-12"><label>Select difficulty</label>
    </div>
    <g:select name="difficulty" class="form-control" from="${Enums.Difficulty.values()}" valueMessagePrefix="questions.coaching.question.add.difficulty" value="${question?.difficulty}"/>
</div>

<div class="form-group col-lg-6 ${hasErrors(bean: question, field: "subjectType", 'has-error')}">
    <div class="col-lg-12"><label>Select subject type</label>
    </div>

    <g:select name="subjectType" id="subjectType" class="form-control" from="${SubjectType.list()}" optionValue="subject" value="${question?.subjectType}"/>
</div>


<div class="form-group col-lg-6 ">
    <div class="col-lg-12"><label>Option1</label>
    </div>
    <input type="text" class="form-control" name="option1" id="option1" placeholder="Option 1"  value="${question?.option1}">
</div>

<div class="form-group col-lg-6 ">
    <div class="col-lg-12"><label>Option2</label>
    </div>
    <input type="text" class="form-control" name="option2" id="option2" placeholder="Option 2"  value="${question?.option2}">
</div>

<div class="form-group col-lg-6 ">
    <div class="col-lg-12"><label>Option3</label>
    </div>
    <input type="text" class="form-control" name="option3" id="option3" placeholder="Option 3"  value="${question?.option3}">
</div>

<div class="form-group col-lg-6 ">
    <div class="col-lg-12"><label>Option4</label>
    </div>
    <input type="text" class="form-control" name="option4" id="option4" placeholder="Option 4"  value="${question?.option4}">
</div>


<div class="form-group col-lg-9 ${hasErrors(bean: question?.answer, field: "answerText1", 'has-error')}">
    <div class="col-lg-12"><label>Answer 1</label>
    </div>
    <input type="text" class="form-control" name="answerText1" id="answer1" placeholder="Answer 1"  value="${question?.answer?.answerText1}">
</div>

<div class="form-group col-lg-3 ">
    <div class="col-lg-12"><label></label>
    </div>

        <input type="radio" class="form-control" name="radioAnswer" id="radioAnswer"   value="answerText1" >

</div>


<div class="form-group col-lg-9 ${hasErrors(bean: question?.answer, field: "answerText2", 'has-error')}">
    <div class="col-lg-12"><label>Answer 2</label>
    </div>
    <input type="text" class="form-control" name="answerText2" id="answer2" placeholder="Answer 2"  value="${question?.answer?.answerText2}">
</div>

<div class="form-group col-lg-3 ">
    <div class="col-lg-12"><label></label>
    </div>

        <input type="radio" class="form-control" name="radioAnswer"   value="answerText2" >

</div>



<div class="form-group col-lg-9 ${hasErrors(bean: question?.answer, field: "answerText3", 'has-error')}">
    <div class="col-lg-12"><label>Answer 3</label>
    </div>
    <input type="text" class="form-control" name="answerText3"  placeholder="Answer 3"  value="${question?.answer?.answerText3}">
</div>

<div class="form-group col-lg-3 ">
    <div class="col-lg-12"><label></label>
    </div>

        <input type="radio" class="form-control" name="radioAnswer"  value="answerText3" >

</div>


<div class="form-group col-lg-9 ${hasErrors(bean: question?.answer, field: "answerText4", 'has-error')}">
    <div class="col-lg-12"><label>Answer 4</label>
    </div>
    <input type="text" class="form-control" name="answerText4" id="answer4" placeholder="Answer 4"  value="${question?.answer?.answerText4}">
</div>

<div class="form-group col-lg-3 ">
    <div class="col-lg-12"><label></label>
    </div>

        <input type="radio" class="form-control" name="radioAnswer"   value="answerText4" >

</div>

<br/>