<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main"/>
</head>

<body>
<div class="container">
    <g:if test="${failedUploadQuestions}">
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-danger">
                   Failed questions are--- ${failedUploadQuestions}
                </div>
            </div>
        </div>
    </g:if>


    <div class="row">
        <div class="col-lg-12">
            <g:render template="/common/errorAndMessage"/>
        </div>
    </div>

    <div class="row well well-lg">
        <div class="col-lg-12">
            <h3 class="text-primary"><i class="fa fa-book"></i> Questions
                <a href="${createLink(controller: 'coachingQuestion', action: 'bulkUploadQuestionForm')}"
                   class="btn btn-primary pull-right">Bulk Upload</a>
                <a href="${createLink(controller: 'coachingQuestion', action: 'addQuestionForm')}"
                   class="btn btn-primary pull-right">Add Question</a>
            </h3>
        </div>
    </div>

    <div class="row well well-lg">
        <div class="col-lg-12">

            <g:if test="${!questionList?.empty}">
                <table class="table table-responsive table-hover">
                    <tr>
                        <th>S.no</th>
                        <th>Question</th>
                        <th>Difficulty</th>
                        <th>Subject Type</th>
                        <th>Last Updated</th>
                        <th>Action</th>
                    </tr>
                    <tbody>
                    <g:each in="${questionList}" var="question" status="i">
                        <tr>
                            <td>${i + 1}</td>
                            <td>${question?.questionText}</td>
                            <td>${question?.difficulty}</td>
                            <td>${question?.subjectType}</td>
                            <td>${question?.lastUpdated?.format("MMMM dd, yyyy")}</td>
                            <td><a href="${createLink(controller:'coachingQuestion',action: 'edit',params: [questionId:question.uniqueId])}"><i class="fa fa-pencil"></i></a> |
                            <a href="${createLink(controller:'coachingQuestion',action: 'delete',params: [questionId:question.id])}" onclick="return confirm('${'Are you sure, you want to delete ?'}');"><i class="fa fa-trash"></i></a></td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </g:if>
            <g:else>
                <div class="alert alert-info">
                    <i class="fa fa-warning"></i> There are no  questions added till now.
                </div>
            </g:else>

        </div>
    </div>
</div>
</body>
</html>