<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main"/>
</head>

<body>
<div class="container">
    <g:render template="/common/errorAndMessage"/>
    <div class="row well well-lg">
        <div class="col-lg-12">
            <h3 class="text-primary"><i class="fa fa-book"></i> Bulk Upload Questions <a href="${createLink(controller:'coachingQuestion',action: 'index' )}" class="btn btn-primary pull-right">All Question</a></h3>
        </div>
    </div>

    <div class="row well well-lg">
        <div class="col-lg-12">
            <g:uploadForm action="uploadQuestions">
                <fieldset class="form">
                    <input type="file" name="file" />
                </fieldset>
                <fieldset class="buttons">
                    <g:submitButton name="upload" class="btn btn-success" value="Upload" />
                </fieldset>
            </g:uploadForm>
        </div>
    </div>
</div>
</body>
</html>