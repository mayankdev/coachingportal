<%@ page import="com.coachingPortal.coaching.tasks.Holidays" %>



<div class="fieldcontain ${hasErrors(bean: holidaysInstance, field: 'title', 'error')} required">
	<label for="title">
		<g:message code="holidays.title.label" default="Title" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="title" class="form-control" required="" value="${holidaysInstance?.title}"/>

</div>
<br>
<div class="fieldcontain ${hasErrors(bean: holidaysInstance, field: 'dateOfHoliday', 'error')} required">
	<label for="dateOfHoliday">
		<g:message code="holidays.dateOfHoliday.label" default="Date Of Holiday" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="dateOfHoliday" class="form-control" precision="day"  value="${holidaysInstance?.dateOfHoliday}"  />

</div>
%{--<br>
<div class="fieldcontain ${hasErrors(bean: holidaysInstance, field: 'coaching', 'error')} required">
	<label for="coaching">
		<g:message code="holidays.coaching.label" default="Coaching" />
		<span class="required-indicator">*</span>
	</label>
    <input name="coaching.id" class="form-control" type="text" value="${sec.loggedInUserInfo(field: 'username')}" readonly="readonly" style="background-color: #c0c0c0;"/>

</div>--}%

