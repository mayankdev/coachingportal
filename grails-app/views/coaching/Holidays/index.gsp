<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main" />
</head>

<body>
<div class="container">
    <g:render template="/common/errorAndMessage" model="[someInstance:someInstance]"/>
    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <g:render template="/coaching/myCoaching/myCoachingNavBar" model="[navBarSelectedItem:'Holidays']"/>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <h3>Holidays<a href="${createLink(controller: 'coachingHolidays',action: 'create',params:[taskType:'Holidays'] )}" class="btn btn-sm btn-primary pull-right">Add</a></h3>
                <hr>
                <g:if test="${holidaysInstanceList?.size()>0}">
                    <table class="table table-responsive table-hover">
                        <tr>
                            <th>#</th>
                            <th>Date of Holiday</th>
                            <th>Title</th>
                            <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_COACHING">
                                <th>Action</th>
                            </sec:ifAnyGranted>
                        </tr>
                        <tbody>
                        <g:each in="${holidaysInstanceList}" var="task" status="i">
                            <tr>
                                <td>${i + 1}</td>
                                <td><g:formatDate format="dd-MM-yyyy" date="${task?.dateOfHoliday}"/></td>
                                <td><g:link action="show" id="${task.id}">${task?.title}</g:link></td>
                                <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_COACHING">
                                    <td><g:link controller="coachingHolidays" action="edit" id="${task.id}"><i class="fa fa-pencil"></i></g:link>|
                                        <g:link controller="coachingHolidays" action="delete" id="${task.id}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"><i class="fa fa-trash"></i></g:link></td>
                                </sec:ifAnyGranted>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </g:if>
                <g:else>
                    <div class="alert alert-info">
                        <i class="fa fa-warning"></i> There are no holidays added.
                    </div>
                </g:else>
            </div>
        </div>
    </div>

</div>
</body>
</html>