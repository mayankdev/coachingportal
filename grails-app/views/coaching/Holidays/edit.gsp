<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main" />
</head>

<body>
<div class="container">
    <g:render template="/common/errorAndMessage" model="[someInstance:holidaysInstance]"/>
    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <g:render template="/coaching/myCoaching/myCoachingNavBar" model="[navBarSelectedItem:'Holidays']"/>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-8">
                        <h3>Edit Holiday<a href="${createLink(controller: 'coachingHolidays',action:'index' )}" class="pull-right">Back</a></h3>
                        <hr>
                        <g:form controller="coachingHolidays" action="update" method="PUT" >
                            <input type="hidden" name="id" value="${holidaysInstance.id}">
                            <g:hiddenField name="version" value="${holidaysInstance?.version}" />
                            <fieldset class="form">
                                <g:render template="/coaching/Holidays/form"/>
                            </fieldset>
                            <fieldset class="buttons">
                                <g:submitButton class="btn btn-success" name="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                            </fieldset>
                        </g:form>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
            </div>
        </div>
    </div>

</div>
</body>
</html>