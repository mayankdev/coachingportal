<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main" />
</head>

<body>
<div class="container">
    <g:render template="/common/errorAndMessage" model="[someInstance:holidaysInstance]"/>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <g:render template="/coaching/myCoaching/myCoachingNavBar" model="[navBarSelectedItem:'Holidays']"/>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <h3>Holiday<a href="${createLink(controller: 'studentHolidays',action: 'index')}" class="btn btn-sm btn-primary pull-right">Back</a></h3>
                <hr>
                <div class="row">
                    <div class="col-lg-9">${holidaysInstance.title}</div>
                    <div class="col-lg-3"><strong>Date&nbsp;</strong>
                        ${holidaysInstance.lastUpdated}</div>
                </div>
                <div class="space20"></div>
                <div class="row">
                    <div class="col-lg-12"><strong>Date of Holiday&nbsp;</strong><g:formatDate format="dd-MM-yyyy" date="${holidaysInstance.dateOfHoliday}"/></div>
                </div>
                %{--<strong>Title</strong>
                ${task.title}
                <strong>Description</strong>
                ${task.description}
                <strong>Date</strong>
                ${task.lastUpdated}--}%
            </div>
        </div>
    </div>

</div>
</body>
</html> 