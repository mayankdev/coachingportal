<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'infocenter.label', default: 'Infocenter')}" />
    <title><g:message code="default.create.label" args="[entityName]" /></title>
</head>
<body>
<div class="container">

    <g:render template="/common/errorAndMessage" model="[someInstance:holidaysInstance]"/>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <g:render template="/coaching/myCoaching/myCoachingNavBar" model="[navBarSelectedItem:'Holidays']"/>
            </div>
        </div>

    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="well">

                %{--<g:hasErrors bean="${holidaysInstance}">
                    <div class="alert alert-danger">
                    <ul class="errors" role="alert">
                        <g:eachError bean="${holidaysInstance}" var="error">
                            <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                        </g:eachError>
                    </ul>
                    </div>
                </g:hasErrors>--}%
                <h3>Add Holiday<a href="${createLink(controller: 'coachingHolidays',action:'index' )}" class="pull-right btn btn-primary">Back</a></h3>
                <hr>

                <g:form controller="coachingHolidays" action="save" >

                    <fieldset class="form">
                        <g:render template="/coaching/Holidays/form"/>
                    </fieldset>

                    <fieldset class="buttons">
                        <g:submitButton name="create" class="btn btn-success" value="${message(code: 'default.button.create.label', default: 'Create')}" />
                    </fieldset>


                </g:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
