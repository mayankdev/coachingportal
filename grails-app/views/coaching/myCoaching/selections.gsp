<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main" />
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <g:render template="/coaching/myCoaching/myCoachingNavBar" model="[navBarSelectedItem:'selections']"/>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                Selections<a href="${createLink(controller: 'myCoaching',action: 'addTasksForm')}" class="btn btn-sm btn-info">Add</a>
            </div>
        </div>
    </div>

</div>
</body>
</html>