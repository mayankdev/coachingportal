<div class="row">
    <strong>

        <div class="col-lg-3 text-center">
            <g:if test="${navBarSelectedItem == 'SubscribedStudents'}">
                <a style="color: #000000;">Subscribed Students</a>
            </g:if>
            <g:else>
                <a href="${createLink(controller: 'subscribeStudent', action: 'index')}">Subscribed Students</a>
            </g:else>
        </div>

        <div class="col-lg-3 text-center">
            <g:if test="${navBarSelectedItem == 'PracticePaper'}">
                <a style="color: #000000;">Practice Papers</a>
            </g:if>
            <g:else>
                <a href="${createLink(controller: 'coachingPracticePaper', action: 'index')}">Practice Papers</a>
            </g:else>
        </div>

        <div class="col-lg-3 text-center">
            <g:if test="${navBarSelectedItem == 'StudyMaterials'}">
                <a style="color: #000000;">Study Materials</a>
            </g:if>
            <g:else>
                <a href="${createLink(controller: 'coachingStudyMaterial', action: 'index')}">Study Materials</a>
            </g:else>
        </div>

        <div class="col-lg-3 text-center">
            <g:if test="${navBarSelectedItem == 'TimeTable'}">
                <a style="color: #000000;">Time table</a>
            </g:if>
            <g:else>
                <a href="${createLink(controller: 'coachingTimetable', action: 'index')}">Time table</a>
            </g:else>
        </div>
</strong>
</div>