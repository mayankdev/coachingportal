<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main" />
</head>

<body>
<div class="container">
    <g:hasErrors bean="${tasks}">
        <div class="alert alert-danger">
            <g:renderErrors bean="${tasks}" />
        </div>
    </g:hasErrors>
    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <g:render template="/coaching/myCoaching/myCoachingNavBar" model="[navBarSelectedItem:task.taskType]"/>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-8">
                        <h3>Edit ${task.taskType}<a href="${createLink(controller: 'myCoaching',action:task.taskType )}" class="pull-right">Back</a></h3>
                        <hr>
                        <g:form controller="myCoaching" action="updateTasks">
                            <input type="hidden" name="taskId" value="${task?.id}">
                            <g:textField class="form-control"  name="taskType" value="${task?.taskType}" readonly="readonly"/>
                            <div class="space20"></div>
                            <g:textField class="form-control" name="title"  value="${task?.title}" placeholder="Title"/>
                            <div class="space20"></div>
                            <g:textArea class="form-control"  name="description" rows="5" value="${task?.description}" placeholder="Description"/>
                            <div class="space20"></div>
                            <g:submitButton class="form-control btn btn-default" name="submit" value="Submit"/>
                        </g:form>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
            </div>
        </div>
    </div>

</div>
</body>
</html>