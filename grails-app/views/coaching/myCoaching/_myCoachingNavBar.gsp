<div class="row">
    <strong>

        <div class="col-lg-4 text-center">

            <g:if test="${navBarSelectedItem == 'Infocenter'}">
                <a style="color: #000000;">Infocenter</a>
            </g:if>
            <g:else>
                <a href="${createLink(controller:'coachingInfocenter', action: 'index')}">Infocenter</a>
            </g:else>
        </div>

        <div class="col-lg-4 text-center">

            <g:if test="${navBarSelectedItem == 'Holidays'}">
                <a style="color: #000000;">Holidays</a>
            </g:if>
            <g:else>
                <a href="${createLink(controller: 'coachingHolidays', action: 'index')}">Holidays</a>
            </g:else>
        </div>

            <div class="col-lg-4 text-center">

                <g:if test="${navBarSelectedItem == 'Batches'}">
                    <a style="color: #000000;">Batches</a>
                </g:if>
                <g:else>
                    <a href="${createLink(controller: 'coachingBatch',action: 'index')}">Batches</a>
                </g:else>
            </div>

    </strong>
</div>