<%@ page import="com.coachingPortal.coaching.Batch" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main" />
    <style type="text/css">
        .sms{
            width: 300px;
            height: 270px;
            text-align: center;
            padding-top: 100px;
            background-color:#e99002 ;
            color: #ffffff;
        }

        .mail{
            width: 300px;
            height: 270px;
            text-align: center;
            padding-top: 100px;
            background-color:#358753 ;
            color: #ffffff;
        }
    </style>
</head>

<body>
<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <g:render template="/common/errorAndMessage" />
        </div>
    </div>

    <div class="row">
      <div class="col-lg-4 well col-lg-offset-3 sms">
          <h1><g:link controller="coachingMail" action="sms" style="color:#ffffff;">SMS</g:link></h1>
          Remaining:10<br>
          History

      </div>

      <div class="col-lg-4 well mail">
         <h1> <g:link controller="coachingMail" action="email" style="color:#ffffff;">Email</g:link></h1>
      </div>
    </div>
</div>
</body>
</html> 