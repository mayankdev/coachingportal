<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main" />
</head>

<body>
<div class="container">
    <g:hasErrors bean="${contactUsCO}">
        <div class="alert alert-danger">
            <g:renderErrors bean="${contactUsCO}" />
        </div>
    </g:hasErrors>
%{--<div class="row">
    <div class="col-lg-12">
        <div class="well">
           <h3>Mail</h3>
        </div>
    </div>

</div>--}%
    <div class="space40"></div>

    <div class="row">
        <div class="col-lg-12">
            <div class="well">
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-8">
                        <h3> Mail</h3>
                        <hr>
                        <g:form controller="coachingMail" action="sendMailUser">
                            <g:if test="${mailId}">
                                <g:textField class="form-control"  name="mailId" value="${mailId}" readonly="readonly" />

                            </g:if>
                            <g:else>
                                <g:textField class="form-control"  name="mailId" value="${contactUsCO?.mailId}" placeholder="To(Multiple mail Id separated by comma )"/>

                            </g:else>
                            <div class="space20"></div>
                            <g:textField class="form-control" name="subject"  value="${contactUsCO?.subject}" placeholder="Subject"/>
                            <div class="space20"></div>
                            <g:textArea class="form-control"  name="body" rows="5" value="${contactUsCO?.body}" placeholder="Body"/>
                            <div class="space20"></div>
                            <g:submitButton class="form-control btn btn-default" name="submit" value="Submit"/>
                        </g:form>
                    </div>
                    <div class="col-lg-2"></div>
                </div>
            </div>
        </div>
    </div>

</div>
</body>
</html>