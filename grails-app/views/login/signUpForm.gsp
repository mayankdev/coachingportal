<html>
<head>
    <meta name='layout' content='main'/>
    <title>Login</title>

</head>

<body>
<div class="container">
    <g:render template="/common/errorAndMessage" model="[someInstance:signUpCO]"/>
    <div class="row">
        <div class="col-lg-8 col-lg-offset-2 well">
            <strong>Sign Up</strong>
            <a href="${createLink(controller: 'login',action: 'auth')}" class="pull-right">Login</a><br>
            <hr>

            <g:form controller="login" action="signUpSubmit">
                <div class="row">
                    <div class="col-lg-6">
                    <label for='firstName'>First Name:</label>
                    <input type='text' class='form-control' name='firstName' id='firstName' placeholder="First Name" value="${signUpCO?.firstName}"/>
                     </div>
                    <div class="col-lg-6">
                    <label for='lastName'>Last Name:</label>
                    <input type='text' class='form-control' name='lastName' id='lastName' placeholder="Last Name" value="${signUpCO?.lastName}"/>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <label for='username'>Email Id:</label>
                        <input type='text' class='form-control' name='username' id='username' placeholder="Email Id" />
                    </div>
                    <div class="col-lg-6">
                        <label for='contactNo'>Contact No:</label>
                        <input type='text' class='form-control' name='contactNo' id='contactNo' placeholder="Contact No" value="${signUpCO?.contactNo}"/>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <label for='password'>Password:</label>
                        <input type='password' class='form-control' name='password' id='password' placeholder="Password"/>
                    </div>
                    <div class="col-lg-6">
                        <label for='confirmPassword'>Re-type Password :</label>
                        <input type='password' class='form-control' name='confirmPassword' id='confirmPassword' placeholder="Re-type Password"/>
                    </div>
                </div>
                <div class="space20"></div>
                <p>
                    <input type='submit' class="btn btn-primary" id="submit" value='Submit'/>
                </p>
            </g:form>
        </div>
    </div>
</div>
<script type='text/javascript'>
    <!--
    (function() {
        document.forms['loginForm'].elements['j_username'].focus();
    })();
    // -->
</script>
</body>
</html>
