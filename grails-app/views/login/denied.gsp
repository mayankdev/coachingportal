<head>
<meta name='layout' content='main' />
<title><g:message code="springSecurity.denied.title" /></title>
</head>

<body>
<div class='body'>
    <div class="space100"></div>
    <div class="container">
       <div class="row">
           <div class="col-lg-8 col-lg-offset-2">
	        <div class='errors'><h2><g:message code="springSecurity.denied.message" />
            Please visit the

            <sec:ifLoggedIn>
                <sec:ifAllGranted roles="ROLE_USER">
                    <g:link controller="studentProfile" action="home">home</g:link>
                </sec:ifAllGranted>
                <sec:ifAnyGranted roles="ROLE_ADMIN,ROLE_COACHING">
                    <g:link controller="coachingProfile" action="home">home</g:link>
                </sec:ifAnyGranted>

            </sec:ifLoggedIn>
            <sec:ifNotLoggedIn>
                <g:link controller="login" action="home">home</g:link>
            </sec:ifNotLoggedIn>
            page.</h2>
            </div>
        </div>
     </div>

    </div>
</div>
<div class="space100"></div>
<div class="space60"></div>
</body>
