<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="loginMain" />

</head>

<body>

    <g:render template="/common/errorAndMessage"/>

    <img src="${resource(dir: '/images/partners', file: 'intro-bg.jpg')}" class="headerwrap">

<div class="container wb">
    <div class="row centered">
        <br><br>
        <div class="col-lg-12 text-center">
            <h2><b>WHAT WE ARE</b></h2>
            <p>We are the change makers who try to let the technology enter into the educational domain so that the knowledge can be attained  at any point , at any location.<br>There is one famous saying...<strong>"If a man empties his purse into his head no one can take it away from him. An investment in knowledge always pays the best interest-Benjamin Franklin " </strong></p>
            <p><br/><br/></p>
        </div>


    </div><!-- row -->
</div><!-- container -->
<div class="space40"></div>
    <div class="container w">
    <div class="row">
        <div class="col-lg-12 text-center"><h2><b>FEATURES</b></h2></div>
    </div>
        <div class="row centered">
            <br><br>

            <div class="col-lg-4 text-center">
                <i class="fa fa-briefcase fa-2x"></i>
                <h4>MY COACHING</h4>
                <p>Easily know what's happening in your coaching , whether its the selection of someone, or your holidays or will there be class tomorrow. All the questions related to your coaching has answers inside this.</p>
            </div><!-- col-lg-4 -->

            <div class="col-lg-4 text-center">
                <i class="fa fa-book fa-2x"></i>
                <h4>PRACTICE PAPERS</h4>
                <p>Someone rightly said that practice makes a person perfect.In this race for perfection we are there to help you to compete and acheive your goals.</p>
            </div><!-- col-lg-4 -->
            <div class="col-lg-4 text-center">
                <i class="fa fa-bar-chart-o fa-2x"></i>
                <h4>PERFORMANCE</h4>
                <p>Know your performance as compared to your last exam or with respect to others who have given the exam.</p>
            </div><!-- col-lg-4 -->
            %{--<div class="col-lg-4 text-center">
                <i class="fa fa-globe fa-2x"></i>
                <h4>COACHING LOCATOR</h4>
                <p>Moving here and there and searching for a student is a painful task.Locator will help you to find the student of a particular location just by few clicks.</p>
            </div><!-- col-lg-4 -->--}%

        </div><!-- row -->
        <br>
        <br>
    </div>

<div class="space40"></div>


%{--<div class="container">
    <div class="row centered">
        <div class="row">
            <div class="col-lg-12 text-center"><h2><b>OUR AWESOME PARTNERS</b></h2></div>
        </div>

        <marquee direction="left" behavior="scroll" onmouseover="this.stop();" onmouseout="this.start();"
                 scrollamount="5" height="100px;">
            <img src="${resource(dir: 'images/partners', file: 'c06.png')}" style="margin-right:258px;">
            <img src="${resource(dir: 'images/partners', file: 'c06.png')}" style="margin-right:258px;">
            <img src="${resource(dir: 'images/partners', file: 'c06.png')}" style="margin-right:258px;">
            <img src="${resource(dir: 'images/partners', file: 'c06.png')}" >

        </marquee>



    </div><!-- row -->
</div>--}%<!-- container -->
<div class="space60"></div>
</body>
</html>