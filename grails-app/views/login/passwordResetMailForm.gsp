<html>
<head>
    <meta name='layout' content='main'/>
    <title>Login</title>

</head>

<body>
<div class="container">
    <g:render template="/common/errorAndMessage" model="[someInstance:signUpCO]"/>
    <div class="space20"></div>
    <div class="row">
        <div class="col-lg-4 col-lg-offset-4 well">
            <strong>Password Reset</strong>
            <a href="${createLink(controller: 'login',action: 'auth')}" class="pull-right">Login</a><br>
            <hr>

            <g:form controller="login" action="passwordResetMail">
                <div class="row">
                    <div class="col-lg-12">
                        <label for='mailId'>Email Id:</label>
                        <input type='text' class='form-control' name='mailId' id='mailId' placeholder="Email"/>
                    </div>

                </div>


                <div class="space20"></div>
                <p>
                    <input type='submit' class="btn btn-primary" id="submit" value='Submit'/>
                </p>
            </g:form>
        </div>
    </div>
    <div class="space100"></div>
</div>

</body>
</html>
