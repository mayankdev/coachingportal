<html>
<head>
    <meta name='layout' content='main'/>
    <title>Login</title>

</head>

<body>
<div class="container">
    <g:render template="/common/errorAndMessage" model="[someInstance:passwordResetCO]"/>
    <div class="row">
        <div class="col-lg-4 col-lg-offset-4 well">
            <strong>Reset Password</strong>
           <br>
            <hr>

            <g:form controller="login" action="passwordResetValidate">
                <input type="hidden" name="token" value="${user.uniqueId}"/>
                <p>
                    <label for='mailId'>Email Address:</label>
                    <input type='text' class='form-control' name='mailId' id='mailId' value="${user?.username}" disabled/>
                </p>

                <p>
                    <label for='password'>New Password:</label>
                    <input type='password' class='form-control' name='password' id='password'  placeholder="Password"/>
                </p>

                <p>
                    <label for='confirmPassword'>Confirm Password:</label>
                    <input type='password' class='form-control' name='confirmPassword' id='confirmPassword' placeholder="Confirm Password"/>
                </p>


                <div class="space20"></div>
                <p>
                    <input type='submit' class="btn btn-primary" id="submit" value='Submit'/>
                </p>
            </g:form>
        </div>
    </div>
</div>

</body>
</html>
