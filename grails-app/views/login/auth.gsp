<html>
<head>
	<meta name='layout' content='loginMain'/>
	<title>Login</title>

</head>

<body>
<div class="container">
    <g:render template="/common/errorAndMessage"/>
	<div class="row">
        <div class="col-lg-4 col-lg-offset-4 well">
		<strong><g:message code="springSecurity.login.header"/></strong>

        <hr>

		<form action='${postUrl}' method='POST' id='loginForm' class='cssform'>
			<p>
				<label for='username'><g:message code="springSecurity.login.username.label"/>:</label>
				<input type='text' class='form-control' name='j_username' id='username' placeholder="Username"/>
			</p>

			<p>
				<label for='password'><g:message code="springSecurity.login.password.label"/>:</label>
				<input type='password' class='form-control' name='j_password' id='password' placeholder="Password"/>
			</p>

			<p id="remember_me_holder">
				<input type='checkbox' class='chk' name='${rememberMeParameter}' id='remember_me' <g:if test='${hasCookie}'>checked='checked'</g:if>/>
				<label for='remember_me'><g:message code="springSecurity.login.remember.me.label"/></label>
			</p>

			<p>
				<input type='submit' class="btn btn-primary" id="submit" value='${message(code: "springSecurity.login.button")}'/>
                <a href="${createLink(controller: 'login',action: 'passwordReset')}" class="pull-right">Forgot Password</a>
            </p>
		</form>
        </div>
	</div>
</div>
<br>
<script type='text/javascript'>
	<!--
	(function() {
		document.forms['loginForm'].elements['j_username'].focus();
	})();
	// -->
</script>
</body>
</html>
