<!DOCTYPE html>
<html>
	<head>
		<title><g:if env="development">Grails Runtime Exception</g:if><g:else>Error</g:else></title>
		<meta name="layout" content="main">
		<g:if env="development"><link rel="stylesheet" href="${resource(dir: 'css', file: 'errors.css')}" type="text/css"></g:if>
	</head>
	<body>
		<g:if env="development">
			<g:renderException exception="${exception}" />
		</g:if>
		<g:else>
			<ul class="errors">
				<li>Something went wrong. Please visit the


                <sec:ifLoggedIn>
                    <sec:ifAllGranted roles="ROLE_USER">
                        <g:link controller="studentProfile" action="home">home</g:link>
                    </sec:ifAllGranted>
                    <sec:ifAllGranted roles="ROLE_ADMIN">
                        <g:link controller="coachingProfile" action="home">home</g:link>
                    </sec:ifAllGranted>
                    <sec:ifAllGranted roles="ROLE_COACHING">
                        <g:link controller="coachingProfile" action="home">home</g:link>
                    </sec:ifAllGranted>
                </sec:ifLoggedIn>
                <sec:ifNotLoggedIn>
                    <g:link controller="login" action="home">home</g:link>
                </sec:ifNotLoggedIn>
                page.</li>
			</ul>
		</g:else>
	</body>
</html>
