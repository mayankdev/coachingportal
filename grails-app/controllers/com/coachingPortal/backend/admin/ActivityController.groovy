package com.coachingPortal.backend.admin

import com.coachingPortal.PasswordResetCO
import com.coachingPortal.coaching.Batch
import com.coachingPortal.coaching.SubscribeUser
import com.coachingPortal.security.User
import com.coachingPortal.users.Coaching
import com.coachingPortal.users.Person
import grails.plugins.springsecurity.Secured

/**
 * Created by MAYANK DWIVEDI on 14-Apr-16.
 */
@Secured(['IS_AUTHENTICATED_FULLY','ROLE_ADMIN'])
class ActivityController {
    def springSecurityService

    def userList={
        Set<Coaching> coachingList=Coaching.all
        Set<Person> studentList=Person.all
        render(view: '/admin/activity/userList' ,model:[coachingList:coachingList.sort {it.username},studentList:studentList.sort {it.username}])
    }

    def deactivateUser(String uniqueId){
        User user=User.findByUniqueId(uniqueId)
        if(user.enabled){
            user.enabled=false
        }else{
            user.enabled=true
        }

        user.save(flush: true,failOnError: true)
        redirect(controller: 'activity',action:'userList')

    }

    def passwordReset = {
        if(params.username){
            render(view:'/admin/activity/passwordResetForm' ,model: [username:params.username])
        }else{
            User user=springSecurityService.currentUser
            render(view:'/admin/activity/passwordResetForm' ,model: [username:user.username])
        }
    }

    def passwordResetValidate = { PasswordResetCO passwordResetCO ->

        User user=User.findByUsername(params.uniqueId)

        if (user) {
            if (passwordResetCO.validate()) {
                user.password = passwordResetCO.password
                user.save(flush: true)
                flash.success = 'Password has been reset successfully.'
                render(view: '/admin/activity/passwordResetForm', model: [username: user.username])
            } else {
                render(view: '/admin/activity/passwordResetForm', model: [passwordResetCO: passwordResetCO,username: user.username])
            }
        }else{
            flash.danger = "Something went wrong. Please try again."
            redirect(controller: 'activity', action: 'passwordReset')
        }
    }

    def coachingAllStudents(String uniqueId){
        Coaching coaching=Coaching.findByUniqueId(uniqueId)
        Set<SubscribeUser> subscribeStudentList=new HashSet<SubscribeUser>()
        coaching.batches.each {batch->
            subscribeStudentList.addAll(batch.subscribeStudents)
        }
        render(view:'/admin/activity/coachingAllStudents', model: [subscribeStudentList:subscribeStudentList,coaching:coaching] )
    }

    def deactivateAllStudents(String uniqueId){
        Coaching coaching=Coaching.findByUniqueId(uniqueId)
        Set<SubscribeUser> subscribeStudentList=new HashSet<SubscribeUser>()
        coaching.batches.each {batch->
            subscribeStudentList.addAll(batch.subscribeStudents)
        }

        subscribeStudentList.each {subscribeStudent->
            if(subscribeStudent.student.enabled==true){
                subscribeStudent.student.enabled=false
                subscribeStudent.student.save(flush: true)
            }
        }

        redirect(controller: 'activity',action: 'coachingAllStudents',params: [uniqueId:uniqueId])
    }

    def reAuthenticateUser(String username){
        User user=springSecurityService.currentUser
        session.setAttribute('lastUser',user.username);
        springSecurityService.reauthenticate(username,null);
        redirect(controller: 'login',action: 'resolveTargetUrl');
    }
}
