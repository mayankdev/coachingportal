package com.coachingPortal.backend.student

import com.coachingPortal.coaching.Batch
import com.coachingPortal.practicePaper.PracticePaper
import com.coachingPortal.question.Question
import com.coachingPortal.result.Result
import com.coachingPortal.users.Person
import com.enums.Enums
import com.google.gson.Gson
import grails.converters.JSON
import grails.plugins.springsecurity.Secured

import java.util.concurrent.TimeUnit


@Secured(['IS_AUTHENTICATED_FULLY', 'ROLE_STUDENT'])
class StudentPracticePaperController {
    def springSecurityService

    def index = {
        Batch batch
        boolean isStudentSubscribed=false
        Person student=springSecurityService.currentUser as Person
        if(params.batchId){
            batch=Batch.findById(params.batchId)
            batch.subscribeStudents.each{subscribeStudent->
                if(subscribeStudent.student.id==student.id){
                    isStudentSubscribed=true
                }
            }
            if(isStudentSubscribed){
                session.setAttribute('batchId',params.batchId)
            }

        }else{
            isStudentSubscribed=true
            batch=Batch.findById(Long.parseLong((String)(session.getAttribute('batchId'))))
        }


        //List<PracticePaper> practicePaperList = PracticePaper.findAllByPracticePaperStatusAndCreatedBy(Enums.PracticePaperStatus.PUBLISH,student.subscribeUsers.coaching)
        Set<PracticePaper> practicePaperList = batch.practicePapers
        Map<PracticePaper,String> practicePaperStatusMap=new HashMap<PracticePaper,String>()
        practicePaperList.each{practicePaper->
            if(practicePaper.practicePaperStatus==Enums.PracticePaperStatus.PUBLISH){
            if(Result.findByPracticePaperAndStudent(practicePaper,student)){
                practicePaperStatusMap.put(practicePaper,'Attempted')
            }else{
                practicePaperStatusMap.put(practicePaper,'UnAttempted')
            }
           }
        }


        render(view: '/student/practicePaper/index', model: [practicePaperList: practicePaperList,practicePaperStatusMap:practicePaperStatusMap])
    }


    def instruction = {
        Person student=springSecurityService.currentUser as Person
        PracticePaper practicePaper = PracticePaper.findById(params.practicePaperId)
        Batch batch=Batch.findById(Long.parseLong((String)(session.getAttribute('batchId'))))
        Set<PracticePaper> practicePaperSet=batch.practicePapers
        if(practicePaperSet.contains(practicePaper)){

            if(Result.findByPracticePaperAndStudent(practicePaper,student)){
                flash.message='You have already given the test'
                redirect(controller: 'studentPracticePaper',action: 'index')
             }else{
                render(view: '/student/practicePaper/instruction', model: [practicePaper: practicePaper])
            }
        }else {
            flash.message='You are not allowed to take the test.'
            redirect(controller: 'studentPracticePaper',action: 'index')
        }
    }

    def test = {
        String timeLeft = null
        PracticePaper practicePaper = PracticePaper.findById(params.practicePaperId)
        if (session['endDate']) {
            Date presentDate = new Date()
            Date endDate = (Date) session['endDate']
            println("Start date is ---------" + presentDate + "and end date is --------" + endDate)

            // Get msec from each, and subtract.

            long duration = endDate.getTime() - presentDate.getTime();

            long diffInSeconds = TimeUnit.MILLISECONDS.toSeconds(duration) % 60;
            long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);

            timeLeft = diffInMinutes + ':' + diffInSeconds
        } else {
            Date startDate = new Date()
            long startTime = startDate.getTime();
            Date endDate = new Date(startTime + (practicePaper.totalTime * 60000));
            session['endDate'] = endDate
            timeLeft = practicePaper.totalTime + ':' + '0'

        }
        ArrayList<String> subjectTypeList = new ArrayList<String>()
        practicePaper.practicePaperSubjects.each { examSubject ->
            subjectTypeList.add(examSubject.subjectType)
        }


        println("Sorted list is ---------------" + subjectTypeList)

        ArrayList<Question> questionArrayList = new ArrayList<Question>()
        Map<String, String> questionAnswerMap = new HashMap<String, String>()
        Map<String, String> questionStatusMap = new HashMap<String, String>()
        if (!session['questionAnswerMap']) {


            for (int i = 0; i < subjectTypeList.size(); i++) {
                ArrayList<Question> subjectTypeQuestionArrayList = new ArrayList<Question>()


                practicePaper.practicePaperSubjects.getAt(i).practicePaperQuestions.eachWithIndex { examQuestion, index ->

                    subjectTypeQuestionArrayList.add(examQuestion.question)
                    questionAnswerMap.put(examQuestion.question.uniqueId, '')
                    questionStatusMap.put(examQuestion.question.uniqueId, '')
                }
                if (i == 0) {
                    questionArrayList = subjectTypeQuestionArrayList
                }
                session[practicePaper.practicePaperSubjects.getAt(i).subjectType] = subjectTypeQuestionArrayList

            }
            session['questionAnswerMap'] = questionAnswerMap
            session['questionStatusMap'] = questionStatusMap
        } else {

            questionArrayList = session[subjectTypeList.get(0)]
            questionAnswerMap = session['questionAnswerMap']
            questionStatusMap = session['questionStatusMap']
        }

        Gson gsonInstance = new Gson();



        render(view: '/student/practicePaper/test', model: [practicePaper: practicePaper, timeLeft: timeLeft, subjectTypeList: subjectTypeList, question: questionArrayList.get(0), questionList: questionArrayList, subjectTypeJsonList: gsonInstance.toJson(subjectTypeList), markedAnswer: questionAnswerMap.get(questionArrayList.get(0).uniqueId), questionStatusMap: questionStatusMap])
    }

    def ax_getSelectedSubject = {
        def result = [:]
        if (params.subjectType) {

            ArrayList<Question> questionArrayList = session[params.subjectType]
            Map<String, String> questionAnswerMap = new HashMap<String, String>()
            Map<String, String> questionStatusMap = new HashMap<String, String>()
            questionStatusMap = session['questionStatusMap']
            questionAnswerMap = session['questionAnswerMap']
            questionAnswerMap.put(params.submittedQuestionUID, params.selectedAnswer)
            if (params.selectedAnswer == null) {
                questionStatusMap.put(params.submittedQuestionUID, 'passed')
            } else {
                questionStatusMap.put(params.submittedQuestionUID, 'answered')
            }


            session['questionAnswerMap'] = questionAnswerMap
            session['questionStatusMap'] = questionStatusMap
            Question nxtQuestion = questionArrayList.get(0)
            String markedAnswer = questionAnswerMap.get(nxtQuestion.uniqueId)

            result.subjectTypeQuestion = g.render(template: '/student/practicePaper/questionTemplate', model: [question: nxtQuestion, markedAnswer: markedAnswer])
            result.questionList = g.render(template: '/student/practicePaper/questionSerialNo', model: [questionList: questionArrayList, subjectType: params.subjectType, questionStatusMap: questionStatusMap])
            result.nxtOrPreQuestion = g.render(template: '/student/practicePaper/questionNxtPreTemplate', model: [subjectType: params.subjectType, questionNo: 1, questionListSize: questionArrayList.size()])
            result.result = 'SUCCESS'
            result.successMsg = 'Ajax call successful.'

        } else {
            result.errMsg = "Something went wrong. Please try again."
        }
        render result as JSON
    }

    def ax_getSelectedQuestion = {
        def result = [:]
        if (params.subjectType) {

            ArrayList<Question> questionArrayList = session[params.subjectType]
            Map<String, String> questionAnswerMap = new HashMap<String, String>()
            Map<String, String> questionStatusMap = new HashMap<String, String>()
            questionStatusMap = session['questionStatusMap']
            questionAnswerMap = session['questionAnswerMap']
            questionAnswerMap.put(params.submittedQuestionUID, params.selectedAnswer)
            /*if (params.markedReview == 'markedReview') {
                questionStatusMap.put(params.submittedQuestionUID, 'review')
            } else {*/
            if (params.selectedAnswer == null) {

                questionStatusMap.put(params.submittedQuestionUID, 'passed')
            } else {
                questionStatusMap.put(params.submittedQuestionUID, 'answered')
            }
            /*}*/
            session['questionAnswerMap'] = questionAnswerMap
            session['questionStatusMap'] = questionStatusMap
            Question nxtQuestion=null
            /*if(questionArrayList.size()==Integer.parseInt(params.questionNo)-1){
               nxtQuestion = questionArrayList.get(0)
            }else{*/
            nxtQuestion = questionArrayList.get(Integer.parseInt(params.questionNo) - 1)
            /*}*/

            String markedAnswer = questionAnswerMap.get(nxtQuestion.uniqueId)
            println("Question List size is " + questionArrayList.size())
            result.subjectTypeQuestion = g.render(template: '/student/practicePaper/questionTemplate', model: [question: nxtQuestion, markedAnswer: markedAnswer])

            result.nxtOrPreQuestion = g.render(template: '/student/practicePaper/questionNxtPreTemplate', model: [subjectType: params.subjectType, questionNo: Integer.parseInt(params.questionNo), questionListSize: questionArrayList.size()])
            result.result = 'SUCCESS'
            result.successMsg = 'Ajax call successful.'

        } else {
            result.errMsg = "Something went wrong. Please try again."
        }
        render result as JSON
    }



    def abcd={
        println("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
    }

}
