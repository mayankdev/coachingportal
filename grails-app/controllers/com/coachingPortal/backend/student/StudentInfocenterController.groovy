package com.coachingPortal.backend.student

import com.coachingPortal.coaching.SubscribeUser
import com.coachingPortal.coaching.tasks.Infocenter
import com.coachingPortal.users.Coaching
import com.coachingPortal.users.Person
import grails.plugins.springsecurity.Secured

/**
 * Created by MAYANK DWIVEDI on 07-Apr-16.
 */
@Secured(['IS_AUTHENTICATED_FULLY', 'ROLE_STUDENT'])
class StudentInfocenterController {

    def springSecurityService

    def index(Long coachingId) {

        if(coachingId){
        Coaching coaching= Coaching.findById(coachingId)
        Person student=springSecurityService.currentUser as Person
            boolean isSubscribed=false
        Set<SubscribeUser> subscribeUsers=coaching.batches.subscribeStudents

        subscribeUsers.each {subscribeUser->

            if((subscribeUser.student.id.getAt(0)).equals(student.id)){
                isSubscribed=true
            }

        }


        if(isSubscribed==true){
            session.setAttribute('coachingId',coachingId)
            render(view:'/student/Infocenter/index' ,model:[infocenterInstanceCount: (Infocenter.findAllByCoaching(coaching)).size(), infocenterInstanceList:Infocenter.findAllByCoaching(coaching).sort{it.lastUpdated}])
        }else{
            flash.danger='Student is not subscribed to coaching.'
            redirect(controller:'studentProfile',action: 'home' )
        }

        }else{
            Coaching coaching=Coaching.findById(session.getAttribute('coachingId'))
            render(view:'/student/Infocenter/index' ,model:[infocenterInstanceCount: (Infocenter.findAllByCoaching(coaching)).size(), infocenterInstanceList:Infocenter.findAllByCoaching(coaching).sort{it.lastUpdated}])

        }
    }


    def show(Infocenter infocenterInstance) {
        render(view:'/student/Infocenter/show',model: [infocenterInstance:infocenterInstance])
    }
}
