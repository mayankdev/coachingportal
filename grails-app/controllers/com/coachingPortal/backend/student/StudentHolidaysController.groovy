package com.coachingPortal.backend.student

import com.coachingPortal.coaching.tasks.Holidays
import com.coachingPortal.users.Coaching
import grails.plugins.springsecurity.Secured

@Secured(['IS_AUTHENTICATED_FULLY', 'ROLE_STUDENT'])
class StudentHolidaysController {

    def index() {

        Coaching coaching=Coaching.findById((session.getAttribute('coachingId')))
        render(view:'/student/Holidays/index' ,model:[holidaysInstanceCount: (Holidays.findAllByCoaching(coaching)).size(), holidaysInstanceList:Holidays.findAllByCoaching(coaching).sort{it.dateOfHoliday}])

    }
}
