package com.coachingPortal.backend.student

import com.coachingPortal.coaching.Batch
import com.coachingPortal.practicePaper.PracticePaper
import com.coachingPortal.question.Question
import com.coachingPortal.result.Result
import com.coachingPortal.result.ResultSubject
import com.coachingPortal.security.User
import com.coachingPortal.users.Person
import grails.plugins.springsecurity.Secured

/**
 * Created by MAYANK DWIVEDI on 6/12/2015.
 */
@Secured(['IS_AUTHENTICATED_FULLY', 'ROLE_STUDENT'])
class StudentResultController {

    def springSecurityService

    def index = {
        Map<String, String> questionAnswerMap = new HashMap<String, String>()
        questionAnswerMap = session['questionAnswerMap']
        questionAnswerMap.put(params.submittedQuestionUID, params.selectedAnswer)
        int totalCorrect=0
        int totalInCorrect=0
        int totalUnAttempted=0

        User user = springSecurityService.currentUser as User
        PracticePaper practicePaper = PracticePaper.findByUniqueId(params.practicePaperUniqueId)
        Result result = new Result()
        List<ResultSubject> resultSubjectList=new ArrayList<ResultSubject>()
            practicePaper.practicePaperSubjects.each { examSubject ->
                int correct=0
                int inCorrect=0
                int unAttempted=0

                ArrayList<Question> questionArrayList = session[examSubject.subjectType]


                    questionArrayList.each {question->
                         if(questionAnswerMap.get(question.uniqueId)==question.answer.answerOption){
                            totalCorrect=totalCorrect+1
                            correct=correct+1
                         }else{
                             if(questionAnswerMap.get(question.uniqueId)==''){
                                totalUnAttempted=totalUnAttempted+1
                                 unAttempted=unAttempted+1
                             }else{
                                    totalInCorrect=totalInCorrect+1
                                    inCorrect=inCorrect+1
                                 }
                             }
                    }
                ResultSubject resultSubject=new ResultSubject()
                resultSubject.subjectType=examSubject.subjectType
                resultSubject.questionsCorrect=correct
                resultSubject.questionsIncorrect=inCorrect
                resultSubject.questionsUnattempted=unAttempted
                resultSubjectList.add(resultSubject)

                session.removeAttribute(examSubject.toString())
            }
        result.questionsCorrect=totalCorrect
        result.questionsIncorrect=totalInCorrect
        result.questionsUnattempted=totalUnAttempted
        result.practicePaper=practicePaper
        result.student=user


        Date presentDate = new Date()
        Date endDate = (Date) session['endDate']

        // Get msec from each, and subtract.
        long timeLeft = endDate.getTime() - presentDate.getTime();
        result.timeTaken=practicePaper.totalTime * 60000-timeLeft


        if (result.validate()) {
            result.save(flush: true)
            log.info('Result subject list has-------------'+resultSubjectList)
            resultSubjectList.each {resultSubject ->
                result.addToResultSubjects(resultSubject)
                result.save(flush: true)
            }

            session.removeAttribute('questionAnswerMap')
            session.removeAttribute('questionStatusMap')
            session.removeAttribute('endDate')

            flash.success = 'Exam completed successfully'
            redirect(controller: 'studentResult', action: 'renderResult', params: [resultUniqueId: result.uniqueId] )
        } else {

            flash.danger = 'Something went wrong.'
            render(view: '/student/result/renderResult', model: [practicePaper: practicePaper])
        }



    }

    def renderResult={
        Result result=Result.findByUniqueId(params.resultUniqueId)
        int totalQuestions
        int correctPercent
        int incorrectPercent
        int unattemptedPercent
        if(result!=null){
            totalQuestions=result.questionsCorrect+result.questionsIncorrect+result.questionsUnattempted
            correctPercent=(result.questionsCorrect*100)/totalQuestions
            incorrectPercent=(result.questionsIncorrect*100)/totalQuestions
            unattemptedPercent=100-correctPercent-incorrectPercent
            render(view:'/student/result/renderResult',model: [result:result,totalQuestions:totalQuestions,correctPercent:correctPercent,incorrectPercent:incorrectPercent,unattemptedPercent:unattemptedPercent] )
        }else{
         flash.message='You have not given the test'
            render(view:'/student/result/renderResult',model: [result:result] )
        }
    }

    def testSessions(){
        println("questionAnswerMap------------------------"+session['questionAnswerMap'])
        println("English------------------------"+session['English'])
        println("Aptitude------------------------"+session['Aptitude'])
    }

    def batchResult= {

        Batch batch
        boolean isStudentSubscribed = false
        Person student = springSecurityService.currentUser as Person
        if (params.batchId) {
            batch = Batch.findById(params.batchId)


            batch.subscribeStudents.each { subscribeStudent ->
                if (subscribeStudent.student.id == student.id) {
                    isStudentSubscribed = true
                }
            }
            if (isStudentSubscribed) {
                session.setAttribute('batchId', params.batchId)
            }

        } else {
            isStudentSubscribed = true
            batch = Batch.findById(Long.parseLong((String) (session.getAttribute('batchId'))))
        }


        if (isStudentSubscribed) {
            Set<Result> resultSet = new HashSet<Result>()
            Result result
            batch.practicePapers.each { practicePaper ->
                result = Result.findByStudentAndPracticePaper(student, practicePaper)
                if (result != null) {
                    resultSet.add(result)
                }
            }

            render(view: '/student/result/batchResult', model: [resultSet: resultSet])

        }else {
            flash.danger='Student is not subscribed to batch.'
            redirect(controller: 'studentBatch',action: 'index')
        }
    }

    def allPapersResult={
        Person student = springSecurityService.currentUser as Person
        Set<Result> resultSet=Result.findAllByStudent(student)
        render(view: '/student/result/allPapersResult', model: [resultSet: resultSet])
    }
}
