package com.coachingPortal.backend.student

import com.coachingPortal.PasswordResetCO
import com.coachingPortal.coaching.Batch
import com.coachingPortal.coaching.SubscribeUser
import com.coachingPortal.coaching.Tasks
import com.coachingPortal.coaching.tasks.Holidays
import com.coachingPortal.coaching.tasks.Infocenter
import com.coachingPortal.users.Coaching
import com.coachingPortal.users.ContactDetails
import com.coachingPortal.users.Person
import grails.plugins.springsecurity.Secured


/**
 * Created by MAYANK DWIVEDI on 6/1/2015.
 */
@Secured(['ROLE_STUDENT', 'IS_AUTHENTICATED_FULLY'])
class StudentProfileController {

    def springSecurityService

    def home={
        Person student=springSecurityService.currentUser as Person
        List<SubscribeUser> subscribeUserList=SubscribeUser.findAllByStudent(student)

        Set<Coaching> coachingSet=new HashSet<Coaching>()
        subscribeUserList.each {subscribeUser->
            coachingSet.add(subscribeUser.batch.coaching)
        }

        render(view:'/student/myCoaching/allCoachings',model: [subscribeUserList:subscribeUserList] )
    }


    def coachingHome(Long coachingId) {
        Coaching coaching= Coaching.findById(coachingId)
        Person student=springSecurityService.currentUser as Person
        Set<SubscribeUser> subscribeUsers=coaching.batches.subscribeStudents
        boolean isSubscribed=false
        subscribeUsers.each {subscribeUser->
            if(subscribeUser.student==student){
                isSubscribed=true
            }

        }

        if(isSubscribed){

        session.setAttribute('coachingId',coachingId)

        String InfocenterQuery = "from Infocenter as t where t.coaching=" + coaching.id + " order by t.lastUpdated desc"
        String HolidayQuery = "from Holidays as t where t.coaching=" + coaching.id+ " order by t.lastUpdated desc"

        List<Tasks> infocenterList = Infocenter.findAll(InfocenterQuery, [max: 4])
        List<Tasks> holidayList = Holidays.findAll(HolidayQuery, [max: 4])

        Set<Batch> batchList=coaching.batches

        render(view: '/student/profile/coachingHome', model: [infocenterList: infocenterList, holidayList: holidayList,batchList:batchList])
        }else{
            flash.danger='Student is not subscribed to coaching.'
            redirect(controller:'studentProfile',action: 'home' )
        }
    }

    def index = {
        Person studentInstance=springSecurityService.currentUser as Person
        render(view:'/student/profile/renderProfile',model: [studentInstance:studentInstance,activeLink:'PersonalDetails'])
    }

    def editPersonalDetailsForm={
        Person studentInstance=springSecurityService.currentUser as Person
        render(view: '/student/profile/editPersonalDetailsForm', model: [studentInstance: studentInstance, activeLink: 'PersonalDetails'])
    }

    def submitPersonalDetailsForm = {
        if(params.studentId){
        Person studentInstance =Person.findByUniqueId(params.studentId)
            studentInstance.firstName=params.firstName
            studentInstance.lastName=params.lastName
            studentInstance.contactNo=params.contactNo
        if (studentInstance.validate()) {
            studentInstance.save(flush: true)
            flash.success="Profile updated successfully."
            render(view:'/student/profile/renderProfile',model: [studentInstance:studentInstance,activeLink:'PersonalDetails'])
        }else{
            render(view: '/student/profile/editPersonalDetailsForm', model: [studentInstance: studentInstance, activeLink: 'PersonalDetails'])
        }
        }else{
            flash.success="Profile updated successfully."
            redirect(controller: 'studentProfile',action: 'index')
        }
    }

    def changePasswordForm={
        Person student=springSecurityService.currentUser as Person
        render(view:'/student/profile/changePasswordForm',model: [studentInstance:student,activeLink:'ChangePassword'])
    }

    def submitPasswordForm(Long id,PasswordResetCO passwordResetCO){
        String oldPassword=springSecurityService.encodePassword(params.oldPassword)
        String password=params.password
        String confirmPassword=params.confirmPassword

        Person studentInstance=Person.get(id)

        if(studentInstance.password!=oldPassword){
            flash.danger='Please provide correct old password'
            redirect(controller:'studentProfile',action: 'changePasswordForm' ,params: [studentInstance: studentInstance,activeLink:'ChangePassword'] )

        }else{

            /*if(password!=confirmPassword){
                flash.error='Password and Confirm Password do not match.'
                render(view: "changePasswordForm", model: [personInstance: personInstance])

                }*/

            if(passwordResetCO.validate()){
                studentInstance.password=passwordResetCO.password
                studentInstance.save(flush: true,failOnError: true)
                flash.success='Password updated successfully.'
                render(view: "/student/profile/changePasswordForm", model: [studentInstance: studentInstance,activeLink:'ChangePassword'])
            }else{

                render(view:'/student/profile/changePasswordForm',model: [studentInstance: studentInstance,passwordResetCO:passwordResetCO,activeLink:'ChangePassword'])
            }

        }

    }

    def contactDetailsForm={
        Person person=springSecurityService.currentUser as Person
        render(view: "/student/profile/contactDetailsForm", model: [contactDetailsInstance: person.contactDetails,activeLink:'ContactDetails'])
    }

    def editContactDetailsForm = {
        Person studentInstance=springSecurityService.currentUser as Person
        ContactDetails contactDetailsInstance = studentInstance?.contactDetails
        render(view: "/student/profile/editContactDetailsForm", model: [ contactDetailsInstance: contactDetailsInstance, activeLink: 'ContactDetails'])
    }

    def submitContactDetailsForm = {
        Person user=springSecurityService.currentUser as Person

        ContactDetails contactDetailsInstance = params.contactId ? ContactDetails.findByUniqueId(params.contactId) : new ContactDetails()
        bindData(contactDetailsInstance, params, ['user'])

        contactDetailsInstance.user=user
        if(contactDetailsInstance.validate()){
            contactDetailsInstance.save(flush: true)
            flash.success="Contact details updated successfully."
            render(view:'/student/profile/contactDetailsForm',model: [contactDetailsInstance:contactDetailsInstance,activeLink:'ContactDetails'])
        }else{
            render(view: '/student/profile/editContactDetailsForm', model: [ contactDetailsInstance: contactDetailsInstance, activeLink: 'ContactDetails'])
        }
    }


}
