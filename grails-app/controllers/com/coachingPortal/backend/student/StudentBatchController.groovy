package com.coachingPortal.backend.student

import com.coachingPortal.coaching.Batch
import com.coachingPortal.coaching.SubscribeUser
import com.coachingPortal.users.Coaching
import com.coachingPortal.users.Person
import grails.plugins.springsecurity.Secured

@Secured(['IS_AUTHENTICATED_FULLY', 'ROLE_STUDENT'])
class StudentBatchController {
    def springSecurityService
    def index() {
        Coaching coaching=Coaching.findById(session.getAttribute('coachingId'))
        Person student=springSecurityService.currentUser as Person
        Set<Batch> subscribedBatches=new HashSet<Batch>()
        coaching.batches.each {batch->
            batch.subscribeStudents.each {subscribeStudent->
                if(subscribeStudent.student.id==student.id){
                    subscribedBatches.add(batch)
                }
            }
        }

       render(view: '/student/Batch/index',model: [batchList:subscribedBatches])
    }
}
