package com.coachingPortal.backend.student

import com.coachingPortal.coaching.Batch
import com.coachingPortal.coaching.tasks.Timetable
import com.coachingPortal.users.Person
import grails.plugins.springsecurity.Secured

/**
 * Created by MAYANK DWIVEDI on 07-Apr-16.
 */
@Secured(['IS_AUTHENTICATED_FULLY', 'ROLE_STUDENT'])
class StudentTimetableController {

    def springSecurityService

    def index(){
        Batch batch
        boolean isStudentSubscribed=false
        if(params.batchId){
            batch=Batch.findById(params.batchId)
            Person student=springSecurityService.currentUser as Person

            batch.subscribeStudents.each{subscribeStudent->
                if(subscribeStudent.student.id==student.id){
                    isStudentSubscribed=true
                }
            }
            if(isStudentSubscribed){
                session.setAttribute('batchId',params.batchId)
            }

        }else{
            isStudentSubscribed=true
            batch=Batch.findById(Long.parseLong((String)(session.getAttribute('batchId'))))
        }

        if(isStudentSubscribed){
            log.info('Student subscribed')
            Set<Timetable> batchTimetableSet=batch?.timetable

            List<Timetable> batchTimetableSortedList1=new LinkedList<Timetable>()
            List<Timetable> batchTimetableSortedList2=new LinkedList<Timetable>()
            List<Timetable> batchTimetableSortedList3=new LinkedList<Timetable>()
            List<Timetable> batchTimetableSortedList4=new LinkedList<Timetable>()
            List<Timetable> batchTimetableSortedList5=new LinkedList<Timetable>()
            List<Timetable> batchTimetableSortedList6=new LinkedList<Timetable>()
            List<Timetable> batchTimetableSortedList7=new LinkedList<Timetable>()



            batchTimetableSet.each { timetable ->
                switch (timetable.day) {
                    case 'Sunday': batchTimetableSortedList1.add(timetable)
                        break;
                    case 'Monday': batchTimetableSortedList2.add(timetable)
                        break;
                    case 'Tuesday': batchTimetableSortedList3.add(timetable)
                        break;
                    case 'Wednesday': batchTimetableSortedList4.add(timetable)
                        break;
                    case 'Thursday': batchTimetableSortedList5.add(timetable)
                        break;
                    case 'Friday': batchTimetableSortedList6.add(timetable)
                        break;
                    case 'Saturday': batchTimetableSortedList7.add(timetable)
                        break;

                }
            }
            batchTimetableSortedList1.sort({it.startTime}).reverse()
            batchTimetableSortedList2.sort({it.startTime}).reverse()
            batchTimetableSortedList3.sort({it.startTime}).reverse()
            batchTimetableSortedList4.sort({it.startTime}).reverse()
            batchTimetableSortedList5.sort({it.startTime}).reverse()
            batchTimetableSortedList6.sort({it.startTime}).reverse()
            batchTimetableSortedList7.sort({it.startTime}).reverse()

            List<Timetable> batchTimetableList=new ArrayList<Timetable>()

            batchTimetableList.addAll(batchTimetableSortedList1)
            batchTimetableList.addAll(batchTimetableSortedList2)
            batchTimetableList.addAll(batchTimetableSortedList3)
            batchTimetableList.addAll(batchTimetableSortedList4)
            batchTimetableList.addAll(batchTimetableSortedList5)
            batchTimetableList.addAll(batchTimetableSortedList6)
            batchTimetableList.addAll(batchTimetableSortedList7)

            render(view: '/student/Timetable/index', model: [timetableList: batchTimetableList, batchInstance: batch, timetableInstanceCount: batch?.timetable?.size()])

        }else {
            flash.danger='Student is not subscribed to batch.'
            redirect(controller: 'studentBatch',action: 'index')
        }

    }
}
