package com.coachingPortal.backend.student

import com.coachingPortal.coaching.Batch
import com.coachingPortal.studyMaterial.StudyMaterial
import com.coachingPortal.users.Coaching
import com.coachingPortal.users.Person
import grails.plugins.springsecurity.Secured

/**
 * Created by MAYANK DWIVEDI on 11-Apr-16.
 */
@Secured(['IS_AUTHENTICATED_FULLY', 'ROLE_STUDENT'])
class StudentStudyMaterialController {
    def springSecurityService
    def index = {

        Batch batch
        boolean isStudentSubscribed=false
        if(params.batchId){
            batch=Batch.findById(params.batchId)
            Person student=springSecurityService.currentUser as Person

            batch.subscribeStudents.each{subscribeStudent->
                if(subscribeStudent.student.id==student.id){
                    isStudentSubscribed=true
                }
            }
            if(isStudentSubscribed){
                session.setAttribute('batchId',params.batchId)
            }

        }else{
            isStudentSubscribed=true
            batch=Batch.findById(Long.parseLong((String)(session.getAttribute('batchId'))))
        }


            if(isStudentSubscribed){
                List<StudyMaterial> studyMaterialList=StudyMaterial.findAllByBatch(batch)
                render(view: '/student/studyMaterial/list',model: [studyMaterialList:studyMaterialList])
            }else{
                flash.danger='Student is not subscribed to batch.'
                redirect(controller: 'studentBatch',action: 'index')
            }
      }


    def downloadFile = {

        StudyMaterial studyMaterial=StudyMaterial.findBySmUniqueId(params.smId)
        if(studyMaterial){
            String filePath=grailsApplication.config.studyMaterial.location.toString()+ '/'+studyMaterial.batch.coaching.id+'/'+studyMaterial.storedLocation
            File fileInstance = new File(filePath)
            if (fileInstance == null) {
                flash.message = "Document not found."
                redirect(action: 'list')
            } else {
                response.setContentType("APPLICATION/OCTET-STREAM")
                response.setHeader("Content-Disposition", "Attachment;Filename=\"${fileInstance.name}\"")
                def fileInputStream = new FileInputStream(fileInstance)
                def outputStream = response.getOutputStream()
                outputStream << fileInstance.bytes
                outputStream.flush()
                outputStream.close()
                fileInputStream.close()
            }
        }
    }

}
