package com.coachingPortal.backend.coaching

import com.coachingPortal.coaching.Batch
import com.coachingPortal.users.Coaching
import grails.plugins.springsecurity.Secured
import grails.transaction.Transactional

/**
 * Created by MAYANK DWIVEDI on 2/12/2016.
 */
@Secured(['IS_AUTHENTICATED_FULLY', 'ROLE_STUDENT', 'ROLE_COACHING'])
@Transactional(readOnly = true)
class CoachingBatchController {

    def springSecurityService

    def index = {
        Coaching coaching = springSecurityService.currentUser as Coaching
        Set<Batch> batchList = coaching.batches
        render(view: '/coaching/Batch/index', model: [batchList: batchList])
    }

    def create = {
        render(view: '/coaching/Batch/create')
    }

    @Transactional
    def save(Batch batchInstance) {
        Coaching coaching = springSecurityService.currentUser as Coaching
        coaching.addToBatches(batchInstance)

        if (batchInstance == null) {
            notFound()
            return
        }

        if (!batchInstance.validate()) {
            render(view: '/coaching/Batch/create', model: [batchInstance: batchInstance])
        } else {
            coaching.save(flush: true)
            flash.success = 'Batch created successfully'
            redirect(action: "index")
        }

    }
}
