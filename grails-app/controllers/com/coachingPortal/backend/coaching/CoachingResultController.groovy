package com.coachingPortal.backend.coaching

import com.coachingPortal.ApachePOI
import com.coachingPortal.practicePaper.PracticePaper
import com.coachingPortal.result.Result
import grails.plugins.springsecurity.Secured

import javax.servlet.http.HttpServletResponse

/**
 * Created by MAYANK DWIVEDI on 6/12/2015.
 */
@Secured(['IS_AUTHENTICATED_FULLY', 'ROLE_ADMIN', 'ROLE_COACHING'])
class CoachingResultController {

    def practicePaperResult = {

        PracticePaper practicePaper = PracticePaper.findById(params.practicePaperId)
        List<Result> resultList = Result.findAllByPracticePaper(practicePaper)
        HttpServletResponse response = response
        ApachePOI apachePOI = new ApachePOI()

        apachePOI.resultPracticePaperAdmin(response, resultList, practicePaper.examName)


    }
}
