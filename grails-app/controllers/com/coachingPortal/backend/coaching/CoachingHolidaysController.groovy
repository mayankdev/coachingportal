package com.coachingPortal.backend.coaching

import com.coachingPortal.coaching.tasks.Holidays
import com.coachingPortal.users.Coaching
import grails.plugins.springsecurity.Secured
import grails.transaction.Transactional

import static org.springframework.http.HttpStatus.NOT_FOUND
import static org.springframework.http.HttpStatus.OK

@Secured(['IS_AUTHENTICATED_FULLY', 'ROLE_STUDENT','ROLE_COACHING'])
@Transactional(readOnly = true)
class CoachingHolidaysController {

    def springSecurityService

    /*static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]*/

    def index(Integer max) {
        Coaching coaching=springSecurityService.currentUser as Coaching
        params.max = Math.min(max ?: 10, 100)
        render(view:'/coaching/Holidays/index' ,model:[holidaysInstanceCount: (Holidays.findAllByCoaching(coaching)).size(), holidaysInstanceList:Holidays.findAllByCoaching(coaching).sort{it.dateOfHoliday}])
    }

    def show(Holidays holidaysInstance) {
        render(view:'/coaching/Holidays/show',model: [holidaysInstance:holidaysInstance])
    }

    def create() {
        render(view:'/coaching/Holidays/create',model: [holidaysInstance:new Holidays(params)])
    }

    @Transactional
    def save() {
        Holidays holidaysInstance=new Holidays(params)
        Coaching coaching=springSecurityService.currentUser as Coaching
        coaching.addToHolidays(holidaysInstance)
        if (holidaysInstance == null) {
            redirect(controller: 'coachingHolidays',action:'index' )
            return
        }

        if (!holidaysInstance.validate()) {
            render(view: 'create',model: [holidaysInstance:holidaysInstance])
        }else {
            coaching.save(flush: true)
            flash.success='Holiday added successfully'
            redirect(controller: 'coachingHolidays',action:'index' )
        }

    }

    def edit(Holidays holidaysInstance) {
        render(view: '/coaching/Holidays/edit',model: [holidaysInstance:holidaysInstance])
    }

    @Transactional
    def update(Holidays holidaysInstance) {
        if (holidaysInstance == null) {
            redirect(controller: 'coachingInfocenter',action: 'index')
            return
        }

        if (holidaysInstance.hasErrors()) {
            render(view: '/coaching/Holidays/edit',model: [holidaysInstance:holidaysInstance])
            return
        }

        holidaysInstance.save(insert: false, update: true)

        flash.success='Holiday updated successfully'
        redirect(controller: 'coachingHolidays',action: 'index')
    }

    @Transactional
    def delete(Holidays holidaysInstance) {

        if (holidaysInstance == null) {
            notFound()
            return
        }

        holidaysInstance.delete(flush:true)
        flash.message='Holiday deleted successfully'
        redirect controller: 'coachingHolidays', action:"index", method:"GET"
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'holidays.label', default: 'Holidays'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
