package com.coachingPortal.backend.coaching

import com.coachingPortal.coaching.Batch
import com.coachingPortal.studyMaterial.StudyMaterial
import com.coachingPortal.users.Coaching
import grails.transaction.Transactional

/**
 * Created by MAYANK DWIVEDI on 07-Mar-16.
 */
@Transactional(readOnly = true)
class CoachingStudyMaterialController {
    def springSecurityService

    def index = {

        Long batchId
        println('batch id in params is------------------'+params.batchId)
        if(params.batchId){
            Batch batch =Batch.findById(Long.parseLong(params.batchId))
            Coaching coaching=springSecurityService.currentUser as Coaching
            if(batch?.coaching==coaching){
                session.setAttribute('batchId', params.batchId)
                batchId=Long.parseLong(params.batchId)
            }else{
                flash.danger='This batch does not belong to coaching.'
                redirect(controller: 'studentBatch',action: 'index')
            }
        }else{
            batchId=Long.parseLong(session.getAttribute('batchId'))
            println('bbbbbbbbbbbbbb Suscribe students bbbbbbbbbbbbbb')
        }

        List<StudyMaterial> studyMaterialList=StudyMaterial.findAllByBatch(Batch.findById(batchId))
        render(view: '/coaching/studyMaterial/list',model: [studyMaterialList:studyMaterialList])
    }

    def create={
        render(view:'/coaching/studyMaterial/create')
    }

 @Transactional
    def upload(){
        def f = request.getFile('fileUpload')
        if(!f.empty) {
            Coaching coaching=springSecurityService.currentUser as Coaching

            StudyMaterial studyMaterial=new StudyMaterial()
            String filePath=grailsApplication.config.studyMaterial.location.toString()+ File.separatorChar+coaching.id
            print('File pth is-------------'+filePath)

           /*
            File name has to be updated with the new unique file name

            String newFileName=f.getOriginalFilename()+studyMaterialCount

            Stored location is not taking the path from coaching

            */
            String storedLocation='/'+coaching.id+'/'+f.getOriginalFilename()

            println('Stored location is-----------------'+storedLocation)

            studyMaterial.name=f.getOriginalFilename()
            studyMaterial.storedName=f.getOriginalFilename()
            studyMaterial.storedLocation=f.getOriginalFilename()
            studyMaterial.paymentType='unpaid'
            studyMaterial.openStatus='private'

            Batch batch=Batch.findById(Long.parseLong(session.getAttribute('batchId')))


            batch.addToStudyMaterials(studyMaterial)
            if(studyMaterial.validate()){

                if(batch.validate()){
                    batch.save(flush: true,failOnError: true)
                }else{
                    flash.danger ='Something went wrong. Please try again.'
                    redirect(controller:'coachingStudyMaterial' , action:'create' )
                }


                new File( filePath).mkdirs()
                f.transferTo( new File( filePath+ File.separatorChar +f.getOriginalFilename()) )
                flash.success = 'Study material has been uploaded successfully.'


                redirect(controller:'coachingStudyMaterial',action: 'index')
                }else{
                studyMaterial.errors.each {it->
                    println('errors are-----------------'+it)
                }
                flash.danger ='Something went wrong. Please try again.'
                redirect(controller:'coachingStudyMaterial' , action:'create' )
            }
          }else {
            flash.danger ='File cannot be empty'
            redirect(controller:'coachingStudyMaterial' , action:'create' )
        }

    }

    def delete = {

        StudyMaterial studyMaterial=StudyMaterial.findBySmUniqueId(params.smId)
        String filePath=grailsApplication.config.studyMaterial.location.toString()+ '/'+studyMaterial.batch.coaching.id+'/'+studyMaterial.storedLocation

        if(studyMaterial){
        boolean success = (new File(filePath)).delete();
        if (success) {
            flash.success = "File ${studyMaterial.name} deleted successfully."
            studyMaterial.delete(flush:true )
           }else{
            flash.danger ='Something went wrong.'
            }
        }
        redirect( controller:'coachingStudyMaterial' , action:'index' )
    }


    def downloadFile = {

        StudyMaterial studyMaterial=StudyMaterial.findBySmUniqueId(params.smId)
        if(studyMaterial){
        String filePath=grailsApplication.config.studyMaterial.location.toString()+ '/'+studyMaterial.batch.coaching.id+'/'+studyMaterial.storedLocation
        File fileInstance = new File(filePath)
        if (fileInstance == null) {
            flash.message = "Document not found."
            redirect(action: 'list')
        } else {
            response.setContentType("APPLICATION/OCTET-STREAM")
            response.setHeader("Content-Disposition", "Attachment;Filename=\"${fileInstance.name}\"")
            def fileInputStream = new FileInputStream(fileInstance)
            def outputStream = response.getOutputStream()
            outputStream << fileInstance.bytes
            outputStream.flush()
            outputStream.close()
            fileInputStream.close()
        }
      }
    }
}
