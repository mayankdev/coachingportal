package com.coachingPortal.backend.coaching

import com.coachingPortal.question.Answer
import com.coachingPortal.question.Question
import com.coachingPortal.subject.SubjectType
import com.coachingPortal.users.Coaching
import com.enums.Enums
import grails.plugins.springsecurity.Secured
import grails.transaction.Transactional
import org.apache.poi.hssf.usermodel.HSSFRow
import org.apache.poi.hssf.usermodel.HSSFSheet
import org.apache.poi.hssf.usermodel.HSSFWorkbook
import org.apache.poi.ss.usermodel.DataFormatter

/**
 * Created by MAYANK DWIVEDI on 6/2/2015.
 */
@Secured(['IS_AUTHENTICATED_FULLY', 'ROLE_ADMIN', 'ROLE_COACHING'])
class CoachingQuestionController {

    def springSecurityService

    def index = {
        Coaching admin = springSecurityService.currentUser as Coaching
        List<Question> questionList = Question.findAllByCreatedBy(admin)
        render(view: '/coaching/question/index', model: [questionList: questionList])
    }

    def addQuestionForm = {
        render(view: '/coaching/question/addQuestionForm')
    }

    def addQuestion= {
        Question question = new Question()
        Answer answer = new Answer()

        Coaching admin = springSecurityService.currentUser as Coaching

        question.createdBy = admin
        question.questionText = params.questionText
        question.difficulty = params.difficulty
        question.subjectType = params.subjectType
        question.option1 = params.option1
        question.option2 = params.option2
        question.option3 = params.option3
        question.option4 = params.option4



        if (params.radioAnswer == 'answerText1') {
            answer.answerOption = params.answerText1
        } else if (params.radioAnswer == 'answerText2') {
            answer.answerOption = params.answerText2
        } else if (params.radioAnswer == 'answerText3') {
            answer.answerOption = params.answerText3
        } else if (params.radioAnswer == 'answerText4') {
            answer.answerOption = params.answerText4
        }


        answer.answerText1 = params.answerText1
        answer.answerText2 = params.answerText2
        answer.answerText3 = params.answerText3
        answer.answerText4 = params.answerText4
        question.answer = answer


        if (question.validate()) {

            question.save(flush: true)
            flash.success = 'Question added successfully.'
            redirect(controller: 'coachingQuestion', action: 'index')

        } else {
            List<String> subjectType = SubjectType.list().unique().sort { it.subject }
            render(view: '/coaching/question/addQuestionForm', model: [question: question, radioAnswer: params.radioAnswer, subjectType: subjectType])
        }
    }


    @Transactional
    def edit(String questionId){
        Question question=Question.findByUniqueId(questionId)

        render(view: '/coaching/question/editQuestionForm',model: [question:question])
    }


    def update(){

        log.info('LOGGER-----------------Inside update method')
        Question question=Question.findByUniqueId(params.questionUId)

        question.questionText = params.questionText
        question.difficulty = params.difficulty
        question.subjectType = params.subjectType
        question.option1 = params.option1
        question.option2 = params.option2
        question.option3 = params.option3
        question.option4 = params.option4



        if (params.radioAnswer == 'answerText1') {
            question.answer.answerOption = params.answerText1
        } else if (params.radioAnswer == 'answerText2') {
            question.answer.answerOption = params.answerText2
        } else if (params.radioAnswer == 'answerText3') {
            question.answer.answerOption = params.answerText3
        } else if (params.radioAnswer == 'answerText4') {
            question.answer.answerOption = params.answerText4
        }


        question.answer.answerText1 = params.answerText1
        question.answer.answerText2 = params.answerText2
        question.answer.answerText3 = params.answerText3
        question.answer.answerText4 = params.answerText4


        if(question.validate()){
            question.save(flush:true,failOnError: true )
            flash.success='Question updated successfully'
            redirect(controller: 'coachingQuestion',action: 'index')
        }else{
           redirect(controller: 'coachingQuestion',action: 'edit',params: [questionId:question.uniqueId])
        }

    }

    def bulkUploadQuestionForm={
        render(view:'/coaching/question/bulkUploadQuestionForm')
    }


    def uploadQuestions() {

        def file = request.getFile('file')

        // inp = new FileInputStream(uploadedFile);

        HSSFWorkbook book = new HSSFWorkbook(file.getInputStream());
        HSSFSheet sheet = book.getSheetAt(0)


        Coaching coaching = springSecurityService.currentUser as Coaching

        List<Question> failedUploadQuestions=new ArrayList<Question>()
        DataFormatter formatter = new DataFormatter(); //creating formatter using the default locale
        int rowsCount = sheet.getLastRowNum()+1;
        for(int i = 1; i <rowsCount; i++){
            Question question= new Question()
            Answer answer=new Answer()
            question.createdBy=coaching
            HSSFRow row = sheet.getRow(i);

            question.questionText= formatter.formatCellValue(row?.getCell(0))
            question.difficulty= Enums.Difficulty.valueOf(formatter.formatCellValue(row?.getCell(1)))
            question.subjectType= formatter.formatCellValue(row?.getCell(2))
            question.option1= formatter.formatCellValue(row?.getCell(3))
            question.option2= formatter.formatCellValue(row?.getCell(4))
            question.option3= formatter.formatCellValue(row?.getCell(5))
            question.option4= formatter.formatCellValue(row?.getCell(6))

            answer.answerText1= formatter.formatCellValue(row?.getCell(7))
            answer.answerText2= formatter.formatCellValue(row?.getCell(8))
            answer.answerText3= formatter.formatCellValue(row?.getCell(9))
            answer.answerText4= formatter.formatCellValue(row?.getCell(10))
            answer.answerOption= formatter.formatCellValue(row?.getCell(11))

            question.answer=answer

            if(question.validate()){
                question.save(flush: true,failOnError: true)
            }else{
                failedUploadQuestions.add(question)
            }
        }

        Coaching admin = springSecurityService.currentUser as Coaching
        List<Question> questionList = Question.findAllByCreatedBy(admin)
        if(failedUploadQuestions.size()==0){
            flash.success='All questions uploaded successfully.'
        }
        render(view: '/coaching/question/index', model: [questionList: questionList,failedUploadQuestions:failedUploadQuestions])
    }

    @Transactional
    def delete(Long questionId){
        Question questionInstance =Question.findById(questionId)

        if (questionInstance !=null) {
           flash.success='Question deleted successfully'
            questionInstance.delete(flush: true)
        }else{
           flash.danger='Something went wrong'
        }
        redirect(controller: 'coachingQuestion',action: 'index')
    }




}
