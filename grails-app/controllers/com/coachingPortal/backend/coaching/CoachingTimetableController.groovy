package com.coachingPortal.backend.coaching

import com.coachingPortal.coaching.Batch
import com.coachingPortal.coaching.tasks.Timetable
import com.coachingPortal.users.Coaching
import grails.transaction.Transactional

import static org.springframework.http.HttpStatus.NOT_FOUND

@Transactional(readOnly = true)
class CoachingTimetableController {

    def springSecurityService

    def index() {
        Long batchId
        if (params.batchId) {
            Batch batch =Batch.findById(Long.parseLong(params.batchId))
            Coaching coaching=springSecurityService.currentUser as Coaching
            if(batch?.coaching==coaching){
                session.setAttribute('batchId', params.batchId)
                batchId=Long.parseLong(params.batchId)
            }else{
                flash.danger='This batch does not belong to coaching.'
                redirect(controller: 'coachingTimetable',action: 'index')
            }
        } else {
            batchId = Long.parseLong(session.getAttribute('batchId'))
        }
        Batch batch = Batch.findById(batchId)

        Set<Timetable> batchTimetableSet=batch?.timetable

        List<Timetable> batchTimetableSortedList1=new LinkedList<Timetable>()
        List<Timetable> batchTimetableSortedList2=new LinkedList<Timetable>()
        List<Timetable> batchTimetableSortedList3=new LinkedList<Timetable>()
        List<Timetable> batchTimetableSortedList4=new LinkedList<Timetable>()
        List<Timetable> batchTimetableSortedList5=new LinkedList<Timetable>()
        List<Timetable> batchTimetableSortedList6=new LinkedList<Timetable>()
        List<Timetable> batchTimetableSortedList7=new LinkedList<Timetable>()



        batchTimetableSet.each { timetable ->
            switch (timetable.day) {
                case 'Sunday': batchTimetableSortedList1.add(timetable)
                    break;
                case 'Monday': batchTimetableSortedList2.add(timetable)
                    break;
                case 'Tuesday': batchTimetableSortedList3.add(timetable)
                    break;
                case 'Wednesday': batchTimetableSortedList4.add(timetable)
                    break;
                case 'Thursday': batchTimetableSortedList5.add(timetable)
                    break;
                case 'Friday': batchTimetableSortedList6.add(timetable)
                    break;
                case 'Saturday': batchTimetableSortedList7.add(timetable)
                    break;

            }
        }
            batchTimetableSortedList1.sort({it.startTime}).reverse()
            batchTimetableSortedList2.sort({it.startTime}).reverse()
            batchTimetableSortedList3.sort({it.startTime}).reverse()
            batchTimetableSortedList4.sort({it.startTime}).reverse()
            batchTimetableSortedList5.sort({it.startTime}).reverse()
            batchTimetableSortedList6.sort({it.startTime}).reverse()
            batchTimetableSortedList7.sort({it.startTime}).reverse()

            List<Timetable> batchTimetableList=new ArrayList<Timetable>()

            batchTimetableList.addAll(batchTimetableSortedList1)
            batchTimetableList.addAll(batchTimetableSortedList2)
            batchTimetableList.addAll(batchTimetableSortedList3)
            batchTimetableList.addAll(batchTimetableSortedList4)
            batchTimetableList.addAll(batchTimetableSortedList5)
            batchTimetableList.addAll(batchTimetableSortedList6)
            batchTimetableList.addAll(batchTimetableSortedList7)

        render(view: '/coaching/Timetable/index', model: [timetableList: batchTimetableList, batchInstance: batch, timetableInstanceCount: batch?.timetable?.size()])
    }

    def show(Timetable timetableInstance) {
        respond timetableInstance
        render(view:'/coaching/Timetable/show',model: [timetableInstance:timetableInstance])
    }

    def create() {
        render(view: '/coaching/Timetable/create', model: [timetableInstance: new Timetable(params)])
    }

    @Transactional
    def save(Timetable timetableInstance) {
        timetableInstance.startTime = params.startHours + ':' + params.startMinutes
        timetableInstance.duration = params.durationHours + ':' + params.durationMinutes

        Batch batch = Batch.findById(Long.parseLong(session.getAttribute('batchId')))
        println("Batch name in save is---------" + batch.name)
        batch.addToTimetable(timetableInstance)
        if (timetableInstance == null) {
            redirect(controller: 'coachingTimetable',action: 'index')
            return
        }

        if (!timetableInstance.validate()) {
            render(view: '/coaching/Timetable/create', model: [timetableInstance: timetableInstance])
        } else {
            batch.save(flush: true)
            flash.success = 'Timetable added successfully'
            redirect(controller: 'coachingTimetable', action: 'index')
/*
            render(view:"show",model: [timetableInstance:timetableInstance])
*/
        }
    }

    def edit(Long timetableId) {
        Timetable timetableInstance = Timetable.findById(timetableId)
        if (Long.parseLong(session.getAttribute('batchId')) == timetableInstance.batchId) {

            String[] durationSplit = timetableInstance.duration.split(':')
            String[] startTimeSplit = timetableInstance.startTime.split(':')

            render(view: '/coaching/Timetable/edit', model: [timetableInstance: timetableInstance, durationHours: durationSplit[0], durationMinutes: durationSplit[1], startHours: startTimeSplit[0], startMinutes: startTimeSplit[1]])
        } else {
            flash.danger = 'Something went wrong. Please try again'
            redirect(controller: 'coachingTimetable', action: 'index')
        }
    }

    @Transactional
    def update(Timetable timetableInstance) {
        if (timetableInstance == null) {
            redirect(controller: 'coachingTimetable',action: 'index')
            return
        }

        if (Long.parseLong(session.getAttribute('batchId')) == timetableInstance.batchId) {
            timetableInstance.startTime = params.startHours + ':' + params.startMinutes
            timetableInstance.duration = params.durationHours + ':' + params.durationMinutes

            if (timetableInstance.hasErrors()) {
                respond timetableInstance.errors, view: 'edit'
                return
            }

            timetableInstance.save flush: true
            flash.success = 'Timetable updated successfully'
        } else {
            flash.danger = 'Something went wrong. Please try again'
        }
        redirect(controller: 'coachingTimetable', action: 'index')
    }

    @Transactional
    def delete(Long timetableId) {
        Timetable timetableInstance = Timetable.findById(timetableId)

        if (timetableInstance == null) {
            notFound()
            return
        }
        if (Long.parseLong(session.getAttribute('batchId')) == timetableInstance.batchId) {
            timetableInstance.delete flush: true
            flash.success = 'Timetable deleted successfully'
          } else {
            flash.danger = 'Something went wrong. Please try again'
        }
        redirect(controller: 'coachingTimetable', action: 'index')
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'timetable.label', default: 'Timetable'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
