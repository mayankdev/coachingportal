package com.coachingPortal.backend.coaching

import com.coachingPortal.subject.SubjectType
import grails.converters.JSON
import grails.plugins.springsecurity.Secured

/**
 * Created by MAYANK DWIVEDI on 6/21/2015.
 */
@Secured(['IS_AUTHENTICATED_FULLY', 'ROLE_ADMIN', 'ROLE_COACHING'])
class CoachingSubjectController {
    def index = {
        render(view: '/coaching/subject/addSubjectForm')
    }

    /*def addSubject={

        String subjectTypeValue=params.subject
        String subjectSubTypeValue=params.subSubject
        String topicValue=params.topic

        SubjectCO subjectCO=new SubjectCO()
        subjectCO.subject=subjectTypeValue
        subjectCO.subSubject=subjectSubTypeValue
        subjectCO.topic=topicValue

        if(subjectCO.validate()){
            SubjectType subjectTypeInstance=null
            SubjectSubType subjectSubTypeInstance=null
            Topic topicInstance=null

            subjectTypeInstance=SubjectType.findBySubject(subjectTypeValue)
            if(!subjectTypeInstance){
             subjectTypeInstance=new SubjectType(subjectTypeValue)
             subjectTypeInstance.save(flush: true,failOnError: true)
            }

            subjectSubTypeInstance=SubjectSubType.findBySubSubject(subjectSubTypeValue)
            if(!subjectSubTypeInstance){
                subjectSubTypeInstance=new SubjectSubType(subjectSubTypeValue)
            }
            subjectTypeInstance.addToSubjectSubTypes(subjectSubTypeInstance)

            subjectSubTypeInstance.save(flush: true,failOnError: true)

            topicInstance=Topic.findByTopic(topicValue)
            if(!topicInstance){
                topicInstance=new Topic(topicValue)
            }

            subjectSubTypeInstance.addToTopics(topicInstance)
            topicInstance.save(flush: true,failOnError: true)

            flash.success='Subject added successfully'
            render(view: '/coaching/subject/addSubjectForm')


        }else{
            render(view: '/coaching/subject/addSubjectForm',model: [subjectCO:subjectCO])
        }
    }*/

    def ax_renderSubjectNames = {
        println("Inside ajax method----------------------")
        List<String> subjectNames = SubjectType.createCriteria().list {
            ilike("subject", params.term + '%')
        }*.subject.unique()
        println("List is------------" + subjectNames)
        render subjectNames as JSON
    }
}
