package com.coachingPortal.backend.coaching

import com.coachingPortal.coaching.Batch
import com.coachingPortal.practicePaper.PracticePaper
import com.coachingPortal.practicePaper.PracticePaperQuestion
import com.coachingPortal.practicePaper.PracticePaperSubject
import com.coachingPortal.question.Question
import com.coachingPortal.users.Coaching
import com.enums.Enums
import grails.plugins.springsecurity.Secured

/**
 * Created by MAYANK DWIVEDI on 5/16/2015.
 */
@Secured(['IS_AUTHENTICATED_FULLY', 'ROLE_ADMIN', 'ROLE_COACHING'])
class CoachingPracticePaperController {

    def springSecurityService

    def index = {
        Long batchId
        if (params.batchId) {
            Batch batch =Batch.findById(Long.parseLong(params.batchId))
            Coaching coaching=springSecurityService.currentUser as Coaching
            if(batch?.coaching==coaching){
                session.setAttribute('batchId', params.batchId)
                batchId=Long.parseLong(params.batchId)
            }else{
                flash.danger='This batch does not belong to caoching.'
                redirect(controller: 'studentBatch',action: 'index')
            }
        } else {
            batchId = Long.parseLong(session.getAttribute('batchId'))
        }
        Batch batch = Batch.findById(batchId)

        render(view: '/coaching/practicePaper/index', model: [practicePaperList: batch?.practicePapers])

    }

    def addPracticePaperForm = {
       render(view: '/coaching/practicePaper/addPracticePaperForm')
    }

    def addPracticePaper() {

        println("Params are -----------------" + params)
        println("Practice paper time are -----------------" + params.totalTime)
        println("Practice paper Name are -----------------" + params.examName)
        String selectedSubjects=params.selectedSubjectValues
        println("Practice paper subjects are -----------------" + selectedSubjects)

        PracticePaper practicePaper=new PracticePaper()
        practicePaper.totalTime=Integer.parseInt(params.totalTime)
        practicePaper.examName=params.examName
        practicePaper.paymentType='unpaid'
        practicePaper.openStatus='private'



        Coaching coaching = springSecurityService.currentUser as Coaching
        practicePaper.createdBy=coaching

        if(practicePaper.validate()){

            String[] subjects=selectedSubjects?.split(',')
            println('subjects are------------------------'+subjects)
            if(subjects==null||subjects.length==0){
                flash.danger='Please select at least one subject.'
                redirect(controller: 'coachingPracticePaper',action: 'addPracticePaperForm',params: [practicePaper:practicePaper])
            }else{
                Batch batch = Batch.findById(Long.parseLong(session.getAttribute('batchId')))
                batch.addToPracticePapers(practicePaper)
                batch.save(flush: true,failOnError: true)
                redirect(controller: 'coachingPracticePaper',action: 'renderQuestionCountToSubject',params: [practicePaperId:practicePaper.id,selectedSubjects:selectedSubjects])
            }


        }else{
            practicePaper.errors.each{
                println('Errors are-----------------'+it)
            }
            render(view: '/coaching/practicePaper/addPracticePaperForm',model: [practicePaper:practicePaper])
        }


    }

    def renderQuestionCountToSubject(Long practicePaperId,String selectedSubjects ){
        render(view: '/coaching/practicePaper/renderQuestionCountToSubject' ,model:[practicePaperId:practicePaperId ,selectedSubjects: selectedSubjects.split(','),selectedSubjectsTotal:selectedSubjects] )
    }

    def addQuestionCountToSubject={
        String allSelectedSubjects=params.selectedSubjectsTotal
        String[] subjects=allSelectedSubjects.split(',')
        Long practicePaperId=Long.parseLong(params.practicePaperId)
        PracticePaper practicePaper=PracticePaper.findById(practicePaperId)
        String noOfQuestions=null
        int totalQuestions=0
        subjects.each {subject->
            noOfQuestions="${params.get(subject)}"
           PracticePaperSubject practicePaperSubject=new PracticePaperSubject(subject,Integer.parseInt(noOfQuestions))
            practicePaper.addToPracticePaperSubjects(practicePaperSubject)
            if(practicePaperSubject.validate()){
                totalQuestions=totalQuestions+Integer.parseInt(noOfQuestions)
                 practicePaper.save(flush: true,failOnError: true)
                }else{
                     practicePaperSubject.errors.each {
                   log.error('errors are-----------------'+it)
                    }
                }
        }

        practicePaper.totalQuestions=totalQuestions
        if(practicePaper.validate()) {
            practicePaper.save(flush: true, failOnError: true)
        }

        flash.success='Questions count added successfully.'
        redirect(controller: 'coachingPracticePaper',action: 'renderSubjects',params: [practicePaperId:practicePaper.id])
    }

    def renderSubjects(Long practicePaperId){
        if (practicePaperId) {
            PracticePaper practicePaper = PracticePaper.findById(params.practicePaperId)
            render(view: '/coaching/practicePaper/practiceSubjects', model: [practicePaper: practicePaper])

        } else {
            flash.message = 'Please select some practice paper'
            render(view: '/coaching/practicePaper/index')
        }
    }

    def renderAddQuestionsToSubject = {

        PracticePaper practicePaper = PracticePaper.findById(params.practicePaperId)
        if (params.practiceSubjectId) {
            Coaching admin = springSecurityService.currentUser as Coaching
            PracticePaperSubject practicePaperSubject = PracticePaperSubject.findById(params.practiceSubjectId)

            Set<PracticePaperQuestion> practicePaperQuestionSet = practicePaperSubject.practicePaperQuestions
            List<Question> questionList = Question.findAllBySubjectTypeAndCreatedBy(practicePaperSubject.subjectType, admin)
            List<String> practicePaperUniqueIdList = new ArrayList<>()
            practicePaperQuestionSet.each { practicePaperQuestion ->
                practicePaperUniqueIdList.add(practicePaperQuestion.question.uniqueId)
            }
            println("Practice paper is---------" + practicePaper)
            render(view: '/coaching/practicePaper/renderAddQuestionsToSubject', model: [questionList: questionList, practicePaperUniqueIdList: practicePaperUniqueIdList, practicePaperSubject: practicePaperSubject, practicePaper: practicePaper])

        } else {
            flash.message = 'Please select some practice paper'
            redirect(controller: 'coachingPracticePaper', action: 'index')
        }
    }

    def ax_addOrRemoveQuestion = {


        if (params.practicePaperSubjectId) {
            String questionAction = params.questionAction
            PracticePaperSubject practicePaperSubject = PracticePaperSubject.findById(params.practicePaperSubjectId)
            PracticePaper practicePaper = PracticePaper.findById(params.practicePaperId)

            if (questionAction == 'add') {

                if (practicePaperSubject.numOfQuestion == practicePaperSubject.practicePaperQuestions.size()) {
                    flash.danger = 'Questions could not be added further as maximum questions are added.'
                } else {
                    PracticePaperQuestion practicePaperQuestion = new PracticePaperQuestion()
                    practicePaperQuestion.question = Question.findByUniqueId(params.questionUniqueId)
                    practicePaperSubject.addToPracticePaperQuestions(practicePaperQuestion)
                    practicePaperQuestion.save(flush: true)
                    flash.success = 'Question added successfully.'
                }
                redirect(controller: 'coachingPracticePaper', action: 'renderAddQuestionsToSubject', params: [practiceSubjectId: practicePaperSubject.id, practicePaperId: practicePaper.id])
            } else {
                PracticePaperQuestion practicePaperQuestion = PracticePaperQuestion.findByQuestion(Question.findByUniqueId(params.questionUniqueId))
                practicePaperQuestion.delete(flush: true)
                flash.success = 'Question removed successfully.'
                redirect(controller: 'coachingPracticePaper', action: 'renderAddQuestionsToSubject', params: [practiceSubjectId: practicePaperSubject.id, practicePaperId: practicePaper.id])
            }

        } else {
            flash.message = 'Please select some practice paper'
            redirect(controller: 'coachingPracticePaper', action: 'index')
        }


    }

    def changeStatus = {

        if (params.practicePaperId) {
            int isAllowed = 1
            PracticePaper practicePaper = PracticePaper.findById(params.practicePaperId)

            practicePaper.practicePaperSubjects.each { practicePaperSubject ->
                if (practicePaperSubject.practicePaperQuestions.size() == 0) {
                    isAllowed = 0
                }
            }

            if (isAllowed == 0) {
                flash.message = 'Please add atleast one question to each subject and then change the status.'
                redirect(controller: 'coachingPracticePaper', action: 'index')
            } else {

                if (practicePaper.practicePaperStatus == Enums.PracticePaperStatus.PUBLISH) {
                    practicePaper.practicePaperStatus = Enums.PracticePaperStatus.UNPUBLISH
                } else {
                    practicePaper.practicePaperStatus = Enums.PracticePaperStatus.PUBLISH
                }
                if (practicePaper.validate()) {
                    practicePaper.save(flush: true, failOnError: true)
                    flash.success = 'Status changed successfully'
                } else {
                    flash.danger = 'Status could not be changed'
                }
                redirect(controller: 'coachingPracticePaper', action: 'index')
            }

        } else {
            flash.message = 'Something went wrong.Please try again later'
            redirect(controller: 'coachingPracticePaper', action: 'index')
        }

    }
}
