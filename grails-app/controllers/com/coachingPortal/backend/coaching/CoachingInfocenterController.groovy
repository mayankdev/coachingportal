package com.coachingPortal.backend.coaching

import com.coachingPortal.coaching.tasks.Infocenter
import com.coachingPortal.users.Coaching
import grails.plugins.springsecurity.Secured
import grails.transaction.Transactional

import static org.springframework.http.HttpStatus.NOT_FOUND

@Secured(['IS_AUTHENTICATED_FULLY', 'ROLE_STUDENT','ROLE_COACHING'])
@Transactional(readOnly = true)
class CoachingInfocenterController {

    def springSecurityService
/*
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
*/

    def index(Integer max) {
        Coaching coaching=springSecurityService.currentUser as Coaching
        params.max = Math.min(max ?: 10, 100)
        render(view:'/coaching/Infocenter/index' ,model:[infocenterInstanceCount: (Infocenter.findAllByCoaching(coaching)).size(), infocenterInstanceList:Infocenter.findAllByCoaching(coaching).sort{it.lastUpdated}])

    }

    def show(Infocenter infocenterInstance) {
        render(view:'/coaching/Infocenter/show',model: [infocenterInstance:infocenterInstance])
    }

    def create() {
        render(view:'/coaching/Infocenter/create',model: [infocenterInstance:new Infocenter(params)])
    }

    @Transactional
    def save() {
        Infocenter infocenterInstance=new Infocenter(params)
        Coaching coaching=springSecurityService.currentUser as Coaching
        infocenterInstance.coaching=coaching
        coaching.addToInfocenter(infocenterInstance)

        if (infocenterInstance == null) {
            redirect(controller: 'coachingInfocenter',action: 'index')
            return
        }

        if (!infocenterInstance.validate()) {
            render(view: '/coaching/Infocenter/create',model: [infocenterInstance:infocenterInstance])
        }else {
            coaching.save flush: true
            flash.success='Information added successfully'
            redirect(controller: 'coachingInfocenter',action: 'index')
        }
    }

    def edit(Infocenter infocenterInstance) {
        render(view: '/coaching/Infocenter/edit',model: [infocenterInstance:infocenterInstance])
        }

    @Transactional
    def update(Infocenter infocenterInstance) {
        if (infocenterInstance == null) {
            redirect(controller: 'coachingInfocenter',action: 'index')
            return
        }

        if (infocenterInstance.hasErrors()) {
            render(view: '/coaching/Infocenter/edit',model: [infocenterInstance:infocenterInstance])
            return
        }

        infocenterInstance.save(insert: false, update: true)

        flash.success='Information updated successfully'
        redirect(controller: 'coachingInfocenter',action: 'index')
    }

    @Transactional
    def delete(Infocenter infocenterInstance) {

        if (infocenterInstance == null) {
            notFound()
            return
        }

        infocenterInstance.delete flush:true

        /*request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Infocenter.label', default: 'Infocenter'), infocenterInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }*/

        flash.success='Information deleted successfully'
        redirect controller: 'coachingInfocenter', action:"index", method:"GET"
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'infocenter.label', default: 'Infocenter'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
