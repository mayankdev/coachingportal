package com.coachingPortal.backend.coaching

import com.coachingPortal.coaching.Tasks
import com.coachingPortal.users.Coaching
import grails.plugins.springsecurity.Secured

/**
 * Created by MAYANK DWIVEDI on 6/13/2015.
 */
@Secured(['IS_AUTHENTICATED_FULLY', 'ROLE_ADMIN', 'ROLE_COACHING'])
class MyCoachingController {

    def springSecurityService

    def index = {
        redirect(controller: 'myCoaching', action: 'timeTable')
    }

    def selections = {

        render(view: '/coaching/myCoaching/selections')
    }

    def timeTable = {
        Coaching admin = springSecurityService.currentUser as Coaching
        List<Tasks> tasksList = Tasks.findAllByTaskTypeAndCreatedBy('TimeTable', admin).sort {
            it.lastUpdated
        }.reverse()
        render(view: '/coaching/myCoaching/timeTable', model: [tasksList: tasksList])
    }

    def infocenter = {
        Coaching admin = springSecurityService.currentUser as Coaching
        List<Tasks> tasksList = Tasks.findAllByTaskTypeAndCreatedBy('Infocenter', admin).sort {
            it.lastUpdated
        }.reverse()
        render(view: '/coaching/myCoaching/infocenter', model: [tasksList: tasksList])
    }

    def holidays = {
        Coaching admin = springSecurityService.currentUser as Coaching
        List<Tasks> tasksList = Tasks.findAllByTaskTypeAndCreatedBy('Holiday', admin).sort { it.lastUpdated }.reverse()
        render(view: '/coaching/myCoaching/holidays', model: [tasksList: tasksList])

    }

    def addTasksForm = {
        render(view: '/coaching/myCoaching/addTasksForm', model: [taskType: params.taskType])
    }

    def addTasks = {
        Coaching createdBy = springSecurityService.currentUser as Coaching
        Tasks tasks = new Tasks()
        tasks.title = params.title
        tasks.description = params.description
        tasks.taskType = params.taskType
        tasks.createdBy = createdBy

        if (tasks.validate()) {
            tasks.save()
            flash.success = 'Task created successfully'
            if (params.taskType == 'Holiday') {
                flash.success = 'Holiday added successfully'
                redirect(controller: 'myCoaching', action: 'holidays')
            } else {
                if (params.taskType == 'Infocenter') {
                    flash.success = 'Infocenter updated successfully'
                    redirect(controller: 'myCoaching', action: 'infocenter')
                } else {
                    flash.success = 'Timetable updated successfully'
                    redirect(controller: 'myCoaching', action: 'timeTable')
                }
            }

        } else {
            render(view: '/coaching/myCoaching/addTasksForm', model: [tasks: tasks, taskType: params.taskType])

        }

    }


    def editTasksForm = {
        Tasks task = Tasks.findById(params.taskId)
        render(view: '/coaching/myCoaching/editTasksForm', model: [task: task])
    }

    def updateTasks = {
        Tasks tasks = Tasks.findById(params.taskId)

        tasks.title = params.title
        tasks.description = params.description



        if (tasks.validate()) {
            tasks.save()
            flash.success = 'Task created successfully'
            if (params.taskType == 'Holiday') {
                flash.success = 'Holiday added successfully'
                redirect(controller: 'myCoaching', action: 'holidays')
            } else {
                if (params.taskType == 'Infocenter') {
                    flash.success = 'Infocenter updated successfully'
                    redirect(controller: 'myCoaching', action: 'infocenter')
                } else {
                    flash.success = 'Timetable updated successfully'
                    redirect(controller: 'myCoaching', action: 'timeTable')
                }
            }

        } else {
            render(view: '/coaching/myCoaching/addTasksForm', model: [tasks: tasks, taskType: params.taskType])

        }

    }

    def deleteTask = {
        Tasks task = Tasks.findById(params.taskId)
        task.delete(flush: true)
        if (params.taskType == 'Holiday') {
            flash.success = 'Holiday deleted successfully'
            redirect(controller: 'myCoaching', action: 'holidays')
        } else {
            if (params.taskType == 'Infocenter') {
                flash.success = 'Infocenter deleted successfully'
                redirect(controller: 'myCoaching', action: 'infocenter')
            } else {
                flash.success = 'Timetable deleted successfully'
                redirect(controller: 'myCoaching', action: 'timeTable')
            }
        }

    }

    def taskDescription = {
        Tasks task = Tasks.findById(params.taskId)
        render(view: '/coaching/myCoaching/taskDescription', model: [task: task])
    }

}
