package com.coachingPortal.backend.coaching

import com.coachingPortal.PasswordResetCO
import com.coachingPortal.coaching.Batch
import com.coachingPortal.coaching.Tasks
import com.coachingPortal.coaching.tasks.Holidays
import com.coachingPortal.coaching.tasks.Infocenter
import com.coachingPortal.users.Coaching
import com.coachingPortal.users.ContactDetails
import grails.plugins.springsecurity.Secured
import grails.transaction.Transactional

/**
 * Created by MAYANK DWIVEDI on 6/1/2015.
 */
@Secured(['IS_AUTHENTICATED_FULLY', 'ROLE_ADMIN', 'ROLE_COACHING'])
class CoachingProfileController {

    def springSecurityService

    def index = {
        Coaching coachingInstance = springSecurityService.currentUser as Coaching
        render(view: '/coaching/profile/renderProfile', model: [adminInstance: coachingInstance, activeLink: 'PersonalDetails'])
    }

    def home = {

        Coaching coaching = springSecurityService.currentUser as Coaching

        String InfocenterQuery = "from Infocenter as t where t.coaching=" + coaching.id + " order by t.lastUpdated desc"
        String HolidayQuery = "from Holidays as t where t.coaching=" + coaching.id+ " order by t.lastUpdated desc"

        List<Tasks> infocenterList = Infocenter.findAll(InfocenterQuery, [max: 4])
        List<Tasks> holidayList = Holidays.findAll(HolidayQuery, [max: 4])

        Set<Batch> batchList=coaching.batches

        render(view: '/coaching/profile/home', model: [infocenterList: infocenterList, holidayList: holidayList,batchList:batchList])

    }



    def editPersonalDetailsForm = {
        Coaching adminInstance = springSecurityService.currentUser as Coaching
        render(view: '/coaching/profile/editPersonalDetailsForm', model: [adminInstance: adminInstance, activeLink: 'PersonalDetails'])
    }

    def submitPersonalDetailsForm = {
        if (params.coachingId) {
            Coaching coachingInstance = Coaching.findByUniqueId(params.coachingId)
            coachingInstance.name = params.name
            log.info('COaching new name is ------------------'+params.name)
            coachingInstance.contactNo =Long.parseLong(params.contactNo)
            if (coachingInstance.validate()) {
                coachingInstance.save(flush: true)
                flash.success = "Profile updated successfully."
                render(view: '/coaching/profile/renderProfile', model: [adminInstance: coachingInstance, activeLink: 'PersonalDetails'])
            } else {
                render(view: '/coaching/profile/editPersonalDetailsForm', model: [adminInstance: coachingInstance, activeLink: 'PersonalDetails'])
            }
        } else {
            flash.danger = "Something went wrong, try again later"
            redirect(controller: 'coachingProfile', action: 'index')
        }
    }

    def changePasswordForm = {
        Coaching adminInstance = springSecurityService.currentUser as Coaching
        render(view: '/coaching/profile/changePasswordForm', model: [adminInstance: adminInstance, activeLink: 'ChangePassword'])
    }

    def submitPasswordForm(Long id, PasswordResetCO passwordResetCO) {
        String oldPassword = springSecurityService.encodePassword(params.oldPassword)

        Coaching adminInstance = Coaching.get(id)

        if (adminInstance.password != oldPassword) {
            flash.danger = 'Please provide correct old password'
            redirect(controller: 'coachingProfile', action: 'changePasswordForm', params: [adminInstance: adminInstance, activeLink: 'ChangePassword'])

        } else {

            /*if(password!=confirmPassword){
                flash.error='Password and Confirm Password do not match.'
                render(view: "changePasswordForm", model: [personInstance: personInstance])

                }*/

            if (passwordResetCO.validate()) {
                adminInstance.password = passwordResetCO.password
                adminInstance.save(flush: true, failOnError: true)
                flash.success = 'Password updated successfully.'
                render(view: "/coaching/profile/changePasswordForm", model: [adminInstance: adminInstance, activeLink: 'ChangePassword'])
            } else {

                render(view: '/coaching/profile/changePasswordForm', model: [adminInstance: adminInstance, passwordResetCO: passwordResetCO, activeLink: 'ChangePassword'])
            }

        }

    }


    def contactDetailsList={
        Coaching coachingInstance = springSecurityService.currentUser as Coaching
        render(view: "/coaching/profile/contactDetailsList", model: [contactDetailsList: (coachingInstance?.contactDetails), activeLink: 'ContactDetails'])
    }

    def edit(Long cDetailsId) {
        ContactDetails contactDetailsInstance = ContactDetails.findById(cDetailsId)
        if (contactDetailsInstance) {
            render(view: "/coaching/profile/editContactDetailsForm", model: [contactDetailsInstance: contactDetailsInstance, activeLink: 'ContactDetails'])
        } else {
            flash.danger = 'Something went wrong. Please try again'
            redirect(controller: 'coachingProfile', action: 'contactDetailsList')
        }
    }


    def update(ContactDetails contactDetailsInstance){

           contactDetailsInstance.country='India'



        if (contactDetailsInstance.validate()) {
            contactDetailsInstance.save(insert: false, update: true)
            flash.success = "Contact details updated successfully."
            redirect(controller: 'coachingProfile',action: 'contactDetailsList')
        } else {
            flash.danger = 'Something went wrong. Please try again'
            render(view: "/coaching/profile/editContactDetailsForm", model: [contactDetailsInstance: contactDetailsInstance, activeLink: 'ContactDetails'])
        }
    }


    @Transactional
    def delete(String cDetailsId) {
        ContactDetails contactDetailsInstance = ContactDetails.findById(cDetailsId)



        if (contactDetailsInstance) {
            Coaching coaching = springSecurityService.currentUser as Coaching
            coaching.removeFromContactDetails(contactDetailsInstance)
            coaching.save(flush: true,failOnError: true)
            contactDetailsInstance.delete( flush: true)
            flash.success = 'Contact detail deleted successfully'
            } else {
            flash.danger = 'Something went wrong. Please try again'
            }
            redirect(controller: 'coachingProfile', action: 'contactDetailsList')
    }

    def addContactDetailsForm={
        render(view: '/coaching/profile/addContactDetailsForm')
    }

    def contactDetailsSubmit={
        ContactDetails contactDetailsInstance=new ContactDetails(params)
        contactDetailsInstance.postalCode=Long.parseLong(params.postalCode)
        contactDetailsInstance.country='India'
        println('Inside update------------------')
        Coaching coaching = springSecurityService.currentUser as Coaching
        coaching.addToContactDetails(contactDetailsInstance)
        if (contactDetailsInstance.validate()) {
            coaching.save(flush: true)
            flash.success = "Contact details saved successfully."
            redirect(controller: 'coachingProfile',action: 'contactDetailsList')
        } else {
            flash.danger = 'Something went wrong. Please try again'
            render(view: "/coaching/profile/addContactDetailsForm", model: [contactDetailsInstance: contactDetailsInstance, activeLink: 'ContactDetails'])
        }
    }
}
