package com.coachingPortal.backend.coaching

import com.coachingPortal.coaching.Batch
import com.coachingPortal.coaching.SubscribeUser
import com.coachingPortal.users.Coaching
import com.coachingPortal.users.Person
import grails.converters.JSON
import grails.plugins.springsecurity.Secured

/**
 * Created by MAYANK DWIVEDI on 6/13/2015.
 */
@Secured(['IS_AUTHENTICATED_FULLY', 'ROLE_ADMIN', 'ROLE_COACHING'])
class SubscribeStudentController {
    def springSecurityService

    def index() {
        Long batchId
        println('batch id in params is------------------'+params.batchId)


        if(params.batchId){
            Batch batch =Batch.findById(Long.parseLong(params.batchId))
            Coaching coaching=springSecurityService.currentUser as Coaching
            if(batch?.coaching==coaching){
            session.setAttribute('batchId', params.batchId)
            batchId=Long.parseLong(params.batchId)
            }else{
                flash.danger='This batch does not belong to coaching.'
                redirect(controller: 'studentBatch',action: 'index')
            }

        }else{
            batchId=Long.parseLong(session.getAttribute('batchId'))
            println('bbbbbbbbbbbbbb Suscribe students bbbbbbbbbbbbbb')
        }

        println('batch id is-------------- '+batchId)
        List<SubscribeUser> subscribeStudentsList = SubscribeUser.findAllByBatch(Batch.findById(batchId))
        println("Subscribed users list is " + subscribeStudentsList)
        render(view: '/coaching/subscribeUsers/index', model: [subscribeStudentsList: subscribeStudentsList])
    }

    def findUserPage() {
        render(view: '/coaching/subscribeUsers/findUserPage')
    }

    def ax_findUser() {
        def result = [:]
        if (params.searchedUserUsername) {
            Person student = Person.findByUsername(params.searchedUserUsername)
            SubscribeUser subscribeUser = SubscribeUser.findByStudent(student)
            println('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa ')
            String isSuscribed = null
            if (subscribeUser) {
                if (subscribeUser.batch.id == session.getAttribute('batchId')) {
                    isSuscribed = 'true'
                    println('bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb')
                } else {
                    isSuscribed = 'false'
                    println('cccccccccccccccccccccccccccccccccccccccc')
                }
            } else {
                isSuscribed = 'false'
            }

            result.studentList = g.render(template: '/coaching/subscribeUsers/searchedUserTemplate', model: [student: student, isSuscribed: isSuscribed])

            result.result = 'SUCCESS'
            result.successMsg = 'Ajax call successful.'

        } else {
            result.errMsg = "Something went wrong. Please try again."
        }
        render result as JSON
    }

    def addUser() {
        if (params.studentId) {
            Person student = Person.get(params.studentId)
            println('Batch Id in session is--------'+session.getAttribute('batchId'))
            Batch batch = Batch.findById(Long.parseLong(session.getAttribute('batchId')))

            SubscribeUser subscribeUserNew = new SubscribeUser(student)
            batch.addToSubscribeStudents(subscribeUserNew)

            if (subscribeUserNew.validate()) {
                batch.save(flush: true, failOnError: true)
                flash.success = 'Student subscribed successfully.'
            } else {
                flash.danger = 'Something went wrong'
                render(view: '/coaching/subscribeUsers/findUserPage')
            }

        }else {
            flash.danger = 'Something went wrong'
        }
        redirect(controller: 'subscribeStudent', action: 'index')
    }

    def removeUser = {

        if (params.studentId) {

            SubscribeUser subscribeUser = SubscribeUser.findById(Long.parseLong(params.studentId))
            println('Subscribe user name is '+subscribeUser.student.username)
            Batch batch = Batch.findById(Long.parseLong(session.getAttribute('batchId')))
            if (subscribeUser.validate()) {
                subscribeUser.delete(flush: true, failOnError: true)
                /*batch.save(flush: true, failOnError: true)*/
                flash.success = 'Student unsubscribed from batch successfully'
            } else {
                flash.danger = 'Something went wrong'
                render(view: '/coaching/subscribeUsers/findUserPage')
            }

        } else {
            flash.danger = 'Something went wrong'
            render(view: '/coaching/subscribeUsers/findUserPage')
        }
        redirect(controller: 'subscribeStudent', action: 'index')
    }

}
