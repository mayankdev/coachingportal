package com.coachingPortal.backend.coaching

import com.coachingPortal.ContactUsCO
import com.coachingPortal.security.User
import grails.plugins.springsecurity.Secured

/**
 * Created by MAYANK DWIVEDI on 6/18/2015.
 */
@Secured(['IS_AUTHENTICATED_FULLY', 'ROLE_ADMIN','ROLE_COACHING'])
class CoachingMailController {
    def mandrillHelperService
    def springSecurityService

    def index={
    render(view: '/coaching/communicate/home')
    }

    def sms={
        render(view: '/coaching/communicate/sms/smsForm')
    }

    def email={
        render(view: '/coaching/communicate/mail/mailForm')
    }

    def mailToUserForm(){
    render(view: '/coaching/communicate/mail/mailToUserForm',model: [mailId:params?.mailId])
    }

    def sendMailUser(){
        ContactUsCO contactUsCO=new ContactUsCO()
        Map<String,String> recipientMailId=new HashMap<String,String>()
        String mailIds=params.mailId
        String[] arrayMailId=mailIds.split(',')

        arrayMailId.each {arrayMailIdEach->
            recipientMailId.put(arrayMailIdEach,'Dummy')
        }

        contactUsCO.mailId=arrayMailId[0]
        contactUsCO.body=params.body
        contactUsCO.subject=params.subject
        if(contactUsCO.validate()){
            User mailSender=springSecurityService.currentUser as User

            String htmlTemplate = g.render(template: '/emailTemplates/mailTemplate', model: [message: params.body ])

            mandrillHelperService.sendMandrillMail(recipientMailId,params.subject,htmlTemplate,'Team PrepareEasy',mailSender)

            flash.success='Mail sent successfully'
            redirect(controller: 'subscribeStudent',action: 'index')
        }else{
            render(view:'/coaching/communicate/mail/mailToUserForm' ,model: [contactUsCO:contactUsCO,mailId:params.mailId]);
        }

    }

}
