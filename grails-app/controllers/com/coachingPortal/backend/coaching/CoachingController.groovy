package com.coachingPortal.backend.coaching

import com.coachingPortal.CoachingSignUpCO
import com.coachingPortal.security.Role
import com.coachingPortal.security.User
import com.coachingPortal.security.UserRole
import com.coachingPortal.users.Coaching
import grails.plugins.springsecurity.Secured

/**
 * Created by MAYANK DWIVEDI on 7/1/2015.
 */
@Secured(['ROLE_COACHING', 'ROLE_ADMIN','IS_AUTHENTICATED_FULLY'])
class CoachingController {

    def mandrillHelperService

   def addCoachingForm={
       render (view: '/coaching/profile/addCoachingForm')
   }

    def addCoaching={CoachingSignUpCO coachingSignUpCO->


        if (coachingSignUpCO.validate()) {

            Coaching coaching = new Coaching(coachingSignUpCO)
            coaching.enabled = true
            coaching.save(flush: true)

            UserRole.create(coaching, Role.findByAuthority("ROLE_COACHING"))

            Map<String, String> toAddressMap = [:]
            toAddressMap.put(coaching?.username, coaching?.name)
            String htmlTemplate = g.render(template: '/emailTemplates/coachingAdded', model: [person: coaching ])
            mandrillHelperService.sendMandrillMail(toAddressMap,'Added to PrepareEasy',htmlTemplate,'Team PrepareEasy',(coaching as User))
            //commonService.sendMandrillMail(toAddressMap, "Activate your account to get started with ClashMate", htmlTemplate, CommonMethod.MANDRILL_FROM_NAME)
            flash.success="Coaching account created successfully"
            redirect(controller: 'coachingProfile',action: 'home')

        } else {
            render(view: '/coaching/profile/addCoachingForm', model: [signUpCO: coachingSignUpCO])
        }

    }
}
