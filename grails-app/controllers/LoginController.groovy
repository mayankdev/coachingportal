import com.coachingPortal.PasswordResetCO
import com.coachingPortal.PersonSignUpCO
import com.coachingPortal.security.Role
import com.coachingPortal.security.User
import com.coachingPortal.security.UserRole
import com.coachingPortal.users.Coaching
import com.coachingPortal.users.Person
import grails.converters.JSON
import org.codehaus.groovy.grails.plugins.springsecurity.SpringSecurityUtils
import org.springframework.security.authentication.AccountExpiredException
import org.springframework.security.authentication.CredentialsExpiredException
import org.springframework.security.authentication.DisabledException
import org.springframework.security.authentication.LockedException
import org.springframework.security.core.context.SecurityContextHolder as SCH
import org.springframework.security.web.WebAttributes
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter

import javax.servlet.http.HttpServletResponse

class LoginController {

    def mandrillHelperService
    /**
     * Dependency injection for the authenticationTrustResolver.
     */
    def authenticationTrustResolver

    /**
     * Dependency injection for the springSecurityService.
     */
    def springSecurityService

    /**
     * Default action; redirects to 'defaultTargetUrl' if logged in, /login/auth otherwise.
     */
    def index = {
        if (springSecurityService.isLoggedIn()) {
            redirect uri: SpringSecurityUtils.securityConfig.successHandler.defaultTargetUrl
        } else {
            redirect action: 'auth', params: params
        }
    }

    /**
     * Show the login page.
     */
    def auth = {

        def config = SpringSecurityUtils.securityConfig

        if (springSecurityService.isLoggedIn()) {
            redirect uri: config.successHandler.defaultTargetUrl
            return
        }

        String view = 'auth'
        String postUrl = "${request.contextPath}${config.apf.filterProcessesUrl}"
        render view: view, model: [postUrl            : postUrl,
                                   rememberMeParameter: config.rememberMe.parameter]
    }

    def resolveTargetUrl = {
        def roles = (springSecurityService.currentUser as User).authorities

        Role studentRole = Role.findByAuthority('ROLE_STUDENT')
        Role coachingRole = Role.findByAuthority('ROLE_COACHING')
        Role adminRole = Role.findByAuthority('ROLE_ADMIN')

        if (studentRole in roles)
            redirect(controller: 'studentProfile', action: 'home')
        else if(coachingRole in roles){
            redirect(controller: 'coachingProfile', action: 'home')
        }else if(adminRole in roles){
            redirect(controller: 'adminProfile', action: 'home')
        }else{
            redirect(controller: 'login', action: 'auth')
        }
    }

    /**
     * The redirect action for Ajax requests.
     */
    def authAjax = {
        response.setHeader 'Location', SpringSecurityUtils.securityConfig.auth.ajaxLoginFormUrl
        response.sendError HttpServletResponse.SC_UNAUTHORIZED
    }

    /**
     * Show denied page.
     */
    def denied = {
        if (springSecurityService.isLoggedIn() &&
                authenticationTrustResolver.isRememberMe(SCH.context?.authentication)) {
            // have cookie but the page is guarded with IS_AUTHENTICATED_FULLY
            redirect action: 'full', params: params
        }
    }

    /**
     * Login page for users with a remember-me cookie but accessing a IS_AUTHENTICATED_FULLY page.
     */
    def full = {
        def config = SpringSecurityUtils.securityConfig
        render view: 'auth', params: params,
                model: [hasCookie: authenticationTrustResolver.isRememberMe(SCH.context?.authentication),
                        postUrl  : "${request.contextPath}${config.apf.filterProcessesUrl}"]
    }

    /**
     * Callback after a failed login. Redirects to the auth page with a warning message.
     */
    def authfail = {

        def username = session[UsernamePasswordAuthenticationFilter.SPRING_SECURITY_LAST_USERNAME_KEY]
        String msg = ''
        def exception = session[WebAttributes.AUTHENTICATION_EXCEPTION]
        if (exception) {
            if (exception instanceof AccountExpiredException) {
                msg = g.message(code: "springSecurity.errors.login.expired")
            } else if (exception instanceof CredentialsExpiredException) {
                msg = g.message(code: "springSecurity.errors.login.passwordExpired")
            } else if (exception instanceof DisabledException) {
                msg = g.message(code: "springSecurity.errors.login.disabled")
            } else if (exception instanceof LockedException) {
                msg = g.message(code: "springSecurity.errors.login.locked")
            } else {
                msg = g.message(code: "springSecurity.errors.login.fail")
            }
        }

        if (springSecurityService.isAjax(request)) {
            render([error: msg] as JSON)
        } else {
            flash.message = msg
            redirect action: 'auth', params: params
        }
    }

    /**
     * The Ajax success redirect url.
     */
    def ajaxSuccess = {
        render([success: true, username: springSecurityService.authentication.name] as JSON)
    }

    /**
     * The Ajax denied redirect url.
     */
    def ajaxDenied = {
        render([error: 'access denied'] as JSON)
    }

    def signUpForm = {
        render(view: '/login/signUpForm')
    }

    def signUpSubmit = { PersonSignUpCO signUpCO ->


        if (signUpCO.validate()) {

            Person student = new Person(signUpCO)
            student.enabled = false
            student.save(flush: true)

            UserRole.create(student, Role.findByAuthority("ROLE_USER"))

            flash.message = "Please check your email for account activation email we just sent you."
            Map<String, String> toAddressMap = [:]
            toAddressMap.put(student?.username, student?.fullName())
            String htmlTemplate = g.render(template: '/emailTemplates/accountActivate', model: [person: student])
            mandrillHelperService.sendMandrillMail(toAddressMap, 'Activate your account to get started with PrepareEasy', htmlTemplate, 'Team PrepareEasy', (student as User))
            //commonService.sendMandrillMail(toAddressMap, "Activate your account to get started with ClashMate", htmlTemplate, CommonMethod.MANDRILL_FROM_NAME)
            redirect(controller: 'login', action: 'auth')

        } else {
            render(view: '/login/signUpForm', model: [signUpCO: signUpCO])
        }

    }

    def activateAccount = {
        User user = User.findByUniqueId(params.token)
        Person student = Person.findByUniqueId(user.uniqueId)
        Coaching admin = Coaching.findByUniqueId(user.uniqueId)
        if (user) {

            user.enabled = true
            //student.uniqueId = UUID.randomUUID().toString()
            user.save(flush: true)

            Map<String, String> toAddressMap = [:]
            if (student) {
                toAddressMap.put(student?.username, student?.fullName())
            } else {
                toAddressMap.put(admin?.username, admin?.fullName())
            }
            //String htmlTemplate = g.render(template: '/emailTemplates/activateWelcome', model: [student: student])
            //commonService.sendMandrillMail(toAddressMap, "Welcome to ClashMate", htmlTemplate, CommonMethod.MANDRILL_FROM_NAME)

            flash.success = "Your account is activated. Please login to continue."
            redirect(controller: 'login', action: 'auth')
        } else {
            flash.error = "Something went wrong. Please try again."
            redirect(controller: 'public', action: 'landingPage')
        }

    }

    def passwordReset = {
        render(view: '/login/passwordResetMailForm')
    }

    def passwordResetMail = {
        User user = User.findByUsername(params.mailId)
        Person student = Person.findByUniqueId(user.uniqueId)
        Coaching admin = Coaching.findByUniqueId(user.uniqueId)
        if (user) {
            Map<String, String> toAddressMap = [:]
            String htmlTemplate = null
            if (student) {
                toAddressMap.put(student?.username, student?.fullName())
                htmlTemplate = g.render(template: '/emailTemplates/passwordReset', model: [person: student])
            } else {
                toAddressMap.put(admin?.username, admin?.fullName())
                htmlTemplate = g.render(template: '/emailTemplates/passwordReset', model: [person: admin])
            }
            //commonService.sendMandrillMail(toAddressMap, "ClashMate | Reset Password Assistance", htmlTemplate, CommonMethod.MANDRILL_FROM_NAME)
            mandrillHelperService.sendMandrillMail(toAddressMap, 'Reset Password', htmlTemplate, 'Team PrepareEasy', user)
            flash.success = 'Please check your email account for email along with all instructions to help you to reset your password.'
            render(view: '/login/auth')
        } else {
            flash.error = 'No user found with entered email address. Please try again.'
            render(view: '/login/passwordResetForm')
        }
    }

    def passwordResetMailPage = {
        User user = User.findByUniqueId(params.token)
        if (user) {
            render(view: 'passwordResetForm', model: [user: user])
        } else {
            flash.error = "Something went wrong. Please try again."
            redirect(action: 'passwordReset')
        }
    }

    def passwordResetValidate = { PasswordResetCO passwordResetCO ->
        User user = User.findByUniqueId(params.token)
        if (user) {
            if (passwordResetCO.validate()) {
                user.password = passwordResetCO.password
                user.save(flush: true)
                flash.success = 'Password has been reset successfully. Please login with new password to continue.'
                redirect(controller: 'login', action: 'auth')
            } else {
                render(view: '/login/passwordResetForm', model: [user: user, passwordResetCO: passwordResetCO])
            }
        } else {
            flash.danger = "Something went wrong. Please try again."
            render(view: '/login/passwordResetMailForm')
        }
    }

    def home = {
        render(view: '/login/home')
    }


    def reAuthenticateUser(String username){
        session.setAttribute('lastUser',null);
        springSecurityService.reauthenticate(username,null);
        redirect(controller: 'login',action: 'resolveTargetUrl');
    }

}
