import com.coachingPortal.users.Coaching
import com.coachingPortal.users.ContactDetails
import com.coachingPortal.users.Person

class BootStrap {

    def springSecurityService

    def init = {

        def studentRole = com.coachingPortal.security.Role.findByAuthority('ROLE_STUDENT') ?: new com.coachingPortal.security.Role(authority: 'ROLE_STUDENT').save(failOnError: true)
        def adminRole = com.coachingPortal.security.Role.findByAuthority('ROLE_ADMIN') ?: new com.coachingPortal.security.Role(authority: 'ROLE_ADMIN').save(failOnError: true)
        def coachingRole = com.coachingPortal.security.Role.findByAuthority('ROLE_COACHING') ?: new com.coachingPortal.security.Role(authority: 'ROLE_COACHING').save(failOnError: true)

        def contactDetails1 = new ContactDetails(houseNo: 'E-109', streetName: 'Krishna-Vihar', city: 'Kanpur', state: 'UP', country: 'India', postalCode: 208017)
        def contactDetails2 = new ContactDetails(houseNo: 'E-109', streetName: 'Krishna-Vihar', city: 'Kanpur', state: 'UP', country: 'India', postalCode: 208017)

        if(contactDetails1.validate()){
            println("Everything OK")
        }else{
            contactDetails1.errors.each {errors->
                println("It ------"+errors)
            }
        }

        def coaching1 = Coaching.findByUsername('coaching1@prepareeasy.in') ?: new Coaching(
                username: 'coaching1@prepareeasy.in',
                password: 'coaching1', name: 'Kautilya', contactNo: 9716915319, contactDetails: contactDetails1,
                enabled: true).save(flush: true, failOnError: true)



        def coaching2 = Coaching.findByUsername('coaching2@prepareeasy.in') ?: new Coaching(
                username: 'coaching2@prepareeasy.in',
                password: 'coaching2', name: 'urPower', contactNo: 8285632143, contactDetails: contactDetails2,
                enabled: true).save(flush: true,failOnError: true)

        def studentUser1 = Person.findByUsername('student1@prepareeasy.in') ?: new Person(
                username: 'student1@prepareeasy.in',
                password: 'student1',
                enabled: true).save(flush: true,failOnError: true)

        def studentUser2 = Person.findByUsername('student2@prepareeasy.in') ?: new Person(
                username: 'student2@prepareeasy.in',
                password: 'student2',
                enabled: true).save(flush: true,failOnError: true)

        def admin1=Person.findByUsername('admin1@prepareeasy.in') ?: new Person(
                username: 'admin1@prepareeasy.in',
                password: 'admin1',
                enabled: true).save(flush: true,failOnError: true)

        if (!coaching1.authorities.contains(coachingRole)) {
            com.coachingPortal.security.UserRole.create coaching1, coachingRole
        }

        if (!coaching2.authorities.contains(coachingRole)) {
            com.coachingPortal.security.UserRole.create coaching2, coachingRole
        }

        if (!studentUser1.authorities.contains(studentRole)) {
            com.coachingPortal.security.UserRole.create studentUser1, studentRole
        }

        if (!studentUser2.authorities.contains(studentRole)) {
            com.coachingPortal.security.UserRole.create studentUser2, studentRole
        }

        if (!admin1.authorities.contains(adminRole)) {
            com.coachingPortal.security.UserRole.create admin1, adminRole
        }
    }
    def destroy = {
    }
}
