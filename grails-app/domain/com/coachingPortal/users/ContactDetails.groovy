package com.coachingPortal.users

class ContactDetails {

    String houseNo
    String streetName
    String city
    String state
    String country
    long postalCode

    Date dateCreated
    Date lastUpdated

    String uniqueId = UUID.randomUUID().toString()


    static constraints = {
        houseNo nullable: false,blank: false
        streetName nullable: false,blank: false
        city nullable: false,blank: false
        state nullable: false,blank: false
        postalCode blank:false, matches:"[0-9]{6}"
    }

   def beforeInsert() {
        uniqueId = UUID.randomUUID().toString()
    }
}
