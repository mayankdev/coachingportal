package com.coachingPortal.users

import com.coachingPortal.CoachingSignUpCO
import com.coachingPortal.coaching.Batch
import com.coachingPortal.coaching.tasks.Holidays
import com.coachingPortal.coaching.tasks.Infocenter
import com.coachingPortal.security.User

/**
 * Created by MAYANK DWIVEDI on 6/1/2015.
 */
class Coaching extends  User{
    String name

    static hasMany = [contactDetails:ContactDetails,batches:Batch,infocenter:Infocenter,holidays:Holidays]


    static  constraints = {
        name nullable: false,blank: false
        contactDetails nullable: false,blank: false
        batches nullable: true,blank: true
        infocenter nullable: true,blank: true
        holidays nullable: true,blank: true
    }

    static  mapping = {
        name type: 'text'
    }

    Coaching(CoachingSignUpCO coachingSignUpCO){
        this.name = coachingSignUpCO?.name
        this.contactNo = coachingSignUpCO?.contactNo
        this.password = coachingSignUpCO?.password
        this.username = coachingSignUpCO?.username
        this.contactNo = coachingSignUpCO?.contactNo
        this.contactDetails.streetName = coachingSignUpCO?.streetName
        this.contactDetails.city = coachingSignUpCO?.city
        this.contactDetails.state = coachingSignUpCO?.state
        this.contactDetails.country = coachingSignUpCO?.country
        this.contactDetails.postalCode = coachingSignUpCO?.postalCode
    }

    String coachingName(){
        return ((this.name!='')?:this.username)

    }

}
