package com.coachingPortal.users

import com.coachingPortal.PersonSignUpCO
import com.coachingPortal.security.User

/**
 * Created by MAYANK DWIVEDI on 6/1/2015.
 */
class Person extends User{

    String firstName
    String lastName
    ContactDetails contactDetails

    static  constraints = {
        firstName nullable: true,blank: true
        lastName nullable: true,blank:true
        contactDetails nullable: true
    }

    Person(PersonSignUpCO signUpCO){
        this.firstName = signUpCO?.firstName
        this.lastName = signUpCO?.lastName
        this.password = signUpCO?.password
        this.username = signUpCO?.username
        this.contactNo = signUpCO?.contactNo
    }

    String fullName(){
        return (this.firstName+' '+this.lastName!='null null')?:this.username

    }
}
