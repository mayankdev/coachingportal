package com.coachingPortal

import com.coachingPortal.security.User
import org.grails.mandrill.SendResponse

/**
 * Created by hitenpratap on 9/5/15.
 */
class MailHistory {

    Date dateCreated
    Date lastUpdated
    String emailAddress
    String title
    String status
    String rejectionReason
    Boolean success = false
    User sender

    static mapping = {
        title type: 'text'
    }

    static constraints = {
        emailAddress blank: false, nullable: false
        title blank: false, nullable: false
        status blank: false, nullable: false
        rejectionReason blank: true, nullable: true
    }

    MailHistory() {}

    MailHistory(SendResponse sendResponse, String subjectStr, User mailSender) {
        this.emailAddress = sendResponse?.email
        this.status = sendResponse?.status
        this.rejectionReason = sendResponse?.rejectReason
        this.success = sendResponse?.success
        this.title = subjectStr
        this.sender = mailSender
    }
}
