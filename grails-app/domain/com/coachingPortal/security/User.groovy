package com.coachingPortal.security


class User {

	transient springSecurityService

	String username
	String password
    long contactNo
	boolean enabled

    //Include short Id like alphanumeric

	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired

    String uniqueId = UUID.randomUUID().toString()

    Date dateCreated
    Date lastUpdated

	static constraints = {
		username blank: false, unique: true,email: true
		password blank: false
        contactNo blank:false, matches:"[0-9]{10}"
	}

	static mapping = {
		password column: '`password`'
	}

	Set<Role> getAuthorities() {
		UserRole.findAllByUser(this).collect { it.role } as Set
	}

	def beforeInsert() {
		encodePassword()
        uniqueId = UUID.randomUUID().toString()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		password = springSecurityService.encodePassword(password)
	}
}
