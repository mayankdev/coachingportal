package com.coachingPortal.result

import com.coachingPortal.practicePaper.PracticePaper
import com.coachingPortal.users.Person

/**
 * Created by MAYANK DWIVEDI on 6/12/2015.
 */
class Result {
    Person student
    PracticePaper practicePaper
    long timeTaken

    Integer questionsCorrect
    Integer questionsIncorrect
    Integer questionsUnattempted

    Date dateCreated
    Date lastUpdated

    String uniqueId = UUID.randomUUID().toString()

    static hasMany=[resultSubjects:ResultSubject]

    static constraints = {
        student nullable: false,blank:false
        practicePaper nullable: false,blank:false
        timeTaken null:false
        questionsCorrect nullable: false,blank:false
        questionsIncorrect nullable: false,blank:false
        questionsUnattempted nullable: false,blank:false
        resultSubjects nullable: true
    }

    def beforeInsert() {
        uniqueId = UUID.randomUUID().toString()
    }
}
