package com.coachingPortal.result


/**
 * Created by MAYANK DWIVEDI on 6/12/2015.
 */
class ResultSubject {
    String subjectType
    Integer questionsCorrect
    Integer questionsIncorrect
    Integer questionsUnattempted
    String timeTaken

    Date dateCreated
    Date lastUpdated

    static belongsTo = [result:Result]

    static constraints = {
        subjectType nullable: false,blank:false
        questionsCorrect nullable: false,blank:false
        questionsIncorrect nullable: false,blank:false
        questionsUnattempted nullable: false,blank:false
        timeTaken nullable: false
    }
}
