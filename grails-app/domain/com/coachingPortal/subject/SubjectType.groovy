package com.coachingPortal.subject

/**
 * Created by MAYANK DWIVEDI on 6/21/2015.
 */
class SubjectType {
    String subject

    Date dateCreated
    Date lastUpdated

    public SubjectType(String subject){
        this.subject=subject
    }


    static constraints = {
        subject nullable: false,blank:false,unique: true

    }

    String toString(){
        return this.subject
    }
}
