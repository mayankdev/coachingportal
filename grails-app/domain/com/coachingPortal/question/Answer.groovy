package com.coachingPortal.question

/**
 * Created by MAYANK DWIVEDI on 6/1/2015.
 */
class Answer {
    String answerText1
    String answerText2
    String answerText3
    String answerText4
    String answerOption

    Date dateCreated
    Date lastUpdated



    //static hasOne= [images:AnswerImage]
    static belongsTo = [question:Question]
    static mapping = {
        answerText1 type: 'text'
        answerText2 type: 'text'
        answerText3 type: 'text'
        answerText4 type: 'text'
        answerOption type: 'text'
    }

    static constraints = {
        answerText1 blank: false, nullable: false
        answerText2 blank: false, nullable: false
        answerText3 blank: false, nullable: false
        answerText4 blank: false, nullable: false
        answerOption blank: false, nullable: false

    }
}
