package com.coachingPortal.question

import com.coachingPortal.users.Coaching
import com.enums.Enums

/**
 * Created by MAYANK DWIVEDI on 6/1/2015.
 */
class Question {

    String questionText
    Date dateCreated
    Date lastUpdated
    Coaching createdBy
    Enums.Difficulty difficulty = Enums.Difficulty.EASY
    String subjectType
    String uniqueId = UUID.randomUUID().toString()
    String option1
    String option2
    String option3
    String option4
    QuestionHeader questionHeader

    static hasMany = [images: QuestionImage]
    static hasOne = [answer: Answer]



    static constraints = {
        questionText blank: false, nullable: false
        createdBy blank: false, nullable: false
        difficulty blank: false, nullable: false
        subjectType blank: false, nullable: false
        option1 nullable: true
        option2 nullable: true
        option3 nullable: true
        option4 nullable: true
        answer unique: true
        questionHeader nullable: true
        images nullable: true
    }

    static mapping = {
        questionText type: 'text'
        option1 type: 'text'
        option2 type: 'text'
        option3 type: 'text'
        option4 type: 'text'
        questionHeader lazy: false
    }

    String toString(){
        return this.questionText
    }
}
