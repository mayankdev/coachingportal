package com.coachingPortal.practicePaper

import com.coachingPortal.question.Question
import org.jsoup.Jsoup


class PracticePaperQuestion {

    Question question

    static belongsTo = [practicePaperSubject: PracticePaperSubject]

    static constraints = {
        question nullable: false
    }

    public String getQuestionShortText(){
        String quesStr = Jsoup.parse(this.question.questionText)
        if(quesStr.length()>70){
            return quesStr.subSequence(0,70)+"..."
        }else{
            return quesStr
        }
    }
}
