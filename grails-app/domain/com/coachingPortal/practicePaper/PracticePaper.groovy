package com.coachingPortal.practicePaper

import com.coachingPortal.users.Coaching
import com.enums.Enums

class PracticePaper {

    String examName
    Coaching createdBy
    String uniqueId = UUID.randomUUID().toString()
    Integer totalTime

    Integer examPrice
    String paymentType
    /*paid/unpaid*/
    String openStatus
    /*openStatus can be private,public ,both*/
    Integer totalQuestions
    Date dateCreated
    Date lastUpdated
    Enums.PracticePaperStatus practicePaperStatus = Enums.PracticePaperStatus.UNPUBLISH

    static hasMany = [practicePaperSubjects: PracticePaperSubject]

    static mapping = {
        practicePaperSubjects sort: 'subjectType', order: 'asc'
        paymentType sqlType: 'varchar(6)'
        openStatus sqlType: 'varchar(7)'
    }

    static constraints = {
        examName nullable: false, blank: false
        openStatus nullable: false, blank: false
        paymentType nullable: false, blank: false
        totalTime min: 1, nullable: false
        examPrice nullable: true
        totalQuestions nullable: true
        practicePaperSubjects nullable: true
        createdBy nullable: false,blank:false
    }


    def beforeInsert() {
        uniqueId = UUID.randomUUID().toString()
    }



    //Has error
    def totalSubjectsList() {
        List<PracticePaperSubject> practicePaperSubjectList = this.examSubjects
        String subjectsList = null
        for (int i = 0; i < practicePaperSubjectList.size(); i++) {
            if (i == 0) {
                subjectsList = subjectsList + practicePaperSubjectList.get(i)
            } else {
                subjectsList = subjectsList + ',' + practicePaperSubjectList.get(i)
            }
        }
        return subjectsList
    }


}
