package com.coachingPortal.practicePaper


class PracticePaperSubject {

    String subjectType
    Integer numOfQuestion
    String uniqueId = UUID.randomUUID().toString()

    static belongsTo = [practicePaper: PracticePaper]

    static hasMany = [practicePaperQuestions: PracticePaperQuestion]

    PracticePaperSubject(String subjectType,Integer numOfQuestion){
       this.subjectType=subjectType
        this.numOfQuestion=numOfQuestion
    }

    static constraints = {
        subjectType nullable: false
        numOfQuestion min: 1, nullable: false
        practicePaper nullable: false,blank:false
        practicePaperQuestions nullable: true,blank:true
    }

    def beforeInsert() {
        uniqueId = UUID.randomUUID().toString()
    }

    String toString(){
        return subjectType
    }


}
