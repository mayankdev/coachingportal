package com.coachingPortal.coaching.tasks

import com.coachingPortal.users.Coaching

/**
 * Created by MAYANK DWIVEDI on 2/11/2016.
 */
class Infocenter {

    String title
    String description
    Date dateCreated
    Date lastUpdated

    static belongsTo = [coaching:Coaching]

    static constraints = {
        title nullable: false, blank: false
        description nullable: false, blank: false
        coaching nullable: false, blank: false
    }

    static mapping = {
        description type: 'text'
    }

    String getShortDescription(){
        this.description.size()>100?((this.description).substring(0,100)+'......'):this.description
    }
}
