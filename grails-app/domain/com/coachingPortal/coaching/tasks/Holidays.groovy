package com.coachingPortal.coaching.tasks

import com.coachingPortal.users.Coaching

/**
 * Created by MAYANK DWIVEDI on 2/11/2016.
 */
class Holidays {
    String title
    Date dateOfHoliday
    Date dateCreated
    Date lastUpdated

    static belongsTo = [coaching:Coaching]

    static constraints = {
        title nullable: false, blank: false
        dateOfHoliday nullable: false, blank: false
        coaching nullable: false, blank: false
     }

  }
