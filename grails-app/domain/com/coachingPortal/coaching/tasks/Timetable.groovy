package com.coachingPortal.coaching.tasks

import com.coachingPortal.coaching.Batch

/**
 * Created by MAYANK DWIVEDI on 2/11/2016.
 */
class Timetable {

    String day
    String startTime
    String duration
    String subjectName

    Date dateCreated
    Date lastUpdated

    static belongsTo = [batch:Batch]

    static constraints = {
        day nullable: false, blank: false
        startTime nullable: false, blank: false
        duration null:false
        subjectName nullable: false, blank: false
        batch nullable: false, blank: false
    }

    static mapping = {
        startTime sqlType: 'varchar(5)'
        duration sqlType: 'varchar(5)'
        day sqlType: 'varchar(9)'
        subjectName sqlType: 'varchar(50)'
    }

    String toString(){
        return this.day
    }
}
