package com.coachingPortal.coaching

import com.coachingPortal.coaching.tasks.Timetable
import com.coachingPortal.practicePaper.PracticePaper
import com.coachingPortal.studyMaterial.StudyMaterial
import com.coachingPortal.users.Coaching

/**
 * Created by MAYANK DWIVEDI on 12/22/2015.
 */
class Batch {
    String name
    Date dateCreated
    Date lastUpdated

    static hasMany = [subscribeStudents:SubscribeUser,timetable:Timetable,practicePapers:PracticePaper,studyMaterials:StudyMaterial]
    static belongsTo = [coaching:Coaching]
    static constraints = {
        name nullable:false ,blank: false
        subscribeStudents nullable: true
        timetable nullable: true
        practicePapers nullable: true
        studyMaterials nullable: true
    }


}
