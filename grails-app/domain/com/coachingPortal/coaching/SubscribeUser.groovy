package com.coachingPortal.coaching

import com.coachingPortal.users.Person

/**
 * Created by MAYANK DWIVEDI on 6/13/2015.
 */
class SubscribeUser {

    Person student

    Date dateCreated
    Date lastUpdated

    static belongsTo = [batch: Batch]

    static mapping = {
        student sort: 'firstName', order: 'asc'
    }

    static constraints = {
        batch nullable: false
        student nullable: false
    }

    SubscribeUser(Person person){
        this.student=person
    }

    String fullName(){
        return (student.firstName+' '+student.lastName!='null null')?:student.username

    }
}
