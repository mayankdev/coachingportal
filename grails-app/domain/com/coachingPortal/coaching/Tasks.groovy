package com.coachingPortal.coaching

import com.coachingPortal.users.Coaching


class Tasks {
    String title
    String description
    String taskType
    Coaching createdBy
    Date dateCreated
    Date lastUpdated

    static constraints = {
        title nullable: false, blank: false
        description nullable: false, blank: false
        taskType nullable: false, blank: false
        createdBy nullable: false, blank: false
    }

    static mapping = {
        description type: 'text'
        taskType sqlType: 'varchar(12)'
    }
}
