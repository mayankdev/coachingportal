package com.coachingPortal

import com.coachingPortal.security.User
import org.grails.mandrill.SendResponse

/**
 * Created by MAYANK DWIVEDI on 02-Apr-16.
 */
class SMSHistory {
    Date dateCreated
    Date lastUpdated
    String contactNos
    String msg

    User sender

    static mapping = {
        contactNos type: 'text'
    }

    static constraints = {
        msg blank: false, nullable: false
        sender blank: false, nullable: false

    }



    SMSHistory(String contactNos, String msg, User smsSender) {
        this.contactNos = contactNos
        this.msg = msg
        this.sender = smsSender
    }
}

