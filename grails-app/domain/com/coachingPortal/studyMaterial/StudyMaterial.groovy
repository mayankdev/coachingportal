package com.coachingPortal.studyMaterial

import com.coachingPortal.coaching.Batch

/**
 * Created by MAYANK DWIVEDI on 2/25/2016.
 */
class StudyMaterial {
    String name
    String storedName
    String storedLocation
    String smUniqueId = UUID.randomUUID().toString()
    /*later study material size is also to be tracked , that it does not exceeds some limit*/
    String paymentType
    /*paid/unpaid initially unpaid*/
    String openStatus
    /*openStatus can be private,public ,both initially private*/

    Date dateCreated
    Date lastUpdated

    static belongsTo = [batch: Batch]

    static constraints = {
        name nullable: false, blank: false
        storedName nullable: false, blank: false, unique: true
        storedLocation nullable: false, blank: false
        openStatus nullable: false, blank: false
        paymentType nullable: false, blank: false
        smUniqueId nullable: false, blank: false, unique: true

        batch nullable: false, blank: false
    }
}
