package com.coachingPortal

import com.coachingPortal.security.User
import org.grails.mandrill.MandrillMessage
import org.grails.mandrill.MandrillRecipient

class MandrillHelperService {
def mandrillService
    def serviceMethod() {

    }


    def sendMandrillMail(Map<String, String> toAddressMap, String subjectStr,String htmlStr,String fromName,User mailSender){
        def recipients = []
        toAddressMap.keySet().each { emailAddress ->
            recipients.add(new MandrillRecipient(name: toAddressMap.get(emailAddress), email: emailAddress))
        }

        def message = new MandrillMessage(
                html: htmlStr,
                subject:subjectStr,
                from_email:"coachingportal15@coachmate.com", from_name: fromName,
                to:recipients)
        message.tags.add("test")
        def ret = mandrillService.send(message)

        ret.each {
            MailHistory mailHistory = new MailHistory(it, subjectStr,mailSender)
            mailHistory.save(flush: true)
            println("**********************    Send mail response ${it.email}==================>>>>>>>>>>>>>>>>>>      ${it.status}")

        }

        if (ret.success) {
            println("Mandrill message sent")
        } else {
            println("Error sending email: status: $ret.status rejectReason: $ret.rejectReason, message: $ret.message")
        }



    }
}
